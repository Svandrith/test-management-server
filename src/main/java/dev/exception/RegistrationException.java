package dev.exception;

public class RegistrationException extends ServerException{
    public RegistrationException(String errorMessage, ServerErrorCode errorCode) {
        super(errorMessage, errorCode);
    }
}
