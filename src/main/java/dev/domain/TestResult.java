package dev.domain;

import dev.domain.enums.Result;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "test_result")
public class TestResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime testDate;
    private Result result;
    private String comment;
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name="test_case_id", nullable = false)
    private TestCase testCase;


    public TestResult(Long id, LocalDateTime testDate, Result result, String comment, TestCase testCase) {
        this.id = id;
        this.testDate = testDate;
        this.result = result;
        this.comment = comment;
        this.testCase = testCase;
    }

    public TestResult() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTestDate() {
        return testDate;
    }

    public void setTestDate(LocalDateTime testDate) {
        this.testDate = testDate;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public TestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(TestCase testCase) {
        this.testCase = testCase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestResult that = (TestResult) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getTestDate(), that.getTestDate()) && getResult() == that.getResult() && Objects.equals(getComment(), that.getComment()) && Objects.equals(getTestCase(), that.getTestCase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTestDate(), getResult(), getComment(), getTestCase());
    }

    @Override
    public String toString() {
        return "TestResult{" +
                "id=" + id +
                ", testDate=" + testDate +
                ", result=" + result +
                ", comment='" + comment + '\'' +
                ", testCase=" + testCase +
                '}';
    }
}
