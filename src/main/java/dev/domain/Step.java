package dev.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "step")
public class Step {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private String expectedResult;

    public Step(Long id, String description, String expectedResult) {
        this.id = id;
        this.description = description;
        this.expectedResult = expectedResult;
    }

    public Step() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Step step = (Step) o;
        return Objects.equals(getId(), step.getId()) && Objects.equals(getDescription(), step.getDescription()) && Objects.equals(getExpectedResult(), step.getExpectedResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDescription(), getExpectedResult());
    }

    @Override
    public String toString() {
        return "Step{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", expectedResult='" + expectedResult + '\'' +
                '}';
    }
}
