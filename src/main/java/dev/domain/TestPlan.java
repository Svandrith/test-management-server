package dev.domain;

import dev.domain.enums.PlanStatus;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "test_plan")
public class TestPlan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @Enumerated(EnumType.STRING)
    private PlanStatus status;
    @Column(nullable = false)
    private LocalDateTime creationDate;
    @Column(nullable = false)
    private LocalDateTime lastUpdateDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name="project_id", nullable = false)
    private Project project;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "test_suite_test_plan",
            joinColumns = { @JoinColumn(name = "test_plan_id") },
            inverseJoinColumns = { @JoinColumn(name = "test_suite_id") }
    )
    private List<TestSuite> testSuite;

    public TestPlan(Long id, String name, String description, PlanStatus status, LocalDateTime creationDate, LocalDateTime lastUpdateDate, Project project, List<TestSuite> testSuite) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.creationDate = creationDate;
        this.lastUpdateDate = lastUpdateDate;
        this.project = project;
        this.testSuite = testSuite;
    }

    public TestPlan() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlanStatus getStatus() {
        return status;
    }

    public void setStatus(PlanStatus status) {
        this.status = status;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<TestSuite> getTestSuite() {
        return testSuite;
    }

    public void setTestSuite(List<TestSuite> testSuite) {
        this.testSuite = testSuite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestPlan testPlan = (TestPlan) o;
        return Objects.equals(getId(), testPlan.getId()) && Objects.equals(getName(), testPlan.getName()) && Objects.equals(getDescription(), testPlan.getDescription()) && getStatus() == testPlan.getStatus() && Objects.equals(getCreationDate(), testPlan.getCreationDate()) && Objects.equals(getLastUpdateDate(), testPlan.getLastUpdateDate()) && Objects.equals(getProject(), testPlan.getProject()) && Objects.equals(getTestSuite(), testPlan.getTestSuite());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getStatus(), getCreationDate(), getLastUpdateDate(), getProject(), getTestSuite());
    }

    @Override
    public String toString() {
        return "TestPlan{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", creationDate=" + creationDate +
                ", lastUpdateDate=" + lastUpdateDate +
                ", project=" + project +
                ", testSuite=" + testSuite +
                '}';
    }
}
