package dev.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PlanCaseId implements Serializable {
    private long testCaseId;
    private long testPlanId;

    public PlanCaseId(long testCaseId, long testPlanId) {
        this.testCaseId = testCaseId;
        this.testPlanId = testPlanId;
    }

    public PlanCaseId() {
    }

    public long getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(long testCaseId) {
        this.testCaseId = testCaseId;
    }

    public long getTestPlanId() {
        return testPlanId;
    }

    public void setTestPlanId(long testPlanId) {
        this.testPlanId = testPlanId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanCaseId that = (PlanCaseId) o;
        return getTestCaseId() == that.getTestCaseId() && getTestPlanId() == that.getTestPlanId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTestCaseId(), getTestPlanId());
    }
}
