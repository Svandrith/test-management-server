package dev.domain;

import dev.domain.enums.Result;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "plan_case_result")
public class PlanCaseResult {
    @EmbeddedId
    private PlanCaseId id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Result result;
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @MapsId(value = "testCaseId")
    @JoinColumn(name = "case_id")
    private TestCase testCase;
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @MapsId("testPlanId")
    @JoinColumn(name = "plan_id")
    private TestPlan testPlan;

    public PlanCaseResult(PlanCaseId id, Result result, TestCase testCase, TestPlan testPlan) {
        this.id = id;
        this.result = result;
        this.testCase = testCase;
        this.testPlan = testPlan;
    }

    public PlanCaseResult() {
    }

    public PlanCaseId getId() {
        return id;
    }

    public void setId(PlanCaseId id) {
        this.id = id;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public TestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(TestCase testCase) {
        this.testCase = testCase;
    }

    public TestPlan getTestPlan() {
        return testPlan;
    }

    public void setTestPlan(TestPlan testPlan) {
        this.testPlan = testPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanCaseResult that = (PlanCaseResult) o;
        return Objects.equals(getId(), that.getId()) && getResult() == that.getResult() && Objects.equals(getTestCase(), that.getTestCase()) && Objects.equals(getTestPlan(), that.getTestPlan());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getResult(), getTestCase(), getTestPlan());
    }

    @Override
    public String toString() {
        return "PlanCaseResult{" +
                "id=" + id +
                ", result=" + result +
                ", testCase=" + testCase +
                ", testPlan=" + testPlan +
                '}';
    }
}
