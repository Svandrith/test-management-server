package dev.domain;


import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "\"user\"")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, length = 128)
    private String email;
    @Column(name = "encoded_password")
    private String encodedPassword;

    @Column(name = "is_activated")
    private boolean isActivated;

    @Column(name = "activation_token")
    private String activationToken;

    public User() {
    }

    public User(String email, boolean isActivated) {
        this.email = email;
        this.isActivated = isActivated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getEncodedPassword() {
        return encodedPassword;
    }

    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean activated) {
        isActivated = activated;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public void setActivationToken(String activationToken) {
        this.activationToken = activationToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return isActivated() == user.isActivated() && Objects.equals(getId(), user.getId()) && Objects.equals(getEmail(), user.getEmail()) && Objects.equals(getEncodedPassword(), user.getEncodedPassword()) && Objects.equals(getActivationToken(), user.getActivationToken());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getEmail(), getEncodedPassword(), isActivated(), getActivationToken());
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", encodedPassword='" + encodedPassword + '\'' +
                ", isActivated=" + isActivated +
                ", activationToken='" + activationToken + '\'' +
                '}';
    }
}
