package dev.domain;

import dev.domain.enums.SuiteStatus;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "test_suite",
        uniqueConstraints = @UniqueConstraint(name = "test_suite_unique_project_id_title", columnNames = {"title", "project_id"}))
public class TestSuite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @Enumerated(EnumType.STRING)
    private SuiteStatus status;
    private String description;
    private String preConditions;
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name="project_id", nullable = false)
    private Project project;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "testSuite")
    private List<TestPlan> testPlans;

    public TestSuite(Long id, String title, SuiteStatus status, String description, String preConditions, Project project, List<TestPlan> testPlans) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
        this.project = project;
        this.testPlans = testPlans;
    }

    public TestSuite() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SuiteStatus getStatus() {
        return status;
    }

    public void setStatus(SuiteStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<TestPlan> getTestPlans() {
        return testPlans;
    }

    public void setTestPlans(List<TestPlan> testPlan) {
        this.testPlans = testPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestSuite testSuite = (TestSuite) o;
        return Objects.equals(getId(), testSuite.getId()) && Objects.equals(getTitle(), testSuite.getTitle()) && getStatus() == testSuite.getStatus() && Objects.equals(getDescription(), testSuite.getDescription()) && Objects.equals(getPreConditions(), testSuite.getPreConditions()) && Objects.equals(getProject(), testSuite.getProject()) && Objects.equals(getTestPlans(), testSuite.getTestPlans());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getStatus(), getDescription(), getPreConditions(), getProject(), getTestPlans());
    }

    @Override
    public String toString() {
        return "TestSuite{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", project=" + project +
                ", testPlan=" + testPlans +
                '}';
    }
}
