package dev.domain;

import dev.domain.enums.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "test_case",
        uniqueConstraints = @UniqueConstraint(name = "test_case_unique_project_id_title", columnNames = {"title", "project_id"}))
public class TestCase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String title;
    @Column(length = 8, nullable = false)
    @Enumerated(EnumType.STRING)
    private CaseStatus status;
    private String description;
    private String preConditions;
    private String postConditions;
    @Column(length = 9)
    @Enumerated(EnumType.STRING)
    private CaseSeverity severity;
    @Column(length = 7)
    @Enumerated(EnumType.STRING)
    private CasePriority priority;
    @Column(length = 11)
    @Enumerated(EnumType.STRING)
    private CaseType type;
    @Column(length = 12)
    @Enumerated(EnumType.STRING)
    private CaseBehavior behavior;
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name="project_id", nullable = false)
    private Project project;
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name="test_suite_id", nullable = false)
    private TestSuite testSuite;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "test_case_id", nullable = false)
    private List<Step> steps;

    public TestCase(Long id, String title, CaseStatus status, String description, String preConditions,
                    String postConditions, CaseSeverity severity, CasePriority priority, CaseType type,
                    CaseBehavior behavior, Project project, TestSuite testSuite, List<Step> steps) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
        this.postConditions = postConditions;
        this.severity = severity;
        this.priority = priority;
        this.type = type;
        this.behavior = behavior;
        this.project = project;
        this.testSuite = testSuite;
        this.steps = steps;
    }

    public TestCase() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CaseStatus getStatus() {
        return status;
    }

    public void setStatus(CaseStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    public String getPostConditions() {
        return postConditions;
    }

    public void setPostConditions(String postConditions) {
        this.postConditions = postConditions;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public CaseSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(CaseSeverity severity) {
        this.severity = severity;
    }

    public CasePriority getPriority() {
        return priority;
    }

    public void setPriority(CasePriority priority) {
        this.priority = priority;
    }

    public CaseType getType() {
        return type;
    }

    public void setType(CaseType type) {
        this.type = type;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public TestSuite getTestSuite() {
        return testSuite;
    }

    public void setTestSuite(TestSuite testSuite) {
        this.testSuite = testSuite;
    }

    public CaseBehavior getBehavior() {
        return behavior;
    }

    public void setBehavior(CaseBehavior behavior) {
        this.behavior = behavior;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestCase testCase = (TestCase) o;
        return Objects.equals(getId(), testCase.getId()) && Objects.equals(getTitle(), testCase.getTitle()) &&
                getStatus() == testCase.getStatus() && Objects.equals(getDescription(), testCase.getDescription()) &&
                Objects.equals(getPreConditions(), testCase.getPreConditions()) &&
                Objects.equals(getPostConditions(), testCase.getPostConditions()) &&
                getSeverity() == testCase.getSeverity() && getPriority() == testCase.getPriority() &&
                getType() == testCase.getType() && getBehavior() == testCase.getBehavior() &&
                Objects.equals(getProject(), testCase.getProject()) &&
                Objects.equals(getTestSuite(), testCase.getTestSuite()) && Objects.equals(getSteps(), testCase.getSteps());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getStatus(), getDescription(), getPreConditions(), getPostConditions(),
                getSeverity(), getPriority(), getType(), getBehavior(), getProject(), getTestSuite(), getSteps());
    }

    @Override
    public String toString() {
        return "TestCase{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", postConditions='" + postConditions + '\'' +
                ", severity=" + severity +
                ", priority=" + priority +
                ", type=" + type +
                ", behavior=" + behavior +
                ", project=" + project +
                ", testSuite=" + testSuite +
                ", steps=" + steps +
                '}';
    }
}
