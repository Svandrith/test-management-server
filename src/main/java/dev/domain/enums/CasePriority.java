package dev.domain.enums;

public enum CasePriority {
    Low, Medium, High
}
