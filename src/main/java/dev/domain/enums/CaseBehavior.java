package dev.domain.enums;

public enum CaseBehavior {
    Positive, Negative, Destructive
}
