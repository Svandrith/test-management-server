package dev.domain.enums;

public enum Result {
    Passed, Failed, Skipped, Ignored, Blocked
}
