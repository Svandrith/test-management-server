package dev.domain.enums;

public enum CaseStatus {
    Active, Draft, Outdated
}
