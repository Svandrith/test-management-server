package dev.domain.enums;

public enum SuiteStatus {
    Active, Draft, Outdated
}
