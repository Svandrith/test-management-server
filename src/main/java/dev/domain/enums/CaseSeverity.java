package dev.domain.enums;

public enum CaseSeverity {
    Critical, Blocker, Medium, Low
}
