package dev.domain.enums;

public enum Sex {
    MALE,
    FEMALE;
}
