package dev.domain.enums;

public enum PlanStatus {
    Created, In_progress, Done
}
