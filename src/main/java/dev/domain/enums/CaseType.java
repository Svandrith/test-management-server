package dev.domain.enums;

public enum CaseType {
    Functional, Usability
}
