package dev.controller;

import dev.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/register/confirm")
@Tag(name = "User activation API")
public class UserActivationController {

    private final UserService userService;

    public UserActivationController(UserService userService){
        this.userService = userService;
    }

    @Operation(
            summary = "Method accepts user registration"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User activated.")
    })
    @GetMapping(value = "/{token}")
    public void acceptRegistration(@PathVariable(value = "token") String token, HttpServletResponse response) throws IOException {
        userService.acceptRegistration(token, response);
    }
}
