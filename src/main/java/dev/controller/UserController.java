package dev.controller;

import dev.dto.requests.user.RegisterUserDtoRequest;
import dev.dto.responses.user.UserDtoResponse;
import dev.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * Rest controller for user endpoints.
 */
@RestController
@Tag(name = "User API")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(
            summary = "Method registers new users"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User registered")
    })
    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void registerUser(@RequestBody @Valid RegisterUserDtoRequest registerUserDtoRequest) {
        userService.registerUser(registerUserDtoRequest);
    }

    @Operation(
            description = "Should be header Authentication : encoded by base64 <login+password>",
            summary = "Method login user"
    )
    @PostMapping(value = "/login", produces = MediaType.TEXT_PLAIN_VALUE)
    public String login(){
        return "OK";
    }

    @PostMapping(value = "/logout", produces = MediaType.TEXT_PLAIN_VALUE)
    public String logout(HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "OK";
    }

    @Operation(
            summary = "Method returns user email"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Email returned")
    })
    @GetMapping(value = "/whoami", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDtoResponse whoAmI(Authentication data){
        if (data != null) {
            return new UserDtoResponse(data.getName());
        }
        return new UserDtoResponse();
    }

    @Operation(
            summary = "Method deletes user"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User deleted")
    })
    @DeleteMapping(value = "/api/v1/users")
    public void deleteUser(Authentication data, HttpServletResponse response){
        response.setStatus(204);
        userService.deleteUser(data.getName());
    }
}
