package dev.controller;

import dev.dto.requests.project.CreateProjectDtoRequest;
import dev.dto.requests.project.UpdateProjectDtoRequest;
import dev.dto.responses.project.ProjectDtoResponse;
import dev.dto.responses.testsuite.ProjectTestSuiteWithCasesDtoResponse;
import dev.dto.responses.project.ProjectWithTestCountDtoResponse;
import dev.service.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/projects")
@Tag(name = "Project API")
public class ProjectController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService){
        this.projectService = projectService;
    }

    @Operation(
            summary = "Method posts new user project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Project added")
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDtoResponse createProject(@RequestBody @Valid CreateProjectDtoRequest request, Authentication authentication){
        return projectService.createProject(request, authentication.getName());
    }

    @Operation(
            summary = "Method deletes user project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Project deleted")
    })
    @DeleteMapping(value = "/{projectId}")
    public void deleteProject(@PathVariable("projectId") long projectId, Authentication data, HttpServletResponse response){
        response.setStatus(204);
        projectService.deleteProject(data.getName(), projectId);
    }

    /*@Operation(
            summary = "Method gets all user projects"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List returned")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDtoResponse> getAllUserProjects(Authentication data){
        return projectService.findAllUserProjects(data.getName());
    }*/

    @Operation(
            summary = "Method updates user project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Project updated")
    })
    @PutMapping(value = "/{projectId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDtoResponse updateProject(@PathVariable("projectId") long projectId,
                                            @RequestBody @Valid UpdateProjectDtoRequest request,
                                            Authentication data){
        return projectService.updateProject(projectId, data.getName(), request);
    }

    @Operation(
            summary = "Method get users project by id"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Project selected")
    })
    @GetMapping(value = "/{projectId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDtoResponse getProjectById(@PathVariable("projectId") long projectId, Authentication data){
        return projectService.findProjectById(data.getName(), projectId);
    }

    @Operation(
            summary = "Method get users projects by name"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Projects selected")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectWithTestCountDtoResponse> getUserProjectsByName(@RequestParam(value = "projectName") String projectName,
                                                          Authentication data){
        return projectService.findUserProjectsByName(data.getName(), projectName);
    }

    @Operation(
            summary = "Method gets all users projects with test case and suite count"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Projects selected")
    })
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectWithTestCountDtoResponse> getUserProjectsWithTestCount(
                                                          Authentication data){
        return projectService.findAllProjectsWithTestCasesAndSuitesCount(data.getName());
    }

    @Operation(
            summary = "Method gets all project test suites and their test cases"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test suites and cases selected")
    })
    @GetMapping(value = "/{projectId}/test-doc",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectTestSuiteWithCasesDtoResponse> getProjectTestSuitesAndCases(@PathVariable("projectId") long projectId, Authentication data){
        return projectService.findProjectTestSuiteWithCases(data.getName(), projectId);
    }

}
