package dev.controller;

import dev.dto.requests.testcase.AddTestCaseDtoRequest;
import dev.dto.requests.testcase.UpdateTestCaseDtoRequest;
import dev.dto.responses.testcase.TestCaseDtoResponse;
import dev.service.TestCaseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/projects/{projectId}/test-case")
@Tag(name = "Test Case API")
public class TestCaseController {

    private final TestCaseService testCaseService;

    public TestCaseController(TestCaseService testCaseService) {
        this.testCaseService = testCaseService;
    }

    @Operation(
            summary = "Method posts new test case to project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test case added")
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TestCaseDtoResponse addTestCase(@PathVariable("projectId") long projectId,
                                           @RequestBody @Valid AddTestCaseDtoRequest request,
                                           Authentication data) {
        return testCaseService.addTestCase(data.getName(), projectId, request);
    }

    @Operation(
            summary = "Method deletes users project test case"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Test case deleted")
    })
    @DeleteMapping(value = "/{testCaseId}")
    public void deleteTestCase(@PathVariable("projectId") long projectId,
                               @PathVariable("testCaseId") long testCaseId,
                               Authentication data,
                               HttpServletResponse response) {
        response.setStatus(204);
        testCaseService.deleteTestCase(data.getName(), testCaseId, projectId);
    }

    @Operation(
            summary = "Method finds users project test case by id"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test case returned")
    })
    @GetMapping(value = "/{testCaseId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TestCaseDtoResponse findTestCaseById(@PathVariable("projectId") long projectId,
                                                @PathVariable("testCaseId") long testCaseId,
                                                Authentication data) {
        return testCaseService.findTestCaseById(data.getName(), testCaseId, projectId);
    }

    @Operation(
            summary = "Method updates users project test case by id"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test case updated")
    })
    @PutMapping(value = "/{testCaseId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public TestCaseDtoResponse updateTestCase(@PathVariable("projectId") long projectId,
                                              @PathVariable("testCaseId") long testCaseId,
                                              @RequestBody @Valid UpdateTestCaseDtoRequest request,
                                              Authentication data) {
        return testCaseService.updateTestCase(data.getName(), testCaseId, projectId, request);
    }

    /*@Operation(
            summary = "Method deletes test case step by id"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Test case deleted")
    })
    @DeleteMapping(value = "/{testCaseId}/{stepToDeleteId}")
    public void deleteTestStep(@PathVariable("projectId") long projectId,
                               @PathVariable("testCaseId") long testCaseId,
                               @PathVariable("stepToDeleteId") long stepId,
                               Authentication data, HttpServletResponse response) {
        testCaseService.deleteTestCaseStep(data.getName(), projectId, testCaseId, stepId);
        response.setStatus(204);
    }*/
}
