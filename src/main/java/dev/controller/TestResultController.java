package dev.controller;

import dev.dto.requests.testresult.AddTestResultDtoRequest;
import dev.dto.requests.testresult.UpdateTestResultDtoRequest;
import dev.dto.responses.testresult.TestResultDtoResponse;
import dev.service.TestResultService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/projects/{projectId}/test-case/{testCaseId}/test-result")
@Tag(name = "Test Result API")
public class TestResultController {
    private final TestResultService testResultService;

    public TestResultController(TestResultService testResultService) {
        this.testResultService = testResultService;
    }

    @Operation(
            summary = "Method posts new test result to test case"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test case result added")
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TestResultDtoResponse addTestResult(@PathVariable("projectId") long projectId,
                                               @PathVariable("testCaseId") long testCaseId,
                                               @RequestBody @Valid AddTestResultDtoRequest request,
                                               Authentication data) {
        return testResultService.addTestResult(data.getName(), projectId, testCaseId, request);
    }

    @Operation(
            summary = "Method updates test case result"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test case result updated")
    })
    @PutMapping(value = "/{testResultId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public TestResultDtoResponse updateTestResult(@PathVariable("projectId") long projectId,
                                               @PathVariable("testCaseId") long testCaseId,
                                               @PathVariable("testResultId") long testResultId,
                                               @RequestBody @Valid UpdateTestResultDtoRequest request,
                                               Authentication data) {
        return testResultService.updateTestResult(data.getName(), projectId, testCaseId, testResultId, request);
    }

    @Operation(
            summary = "Method deletes test case result"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Test case result deleted")
    })
    @DeleteMapping(value = "/{testResultId}")
    public void deleteTestResult(@PathVariable("projectId") long projectId,
                               @PathVariable("testCaseId") long testCaseId,
                               @PathVariable("testResultId") long testResultId,
                               Authentication data, HttpServletResponse response) {
        testResultService.deleteTestResult(data.getName(), projectId, testCaseId, testResultId);
        response.setStatus(204);
    }

    @Operation(
            summary = "Method finds test case result by id"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test case result found")
    })
    @GetMapping(value = "/{testResultId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TestResultDtoResponse findTestResultById(@PathVariable("projectId") long projectId,
                                                @PathVariable("testCaseId") long testCaseId,
                                                @PathVariable("testResultId") long testResultId,
                                                Authentication data) {
        return testResultService.findTestResultById(data.getName(), projectId, testCaseId, testResultId);
    }

    @Operation(
            summary = "Method finds all test case results"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test case results found")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TestResultDtoResponse> findAllTestCaseResults(@PathVariable("projectId") long projectId,
                                                        @PathVariable("testCaseId") long testCaseId,
                                                        Authentication data) {
        return testResultService.findAllTestCaseResults(data.getName(), projectId, testCaseId);
    }
}
