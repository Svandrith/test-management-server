package dev.controller;

import dev.dto.requests.testPlanCaseResult.TestPlanCaseDtoRequest;
import dev.dto.requests.testplan.AddTestPlanDtoRequest;
import dev.dto.requests.testplan.UpdateTestPlanDtoRequest;
import dev.dto.responses.testplan.TestPlanDtoResponse;
import dev.dto.responses.testplan.TestPlanWithShortTestCasesDtoResponse;
import dev.service.TestPlanService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/projects/{projectId}/test-plan")
@Tag(name = "Test Plan API")
public class TestPlanController {
    private final TestPlanService testPlanService;

    public TestPlanController(TestPlanService testPlanService) {
        this.testPlanService = testPlanService;
    }

    @Operation(
            summary = "Method posts new test plan to project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test plan added")
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TestPlanDtoResponse addTestPlan(@PathVariable("projectId") long projectId,
                                           @RequestBody @Valid AddTestPlanDtoRequest request,
                                           Authentication data) {
        return testPlanService.addTestPlan(data.getName(), projectId, request);
    }

    @Operation(
            summary = "Method updates test plan from project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test plan updated")
    })
    @PutMapping(value = "/{testPlanId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public TestPlanDtoResponse updateTestPlan(@PathVariable("projectId") long projectId,
                                              @PathVariable("testPlanId") long testPlanId,
                                              @RequestBody UpdateTestPlanDtoRequest request,
                                              Authentication data) {
        return testPlanService.updateTestPlan(data.getName(), projectId, testPlanId, request);
    }

    @Operation(
            summary = "Method deletes test plan from project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Test plan deleted")
    })
    @DeleteMapping(value = "/{testPlanId}")
    public void deleteTestPlan(@PathVariable("projectId") long projectId,
                               @PathVariable("testPlanId") long testPlanId,
                               Authentication data, HttpServletResponse response) {
        testPlanService.deleteTestPlan(data.getName(), projectId, testPlanId);
        response.setStatus(204);
    }

    @Operation(
            summary = "Method finds test plan by id from project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test plan found")
    })
    @GetMapping(value = "/{testPlanId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TestPlanWithShortTestCasesDtoResponse findTestPlanById(@PathVariable("projectId") long projectId,
                                                                  @PathVariable("testPlanId") long testPlanId,
                                                                  Authentication data) {
        return testPlanService.findTestPlanById(data.getName(), projectId, testPlanId);
    }

    @Operation(
            summary = "Method finds all test plans from project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test plans found")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TestPlanDtoResponse> findTestPlanById(@PathVariable("projectId") long projectId,
                                                      Authentication data) {
        return testPlanService.findAllProjectTestPlan(data.getName(), projectId);
    }

    @Operation(
            summary = "Method add test plan case result"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test plan case result created")
    })
    @PostMapping(value = "/{testPlanId}/result/{testCaseId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void addTestPlanCaseResult(@PathVariable("projectId") long projectId,
                                      @PathVariable("testCaseId") long testCaseId,
                                      @PathVariable("testPlanId") long testPlanId,
                                      @RequestBody @Valid TestPlanCaseDtoRequest request,
                                      Authentication data) {
        testPlanService.addTestPlanCaseResult(data.getName(), projectId, testCaseId, testPlanId, request);
    }

    @Operation(
            summary = "Method add test plan case result"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test plan case result created")
    })
    @PutMapping(value = "/{testPlanId}/result/{testCaseId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateTestPlanCaseResult(@PathVariable("projectId") long projectId,
                                      @PathVariable("testCaseId") long testCaseId,
                                      @PathVariable("testPlanId") long testPlanId,
                                      @RequestBody @Valid TestPlanCaseDtoRequest request,
                                      Authentication data) {
        testPlanService.updateTestPlanCaseResult(data.getName(), projectId, testCaseId, testPlanId, request);
    }
}
