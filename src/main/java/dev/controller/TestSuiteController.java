package dev.controller;

import dev.dto.requests.testsuite.AddTestSuiteDtoRequest;
import dev.dto.requests.testsuite.UpdateTestSuiteDtoRequest;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;
import dev.service.TestSuiteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/projects/{projectId}/test-suite")
@Tag(name = "Test Suite API")
public class TestSuiteController {
    private final TestSuiteService testSuiteService;

    public TestSuiteController(TestSuiteService testSuiteService) {
        this.testSuiteService = testSuiteService;
    }

    @Operation(
            summary = "Method posts new test suite to project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test suite added")
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TestSuiteDtoResponse addTestSuite(@PathVariable("projectId") long projectId,
                                             @RequestBody @Valid AddTestSuiteDtoRequest request,
                                             Authentication data) {
        return testSuiteService.addTestSuite(data.getName(), projectId, request);
    }

    @Operation(
            summary = "Method returns all test suites from project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test suites returned")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TestSuiteDtoResponse> findAllTestSuitesByProject(@PathVariable("projectId") long projectId,
                                                                 Authentication data) {
        return testSuiteService.findAllTestSuitesByProject(data.getName(), projectId);
    }

    @Operation(
            summary = "Method updates test suite from project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test suite updated")
    })
    @PutMapping(value = "/{testSuiteId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public TestSuiteDtoResponse updateTestSuite(@PathVariable("projectId") long projectId,
                                                @PathVariable("testSuiteId") long testSuiteId,
                                                @RequestBody UpdateTestSuiteDtoRequest request,
                                                Authentication data) {
        return testSuiteService.updateTestSuite(data.getName(), projectId, testSuiteId, request);
    }

    @Operation(
            summary = "Method deletes test suite from project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Test suite deleted")
    })
    @DeleteMapping(value = "/{testSuiteId}")
    public void deleteTestSuite(@PathVariable("projectId") long projectId,
                                @PathVariable("testSuiteId") long testSuiteId,
                                Authentication data, HttpServletResponse response) {
        testSuiteService.deleteTestSuite(data.getName(), projectId, testSuiteId);
        response.setStatus(204);
    }

    @Operation(
            summary = "Method finds test suite by id from project"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Test suite found")
    })
    @GetMapping(value = "/{testSuiteId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TestSuiteDtoResponse findTestSuiteById(@PathVariable("projectId") long projectId,
                                                  @PathVariable("testSuiteId") long testSuiteId,
                                                  Authentication data) {
        return testSuiteService.findTestSuiteById(data.getName(), projectId, testSuiteId);
    }
}
