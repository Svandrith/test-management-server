package dev.validator;

import dev.tools.AppPropertiesUtils;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Service
public class CorrectPasswordValidator implements ConstraintValidator<CorrectPassword, String> {

    private AppPropertiesUtils appPropertiesUtils;

    public CorrectPasswordValidator(AppPropertiesUtils appPropertiesUtils){
        this.appPropertiesUtils = appPropertiesUtils;
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        if(password == null){
            return false;
        }
        int passwordLength = password.length();
        return passwordLength >= appPropertiesUtils.getMinPasswordLength() && passwordLength <= appPropertiesUtils.getMaxPasswordLength();
    }
}
