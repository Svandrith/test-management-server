package dev.validator;

import dev.tools.AppPropertiesUtils;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Service
public class CorrectMailValidator implements ConstraintValidator<CorrectMail, String> {

    private AppPropertiesUtils appPropertiesUtils;

    public CorrectMailValidator(AppPropertiesUtils appPropertiesUtils){
        this.appPropertiesUtils = appPropertiesUtils;
    }

    @Override
    public boolean isValid(String mail, ConstraintValidatorContext context) {
        if(mail == null){
            return false;
        }
        int mailLength = mail.length();
        return mailLength >= appPropertiesUtils.getMinMailLength() && mailLength <= appPropertiesUtils.getMaxMailLength();
    }
}