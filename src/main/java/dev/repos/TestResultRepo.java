package dev.repos;

import dev.domain.TestCase;
import dev.domain.TestResult;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TestResultRepo extends CrudRepository<TestResult, Long> {
    Optional<TestResult> findByIdAndTestCase(long testResultId, TestCase testCase);

    Optional<List<TestResult>> findAllByTestCase(TestCase testCase);

    Optional<List<TestResult>> findAllByTestCaseOrderByIdDesc(TestCase testCase);
}
