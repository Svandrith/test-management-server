package dev.repos;

import dev.domain.Step;
import org.springframework.data.repository.CrudRepository;

public interface StepRepo extends CrudRepository<Step, Long> {
}
