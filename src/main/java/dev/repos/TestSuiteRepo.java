package dev.repos;

import dev.domain.Project;
import dev.domain.TestSuite;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TestSuiteRepo extends CrudRepository<TestSuite, Long> {
    Optional<Integer> countAllByProject(Project project);
    Optional<List<TestSuite>> findAllByProject(Project project);
    Optional<TestSuite> findByIdAndProject(long testSuiteId, Project project);
}
