package dev.repos;

import dev.domain.Project;
import dev.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProjectRepo extends CrudRepository<Project, Long> {
    Optional<Project> findByIdAndUser(long projectId, User user);

    Optional<List<Project>> findAllByUser(User user);

    Optional<List<Project>> findAllByUserOrderByIdDesc(User user);

    Optional<List<Project>> findByUserAndNameLikeIgnoreCase(User user, String name);
}
