package dev.repos;

import dev.domain.Project;
import dev.domain.TestPlan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TestPlanRepo extends CrudRepository<TestPlan, Long> {
    Optional<TestPlan> findByIdAndProject(long testPlanId, Project project);

    Optional<List<TestPlan>> findAllByProjectOrderByLastUpdateDateDesc(Project project);
}
