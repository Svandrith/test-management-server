package dev.repos;

import dev.domain.PlanCaseId;
import dev.domain.PlanCaseResult;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PlanCaseResultRepo extends CrudRepository<PlanCaseResult, PlanCaseId> {
}
