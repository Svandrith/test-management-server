package dev.repos;

import dev.domain.Project;
import dev.domain.TestCase;
import dev.domain.TestSuite;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TestCaseRepo extends CrudRepository<TestCase, Long> {
    Optional<Integer> countAllByProject(Project project);

    List<TestCase> findAllByTestSuite(TestSuite testSuite);
}
