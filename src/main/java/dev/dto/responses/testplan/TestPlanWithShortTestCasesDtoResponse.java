package dev.dto.responses.testplan;

import dev.domain.enums.PlanStatus;
import dev.dto.responses.testsuite.TestSuiteWithCasesWithoutStepsDtoResponse;

import java.util.List;
import java.util.Objects;

public class TestPlanWithShortTestCasesDtoResponse {
    private long id;
    private String name;
    private String description;
    private PlanStatus status;
    private long projectId;
    private List<TestSuiteWithCasesWithoutStepsDtoResponse> testSuites;

    public TestPlanWithShortTestCasesDtoResponse(long id, String name, String description, PlanStatus status, long projectId, List<TestSuiteWithCasesWithoutStepsDtoResponse> testSuites) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.projectId = projectId;
        this.testSuites = testSuites;
    }

    public TestPlanWithShortTestCasesDtoResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlanStatus getStatus() {
        return status;
    }

    public void setStatus(PlanStatus status) {
        this.status = status;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public List<TestSuiteWithCasesWithoutStepsDtoResponse> getTestSuites() {
        return testSuites;
    }

    public void setTestSuites(List<TestSuiteWithCasesWithoutStepsDtoResponse> testSuites) {
        this.testSuites = testSuites;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestPlanWithShortTestCasesDtoResponse that = (TestPlanWithShortTestCasesDtoResponse) o;
        return getId() == that.getId() && getProjectId() == that.getProjectId() && Objects.equals(getName(), that.getName()) && Objects.equals(getDescription(), that.getDescription()) && getStatus() == that.getStatus() && Objects.equals(getTestSuites(), that.getTestSuites());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getStatus(), getProjectId(), getTestSuites());
    }

    @Override
    public String toString() {
        return "TestPlanWithShortTestCasesDtoResponse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", projectId=" + projectId +
                ", testSuites=" + testSuites +
                '}';
    }
}
