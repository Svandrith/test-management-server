package dev.dto.responses.testplan;

import dev.domain.enums.PlanStatus;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;

import java.util.List;
import java.util.Objects;

public class TestPlanDtoResponse {
    private long id;
    private String name;
    private String description;
    private PlanStatus status;
    private long projectId;
    private List<TestSuiteDtoResponse> testSuites;

    public TestPlanDtoResponse(long id, String name, String description, PlanStatus status, long projectId, List<TestSuiteDtoResponse> testSuites) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.projectId = projectId;
        this.testSuites = testSuites;
    }

    public TestPlanDtoResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlanStatus getStatus() {
        return status;
    }

    public void setStatus(PlanStatus status) {
        this.status = status;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public List<TestSuiteDtoResponse> getTestSuites() {
        return testSuites;
    }

    public void setTestSuites(List<TestSuiteDtoResponse> testSuites) {
        this.testSuites = testSuites;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestPlanDtoResponse response = (TestPlanDtoResponse) o;
        return getId() == response.getId() && getProjectId() == response.getProjectId() && Objects.equals(getName(), response.getName()) && Objects.equals(getDescription(), response.getDescription()) && getStatus() == response.getStatus() && Objects.equals(getTestSuites(), response.getTestSuites());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getStatus(), getProjectId(), getTestSuites());
    }

    @Override
    public String toString() {
        return "TestPlanDtoResponse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", projectId=" + projectId +
                ", testSuites=" + testSuites +
                '}';
    }
}
