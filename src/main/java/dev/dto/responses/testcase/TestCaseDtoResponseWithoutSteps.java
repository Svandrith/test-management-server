package dev.dto.responses.testcase;

import dev.domain.enums.*;

import java.util.Objects;

public class TestCaseDtoResponseWithoutSteps {
    private long id;
    private String title;
    private CaseStatus caseStatus;
    private Result result;
    private String description;
    private String preConditions;
    private String postConditions;
    private CaseSeverity severity;
    private CasePriority priority;
    private CaseType type;
    private CaseBehavior behavior;
    private long projectId;
    private long testSuiteId;

    public TestCaseDtoResponseWithoutSteps(long id, String title, CaseStatus caseStatus, Result result, String description, String preConditions, String postConditions, CaseSeverity severity, CasePriority priority, CaseType type, CaseBehavior behavior, long projectId, long testSuiteId) {
        this.id = id;
        this.title = title;
        this.caseStatus = caseStatus;
        this.result = result;
        this.description = description;
        this.preConditions = preConditions;
        this.postConditions = postConditions;
        this.severity = severity;
        this.priority = priority;
        this.type = type;
        this.behavior = behavior;
        this.projectId = projectId;
        this.testSuiteId = testSuiteId;
    }

    public TestCaseDtoResponseWithoutSteps() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CaseStatus getCaseStatus() {
        return caseStatus;
    }

    public void setCaseStatus(CaseStatus caseStatus) {
        this.caseStatus = caseStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    public String getPostConditions() {
        return postConditions;
    }

    public void setPostConditions(String postConditions) {
        this.postConditions = postConditions;
    }

    public CaseSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(CaseSeverity severity) {
        this.severity = severity;
    }

    public CasePriority getPriority() {
        return priority;
    }

    public void setPriority(CasePriority priority) {
        this.priority = priority;
    }

    public CaseType getType() {
        return type;
    }

    public void setType(CaseType type) {
        this.type = type;
    }

    public CaseBehavior getBehavior() {
        return behavior;
    }

    public void setBehavior(CaseBehavior behavior) {
        this.behavior = behavior;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getTestSuiteId() {
        return testSuiteId;
    }

    public void setTestSuiteId(long testSuiteId) {
        this.testSuiteId = testSuiteId;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestCaseDtoResponseWithoutSteps that = (TestCaseDtoResponseWithoutSteps) o;
        return getId() == that.getId() && getProjectId() == that.getProjectId() && getTestSuiteId() == that.getTestSuiteId() && Objects.equals(getTitle(), that.getTitle()) && getCaseStatus() == that.getCaseStatus() && getResult() == that.getResult() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getPreConditions(), that.getPreConditions()) && Objects.equals(getPostConditions(), that.getPostConditions()) && getSeverity() == that.getSeverity() && getPriority() == that.getPriority() && getType() == that.getType() && getBehavior() == that.getBehavior();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getCaseStatus(), getResult(), getDescription(), getPreConditions(), getPostConditions(), getSeverity(), getPriority(), getType(), getBehavior(), getProjectId(), getTestSuiteId());
    }

    @Override
    public String toString() {
        return "TestCaseDtoResponseWithoutSteps{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", caseStatus=" + caseStatus +
                ", result=" + result +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", postConditions='" + postConditions + '\'' +
                ", severity=" + severity +
                ", priority=" + priority +
                ", type=" + type +
                ", behavior=" + behavior +
                ", projectId=" + projectId +
                ", testSuiteId=" + testSuiteId +
                '}';
    }
}
