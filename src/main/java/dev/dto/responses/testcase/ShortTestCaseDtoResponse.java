package dev.dto.responses.testcase;

import dev.domain.enums.*;

import java.util.List;
import java.util.Objects;

public class ShortTestCaseDtoResponse {
    private long id;
    private String title;
    private CaseStatus status;
    private String description;
    private String preConditions;
    private String postConditions;
    private List<StepDtoResponse> steps;
    private CaseSeverity severity;
    private CasePriority priority;
    private CaseType type;
    private CaseBehavior behavior;

    public ShortTestCaseDtoResponse(long id, String title, CaseStatus status, String description, String preConditions, String postConditions, List<StepDtoResponse> steps, CaseSeverity severity, CasePriority priority, CaseType type, CaseBehavior behavior) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
        this.postConditions = postConditions;
        this.steps = steps;
        this.severity = severity;
        this.priority = priority;
        this.type = type;
        this.behavior = behavior;
    }

    public ShortTestCaseDtoResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CaseStatus getStatus() {
        return status;
    }

    public void setStatus(CaseStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    public String getPostConditions() {
        return postConditions;
    }

    public void setPostConditions(String postConditions) {
        this.postConditions = postConditions;
    }

    public List<StepDtoResponse> getSteps() {
        return steps;
    }

    public void setSteps(List<StepDtoResponse> steps) {
        this.steps = steps;
    }

    public CaseSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(CaseSeverity severity) {
        this.severity = severity;
    }

    public CasePriority getPriority() {
        return priority;
    }

    public void setPriority(CasePriority priority) {
        this.priority = priority;
    }

    public CaseType getType() {
        return type;
    }

    public void setType(CaseType type) {
        this.type = type;
    }

    public CaseBehavior getBehavior() {
        return behavior;
    }

    public void setBehavior(CaseBehavior behavior) {
        this.behavior = behavior;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShortTestCaseDtoResponse that = (ShortTestCaseDtoResponse) o;
        return getId() == that.getId() && Objects.equals(getTitle(), that.getTitle()) && getStatus() == that.getStatus() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getPreConditions(), that.getPreConditions()) && Objects.equals(getPostConditions(), that.getPostConditions()) && Objects.equals(getSteps(), that.getSteps()) && getSeverity() == that.getSeverity() && getPriority() == that.getPriority() && getType() == that.getType() && getBehavior() == that.getBehavior();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getStatus(), getDescription(), getPreConditions(), getPostConditions(), getSteps(), getSeverity(), getPriority(), getType(), getBehavior());
    }

    @Override
    public String toString() {
        return "ShortTestCaseDtoResponse{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", postConditions='" + postConditions + '\'' +
                ", steps=" + steps +
                ", severity=" + severity +
                ", priority=" + priority +
                ", type=" + type +
                ", behavior=" + behavior +
                '}';
    }
}
