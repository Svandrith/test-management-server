package dev.dto.responses.testcase;

import java.util.Objects;

public class StepDtoResponse {
    private long id;
    private String stepDescription;
    private String expectedResult;

    public StepDtoResponse(long id, String stepDescription, String expectedResult) {
        this.id = id;
        this.stepDescription = stepDescription;
        this.expectedResult = expectedResult;
    }

    public StepDtoResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStepDescription() {
        return stepDescription;
    }

    public void setStepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StepDtoResponse response = (StepDtoResponse) o;
        return getId() == response.getId() && Objects.equals(getStepDescription(), response.getStepDescription()) && Objects.equals(getExpectedResult(), response.getExpectedResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getStepDescription(), getExpectedResult());
    }

    @Override
    public String toString() {
        return "StepDtoResponse{" +
                "id=" + id +
                ", description='" + stepDescription + '\'' +
                ", expectedResult='" + expectedResult + '\'' +
                '}';
    }
}
