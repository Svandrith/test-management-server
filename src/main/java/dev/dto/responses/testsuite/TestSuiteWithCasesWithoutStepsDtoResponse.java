package dev.dto.responses.testsuite;

import dev.domain.enums.SuiteStatus;
import dev.dto.responses.testcase.ShortTestCaseDtoResponse;
import dev.dto.responses.testcase.TestCaseDtoResponseWithoutSteps;

import java.util.List;
import java.util.Objects;

public class TestSuiteWithCasesWithoutStepsDtoResponse {
    private long suiteId;
    private String title;
    private SuiteStatus status;
    private String description;
    private String preConditions;
    private List<TestCaseDtoResponseWithoutSteps> children;

    public TestSuiteWithCasesWithoutStepsDtoResponse(long suiteId, String title, SuiteStatus status, String description, String preConditions, List<TestCaseDtoResponseWithoutSteps> children) {
        this.suiteId = suiteId;
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
        this.children = children;
    }

    public TestSuiteWithCasesWithoutStepsDtoResponse() {
    }

    public long getSuiteId() {
        return suiteId;
    }

    public void setSuiteId(long suiteId) {
        this.suiteId = suiteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SuiteStatus getStatus() {
        return status;
    }

    public void setStatus(SuiteStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    public List<TestCaseDtoResponseWithoutSteps> getChildren() {
        return children;
    }

    public void setTestCases(List<TestCaseDtoResponseWithoutSteps> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestSuiteWithCasesWithoutStepsDtoResponse that = (TestSuiteWithCasesWithoutStepsDtoResponse) o;
        return getSuiteId() == that.getSuiteId() && Objects.equals(getTitle(), that.getTitle()) && getStatus() == that.getStatus() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getPreConditions(), that.getPreConditions()) && Objects.equals(getChildren(), that.getChildren());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSuiteId(), getTitle(), getStatus(), getDescription(), getPreConditions(), getChildren());
    }

    @Override
    public String toString() {
        return "TestSuiteWithCasesWithoutStepsDtoResponse{" +
                "suiteId=" + suiteId +
                ", title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", children=" + children +
                '}';
    }
}
