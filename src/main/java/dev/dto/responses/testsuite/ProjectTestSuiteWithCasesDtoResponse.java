package dev.dto.responses.testsuite;

import dev.domain.enums.SuiteStatus;
import dev.dto.responses.testcase.ShortTestCaseDtoResponse;

import java.util.List;
import java.util.Objects;

public class ProjectTestSuiteWithCasesDtoResponse {
    private long suiteId;
    private String title;
    private SuiteStatus status;
    private String description;
    private String preConditions;
    private List<ShortTestCaseDtoResponse> testCases;

    public ProjectTestSuiteWithCasesDtoResponse(long suiteId, String title, SuiteStatus status, String description, String preConditions, List<ShortTestCaseDtoResponse> testCases) {
        this.suiteId = suiteId;
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
        this.testCases = testCases;
    }

    public ProjectTestSuiteWithCasesDtoResponse() {
    }

    public long getSuiteId() {
        return suiteId;
    }

    public void setSuiteId(long suiteId) {
        this.suiteId = suiteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ShortTestCaseDtoResponse> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<ShortTestCaseDtoResponse> testCases) {
        this.testCases = testCases;
    }

    public SuiteStatus getStatus() {
        return status;
    }

    public void setStatus(SuiteStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectTestSuiteWithCasesDtoResponse that = (ProjectTestSuiteWithCasesDtoResponse) o;
        return getSuiteId() == that.getSuiteId() && Objects.equals(getTitle(), that.getTitle()) && getStatus() == that.getStatus() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getPreConditions(), that.getPreConditions()) && Objects.equals(getTestCases(), that.getTestCases());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSuiteId(), getTitle(), getStatus(), getDescription(), getPreConditions(), getTestCases());
    }

    @Override
    public String toString() {
        return "ProjectTestSuiteWithCasesDtoResponse{" +
                "suiteId=" + suiteId +
                ", title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", testCases=" + testCases +
                '}';
    }
}
