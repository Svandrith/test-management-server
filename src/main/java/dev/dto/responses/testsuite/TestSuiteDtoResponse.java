package dev.dto.responses.testsuite;

import dev.domain.enums.SuiteStatus;

import java.util.Objects;

public class TestSuiteDtoResponse {
    private long id;
    private String title;
    private SuiteStatus status;
    private String description;
    private String preConditions;
    private long projectId;

    public TestSuiteDtoResponse(long id, String title, SuiteStatus status, String description, String preConditions, long projectId) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
        this.projectId = projectId;
    }

    public TestSuiteDtoResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SuiteStatus getStatus() {
        return status;
    }

    public void setStatus(SuiteStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestSuiteDtoResponse that = (TestSuiteDtoResponse) o;
        return getId() == that.getId() && getProjectId() == that.getProjectId() && Objects.equals(getTitle(), that.getTitle()) && getStatus() == that.getStatus() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getPreConditions(), that.getPreConditions());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getStatus(), getDescription(), getPreConditions(), getProjectId());
    }

    @Override
    public String toString() {
        return "TestSuiteDtoResponse{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", projectId=" + projectId +
                '}';
    }
}
