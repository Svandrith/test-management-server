package dev.dto.responses.testresult;

import dev.domain.enums.Result;

import java.time.LocalDateTime;
import java.util.Objects;

public class TestResultDtoResponse {
    private Long id;
    private LocalDateTime testDate;
    private Result result;
    private String comment;
    private long testCaseId;

    public TestResultDtoResponse(Long id, LocalDateTime testDate, Result result, String comment, long testCaseId) {
        this.id = id;
        this.testDate = testDate;
        this.result = result;
        this.comment = comment;
        this.testCaseId = testCaseId;
    }

    public TestResultDtoResponse() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTestDate() {
        return testDate;
    }

    public void setTestDate(LocalDateTime testDate) {
        this.testDate = testDate;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(long testCaseId) {
        this.testCaseId = testCaseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestResultDtoResponse that = (TestResultDtoResponse) o;
        return getTestCaseId() == that.getTestCaseId() && Objects.equals(getId(), that.getId()) && Objects.equals(getTestDate(), that.getTestDate()) && getResult() == that.getResult() && Objects.equals(getComment(), that.getComment());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTestDate(), getResult(), getComment(), getTestCaseId());
    }

    @Override
    public String toString() {
        return "TestResultDtoResponse{" +
                "id=" + id +
                ", testDate=" + testDate +
                ", result=" + result +
                ", comment='" + comment + '\'' +
                ", testCaseId=" + testCaseId +
                '}';
    }
}
