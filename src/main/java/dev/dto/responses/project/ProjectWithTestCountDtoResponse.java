package dev.dto.responses.project;

import java.util.Objects;

public class ProjectWithTestCountDtoResponse {
    private Long id;
    private String name;
    private String description;
    private int testCaseCount;
    private int testSuiteCount;

    public ProjectWithTestCountDtoResponse(Long id, String name, String description, int testCaseCount, int testSuiteCount) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.testCaseCount = testCaseCount;
        this.testSuiteCount = testSuiteCount;
    }

    public ProjectWithTestCountDtoResponse() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getTestCaseCount() {
        return testCaseCount;
    }

    public int getTestSuiteCount() {
        return testSuiteCount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTestCaseCount(int testCaseCount) {
        this.testCaseCount = testCaseCount;
    }

    public void setTestSuiteCount(int testSuiteCount) {
        this.testSuiteCount = testSuiteCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectWithTestCountDtoResponse that = (ProjectWithTestCountDtoResponse) o;
        return getTestCaseCount() == that.getTestCaseCount() && getTestSuiteCount() == that.getTestSuiteCount() && Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName()) && Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getDescription(), getTestCaseCount(), getTestSuiteCount());
    }

    @Override
    public String toString() {
        return "ProjectWithTestCountDtoResponse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", testCaseCount=" + testCaseCount +
                ", testSuiteCount=" + testSuiteCount +
                '}';
    }
}
