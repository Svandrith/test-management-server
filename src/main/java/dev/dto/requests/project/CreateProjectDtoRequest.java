package dev.dto.requests.project;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

public class CreateProjectDtoRequest {
    @NotBlank
    private String name;

    private String description;

    public CreateProjectDtoRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public CreateProjectDtoRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateProjectDtoRequest that = (CreateProjectDtoRequest) o;
        return Objects.equals(name, that.name) && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }
}
