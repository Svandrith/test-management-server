package dev.dto.requests.project;

import java.util.Objects;

public class UpdateProjectDtoRequest {
    private String name;
    private String description;

    public UpdateProjectDtoRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public UpdateProjectDtoRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateProjectDtoRequest that = (UpdateProjectDtoRequest) o;
        return Objects.equals(getName(), that.getName()) && Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription());
    }
}
