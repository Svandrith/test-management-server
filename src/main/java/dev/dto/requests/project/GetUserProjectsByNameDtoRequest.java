package dev.dto.requests.project;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

public class GetUserProjectsByNameDtoRequest {
    @NotBlank
    private String name;

    public GetUserProjectsByNameDtoRequest(String name) {
        this.name = name;
    }

    public GetUserProjectsByNameDtoRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetUserProjectsByNameDtoRequest that = (GetUserProjectsByNameDtoRequest) o;
        return Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public String toString() {
        return "GetUserProjectsByNameDtoRequest{" +
                "name='" + name + '\'' +
                '}';
    }
}
