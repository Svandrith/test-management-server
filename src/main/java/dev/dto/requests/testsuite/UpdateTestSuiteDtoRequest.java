package dev.dto.requests.testsuite;

import dev.domain.enums.SuiteStatus;

import java.util.Objects;

public class UpdateTestSuiteDtoRequest {
    private String title;
    private SuiteStatus status;
    private String description;
    private String preConditions;

    public UpdateTestSuiteDtoRequest(String title, SuiteStatus status, String description, String preConditions) {
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
    }

    public UpdateTestSuiteDtoRequest() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SuiteStatus getStatus() {
        return status;
    }

    public void setStatus(SuiteStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateTestSuiteDtoRequest that = (UpdateTestSuiteDtoRequest) o;
        return Objects.equals(getTitle(), that.getTitle()) && getStatus() == that.getStatus() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getPreConditions(), that.getPreConditions());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getStatus(), getDescription(), getPreConditions());
    }

    @Override
    public String toString() {
        return "UpdateTestSuiteDtoRequest{" +
                "title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                '}';
    }
}
