package dev.dto.requests.testcase;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

public class StepDtoRequest {
    @NotBlank
    private String stepDescription;
    @NotBlank
    private String expectedResult;

    public StepDtoRequest(String stepDescription, String expectedResult) {
        this.stepDescription = stepDescription;
        this.expectedResult = expectedResult;
    }

    public StepDtoRequest() {
    }

    public String getStepDescription() {
        return stepDescription;
    }

    public void setStepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StepDtoRequest that = (StepDtoRequest) o;
        return Objects.equals(getStepDescription(), that.getStepDescription()) && Objects.equals(getExpectedResult(), that.getExpectedResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStepDescription(), getExpectedResult());
    }

    @Override
    public String toString() {
        return "StepDtoRequest{" +
                "description='" + stepDescription + '\'' +
                ", expectedResult='" + expectedResult + '\'' +
                '}';
    }
}
