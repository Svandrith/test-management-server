package dev.dto.requests.testcase;

import dev.domain.enums.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

public class UpdateTestCaseDtoRequest {
    private String title;
    private CaseStatus status;
    private String description;
    private String preConditions;
    private String postConditions;
    private CaseSeverity severity;
    private CasePriority priority;
    private CaseType type;
    private CaseBehavior behavior;
    private Long testSuiteId;
    @Valid
    private List<UpdateStepDtoRequest> steps;

    public UpdateTestCaseDtoRequest(String title, CaseStatus status, String description, String preConditions, String postConditions, CaseSeverity severity, CasePriority priority, CaseType type, CaseBehavior behavior, Long testSuiteId, List<UpdateStepDtoRequest> steps) {
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
        this.postConditions = postConditions;
        this.severity = severity;
        this.priority = priority;
        this.type = type;
        this.behavior = behavior;
        this.testSuiteId = testSuiteId;
        this.steps = steps;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CaseStatus getStatus() {
        return status;
    }

    public void setStatus(CaseStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    public String getPostConditions() {
        return postConditions;
    }

    public void setPostConditions(String postConditions) {
        this.postConditions = postConditions;
    }

    public CaseSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(CaseSeverity severity) {
        this.severity = severity;
    }

    public CasePriority getPriority() {
        return priority;
    }

    public void setPriority(CasePriority priority) {
        this.priority = priority;
    }

    public CaseType getType() {
        return type;
    }

    public void setType(CaseType type) {
        this.type = type;
    }

    public CaseBehavior getBehavior() {
        return behavior;
    }

    public void setBehavior(CaseBehavior behavior) {
        this.behavior = behavior;
    }

    public Long getTestSuiteId() {
        return testSuiteId;
    }

    public void setTestSuiteId(Long testSuite) {
        this.testSuiteId = testSuite;
    }

    public List<UpdateStepDtoRequest> getSteps() {
        return steps;
    }

    public void setSteps(List<UpdateStepDtoRequest> steps) {
        this.steps = steps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateTestCaseDtoRequest that = (UpdateTestCaseDtoRequest) o;
        return Objects.equals(getTitle(), that.getTitle()) && getStatus() == that.getStatus() && Objects.equals(getDescription(), that.getDescription()) && Objects.equals(getPreConditions(), that.getPreConditions()) && Objects.equals(getPostConditions(), that.getPostConditions()) && getSeverity() == that.getSeverity() && getPriority() == that.getPriority() && getType() == that.getType() && getBehavior() == that.getBehavior() && Objects.equals(testSuiteId, that.testSuiteId) && Objects.equals(getSteps(), that.getSteps());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getStatus(), getDescription(), getPreConditions(), getPostConditions(), getSeverity(), getPriority(), getType(), getBehavior(), testSuiteId, getSteps());
    }

    @Override
    public String toString() {
        return "UpdateTestCaseDtoRequest{" +
                "title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", postConditions='" + postConditions + '\'' +
                ", severity=" + severity +
                ", priority=" + priority +
                ", type=" + type +
                ", behavior=" + behavior +
                ", testSuiteId=" + testSuiteId +
                ", steps=" + steps +
                '}';
    }
}