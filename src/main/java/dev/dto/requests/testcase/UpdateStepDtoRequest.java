package dev.dto.requests.testcase;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class UpdateStepDtoRequest {
    @NotNull
    private long id;
    private String stepDescription;
    private String expectedResult;

    public UpdateStepDtoRequest(long id, String stepDescription, String expectedResult) {
        this.id = id;
        this.stepDescription = stepDescription;
        this.expectedResult = expectedResult;
    }

    public UpdateStepDtoRequest() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStepDescription() {
        return stepDescription;
    }

    public void setStepDescription(String stepDescription) {
        this.stepDescription = stepDescription;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateStepDtoRequest that = (UpdateStepDtoRequest) o;
        return getId() == that.getId() && Objects.equals(getStepDescription(), that.getStepDescription()) && Objects.equals(getExpectedResult(), that.getExpectedResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getStepDescription(), getExpectedResult());
    }

    @Override
    public String toString() {
        return "UpdateStepDtoRequest{" +
                "id=" + id +
                ", description='" + stepDescription + '\'' +
                ", expectedResult='" + expectedResult + '\'' +
                '}';
    }
}
