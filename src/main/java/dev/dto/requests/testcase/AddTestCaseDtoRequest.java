package dev.dto.requests.testcase;

import dev.domain.enums.*;
import dev.validator.EnumNamePattern;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

public class AddTestCaseDtoRequest {
    @NotBlank
    @Size(max = 128, min = 1)
    private String title;
    @NotNull
    @EnumNamePattern(regexp = "Active|Draft|Outdated")
    private CaseStatus status;
    private String description;
    private String preConditions;
    private String postConditions;
    @NotNull
    @EnumNamePattern(regexp = "Critical|Blocker|Medium|Low")
    private CaseSeverity severity;
    @NotNull
    @EnumNamePattern(regexp = "Low|Medium|High")
    private CasePriority priority;
    @NotNull
    @EnumNamePattern(regexp = "Functional|Usability")
    private CaseType type;
    @NotNull
    @EnumNamePattern(regexp = "Positive|Negative|Destructive")
    private CaseBehavior behavior;
    private long testSuiteId;
    @Valid
    private List<StepDtoRequest> steps;

    public AddTestCaseDtoRequest(String title, CaseStatus status, String description, String preConditions,
                                 String postConditions, List<StepDtoRequest> steps, CaseSeverity severity,
                                 CasePriority priority, CaseBehavior behavior, CaseType type, long testSuiteId)
    {
        this.title = title;
        this.status = status;
        this.description = description;
        this.preConditions = preConditions;
        this.postConditions = postConditions;
        this.steps = steps;
        this.severity = severity;
        this.priority = priority;
        this.behavior = behavior;
        this.type = type;
        this.testSuiteId = testSuiteId;
    }

    public AddTestCaseDtoRequest() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CaseStatus getStatus() {
        return status;
    }

    public void setStatus(CaseStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(String preConditions) {
        this.preConditions = preConditions;
    }

    public String getPostConditions() {
        return postConditions;
    }

    public void setPostConditions(String postConditions) {
        this.postConditions = postConditions;
    }

    public List<StepDtoRequest> getSteps() {
        return steps;
    }

    public void setSteps(List<StepDtoRequest> steps) {
        this.steps = steps;
    }

    public CaseSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(CaseSeverity severity) {
        this.severity = severity;
    }

    public CasePriority getPriority() {
        return priority;
    }

    public void setPriority(CasePriority priority) {
        this.priority = priority;
    }

    public CaseType getType() {
        return type;
    }

    public void setType(CaseType type) {
        this.type = type;
    }

    public long getTestSuiteId() {
        return testSuiteId;
    }

    public void setTestSuiteId(long testSuiteId) {
        this.testSuiteId = testSuiteId;
    }

    public CaseBehavior getBehavior() {
        return behavior;
    }

    public void setBehavior(CaseBehavior behavior) {
        this.behavior = behavior;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddTestCaseDtoRequest that = (AddTestCaseDtoRequest) o;
        return getTestSuiteId() == that.getTestSuiteId() && Objects.equals(getTitle(), that.getTitle()) &&
                getStatus() == that.getStatus() && Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getPreConditions(), that.getPreConditions()) && Objects.equals(getBehavior(), that.getBehavior()) &&
                Objects.equals(getPostConditions(), that.getPostConditions()) && getSeverity() == that.getSeverity() &&
                getPriority() == that.getPriority() && getType() == that.getType() &&
                Objects.equals(getSteps(), that.getSteps());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getStatus(), getDescription(), getPreConditions(), getPostConditions(),
                getSeverity(), getPriority(), getType(), getBehavior(), getTestSuiteId(), getSteps());
    }

    @Override
    public String toString() {
        return "AddTestCaseDtoRequest{" +
                "title='" + title + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", preConditions='" + preConditions + '\'' +
                ", postConditions='" + postConditions + '\'' +
                ", severity=" + severity +
                ", priority=" + priority +
                ", type=" + type +
                ", behavior=" + behavior +
                ", testSuiteId=" + testSuiteId +
                ", steps=" + steps +
                '}';
    }
}
