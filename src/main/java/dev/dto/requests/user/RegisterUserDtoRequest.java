package dev.dto.requests.user;


import dev.validator.CorrectMail;
import dev.validator.CorrectPassword;

import javax.validation.constraints.Pattern;
import java.util.Objects;

/**
 * Dto request for new users.
 */
public class RegisterUserDtoRequest {
    @CorrectMail
    @Pattern(regexp = "^.+@.+\\..+$", message = "Wrong email format")
    private String email;

    @CorrectPassword
    @Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*\\d)(?=.*[!\\.\\*\\$\\%\\#\\&])[A-z\\d!\\.\\*\\$\\%\\#\\&]{8,128}+$", message = "Incorrect password! Please use at least 1 Latin letter, 1 digit and 1 special symbol from set “! . * $ % # &”.")
    private String password;
    @CorrectPassword
    @Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*\\d)(?=.*[!\\.\\*\\$\\%\\#\\&])[A-z\\d!\\.\\*\\$\\%\\#\\&]{8,128}+$", message = "Incorrect password! Please use at least 1 Latin letter, 1 digit and 1 special symbol from set “! . * $ % # &”.")
    private String confirmPassword;

    public RegisterUserDtoRequest(String email, String password, String confirmPassword) {
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public RegisterUserDtoRequest(){}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterUserDtoRequest that = (RegisterUserDtoRequest) o;
        return Objects.equals(email, that.email) && Objects.equals(password, that.password) && Objects.equals(confirmPassword, that.confirmPassword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password, confirmPassword);
    }
}
