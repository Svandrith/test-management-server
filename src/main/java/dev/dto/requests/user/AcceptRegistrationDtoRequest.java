package dev.dto.requests.user;

import dev.validator.CorrectMail;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;

public class AcceptRegistrationDtoRequest {

    @CorrectMail
    @Pattern(regexp = "^.+@.+\\..+$", message = "Wrong email format")
    private String email;
    @NotBlank(message = "Token should not be null or empty!")
    private String token;

    public AcceptRegistrationDtoRequest(String email, String token) {
        this.email = email;
        this.token = token;
    }

    public AcceptRegistrationDtoRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AcceptRegistrationDtoRequest that = (AcceptRegistrationDtoRequest) o;
        return Objects.equals(email, that.email) && Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, token);
    }
}
