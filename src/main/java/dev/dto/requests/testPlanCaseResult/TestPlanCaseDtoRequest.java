package dev.dto.requests.testPlanCaseResult;

import dev.domain.enums.Result;

import java.util.Objects;

public class TestPlanCaseDtoRequest {
    private Result result;

    public TestPlanCaseDtoRequest(Result result) {
        this.result = result;
    }

    public TestPlanCaseDtoRequest() {
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestPlanCaseDtoRequest that = (TestPlanCaseDtoRequest) o;
        return getResult() == that.getResult();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getResult());
    }

    @Override
    public String toString() {
        return "TestPlanCaseDtoRequest{" +
                "result=" + result +
                '}';
    }
}
