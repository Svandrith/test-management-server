package dev.dto.requests.testplan;

import dev.domain.enums.PlanStatus;
import dev.validator.EnumNamePattern;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class AddTestPlanDtoRequest {
    @NotBlank
    private String name;
    private String description;
    @NotNull
    @EnumNamePattern(regexp = "Created|In_progress|Done")
    private PlanStatus status;
    private List<Long> testSuitesId;

    public AddTestPlanDtoRequest(String name, String description, PlanStatus status, List<Long> testSuitesId) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.testSuitesId = testSuitesId;
    }

    public AddTestPlanDtoRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlanStatus getStatus() {
        return status;
    }

    public void setStatus(PlanStatus status) {
        this.status = status;
    }

    public List<Long> getTestSuitesId() {
        return testSuitesId;
    }

    public void setTestSuitesId(List<Long> testSuitesId) {
        this.testSuitesId = testSuitesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddTestPlanDtoRequest that = (AddTestPlanDtoRequest) o;
        return Objects.equals(getName(), that.getName()) && Objects.equals(getDescription(), that.getDescription()) && getStatus() == that.getStatus() && Objects.equals(getTestSuitesId(), that.getTestSuitesId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), getStatus(), getTestSuitesId());
    }

    @Override
    public String toString() {
        return "AddTestPlanDtoRequest{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", testSuitesId=" + testSuitesId +
                '}';
    }
}
