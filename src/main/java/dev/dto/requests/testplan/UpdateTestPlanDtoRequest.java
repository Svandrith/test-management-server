package dev.dto.requests.testplan;

import dev.domain.enums.PlanStatus;

import java.util.List;
import java.util.Objects;

public class UpdateTestPlanDtoRequest {
    private String name;
    private String description;
    private PlanStatus status;
    private List<Long> testSuitesId;

    public UpdateTestPlanDtoRequest(String name, String description, PlanStatus status, List<Long> testSuitesId) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.testSuitesId = testSuitesId;
    }

    public UpdateTestPlanDtoRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlanStatus getStatus() {
        return status;
    }

    public void setStatus(PlanStatus status) {
        this.status = status;
    }

    public List<Long> getTestSuitesId() {
        return testSuitesId;
    }

    public void setTestSuitesId(List<Long> testSuitesId) {
        this.testSuitesId = testSuitesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateTestPlanDtoRequest that = (UpdateTestPlanDtoRequest) o;
        return Objects.equals(getName(), that.getName()) && Objects.equals(getDescription(), that.getDescription()) && getStatus() == that.getStatus() && Objects.equals(getTestSuitesId(), that.getTestSuitesId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), getStatus(), getTestSuitesId());
    }

    @Override
    public String toString() {
        return "UpdateTestPlanDtoRequest{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                ", testSuitesId=" + testSuitesId +
                '}';
    }
}
