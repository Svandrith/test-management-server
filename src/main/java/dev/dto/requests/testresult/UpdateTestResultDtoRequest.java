package dev.dto.requests.testresult;

import com.fasterxml.jackson.annotation.JsonFormat;
import dev.domain.enums.Result;

import java.time.LocalDateTime;
import java.util.Objects;

public class UpdateTestResultDtoRequest {
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime testDate;
    private Result result;
    private String comment;

    public UpdateTestResultDtoRequest(LocalDateTime testDate, Result result, String comment) {
        this.testDate = testDate;
        this.result = result;
        this.comment = comment;
    }

    public UpdateTestResultDtoRequest() {
    }

    public LocalDateTime getTestDate() {
        return testDate;
    }

    public void setTestDate(LocalDateTime testDate) {
        this.testDate = testDate;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateTestResultDtoRequest that = (UpdateTestResultDtoRequest) o;
        return Objects.equals(getTestDate(), that.getTestDate()) && getResult() == that.getResult() && Objects.equals(getComment(), that.getComment());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTestDate(), getResult(), getComment());
    }

    @Override
    public String toString() {
        return "UpdateTestResultDtoRequest{" +
                "testDate=" + testDate +
                ", result=" + result +
                ", comment='" + comment + '\'' +
                '}';
    }
}
