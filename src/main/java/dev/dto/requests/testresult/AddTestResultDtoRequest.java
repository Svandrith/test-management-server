package dev.dto.requests.testresult;

import com.fasterxml.jackson.annotation.JsonFormat;
import dev.domain.enums.Result;
import dev.validator.EnumNamePattern;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

public class AddTestResultDtoRequest {
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @NotNull
    private LocalDateTime testDate;
    @NotNull
    @EnumNamePattern(regexp = "Passed|Failed|Skipped|Ignored|Blocked")
    private Result result;
    private String comment;

    public AddTestResultDtoRequest(LocalDateTime testDate, Result result, String comment) {
        this.testDate = testDate;
        this.result = result;
        this.comment = comment;
    }

    public AddTestResultDtoRequest() {
    }

    public LocalDateTime getTestDate() {
        return testDate;
    }

    public void setTestDate(LocalDateTime testDate) {
        this.testDate = testDate;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddTestResultDtoRequest that = (AddTestResultDtoRequest) o;
        return Objects.equals(getTestDate(), that.getTestDate()) && getResult() == that.getResult() && Objects.equals(getComment(), that.getComment());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTestDate(), getResult(), getComment());
    }

    @Override
    public String toString() {
        return "AddTestResultDtoRequest{" +
                "testDate=" + testDate +
                ", result=" + result +
                ", comment='" + comment + '\'' +
                '}';
    }
}
