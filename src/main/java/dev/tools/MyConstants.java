package dev.tools;

public class MyConstants {
    public static final String ACCEPT_REGISTRATION_MESSAGE = "Для подтверждения регистрации перейдите по следующей ссылке:\n";

    public static final String ACCEPT_REGISTRATION_SUBJECT = "(TMS) Подтверждение регистрации.";
}
