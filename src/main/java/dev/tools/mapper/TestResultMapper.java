package dev.tools.mapper;

import dev.domain.TestCase;
import dev.domain.TestResult;
import dev.domain.enums.Result;
import dev.dto.requests.testresult.AddTestResultDtoRequest;
import dev.dto.requests.testresult.UpdateTestResultDtoRequest;
import dev.dto.responses.testresult.TestResultDtoResponse;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Converts test result model to dto and vice versa.
 */
@Component
public class TestResultMapper {
    public TestResult toModel(AddTestResultDtoRequest request, TestCase testCase){
        TestResult testResult = new TestResult();

        testResult.setResult(request.getResult());
        testResult.setTestDate(request.getTestDate());
        testResult.setComment(request.getComment());
        testResult.setTestCase(testCase);

        return testResult;
    }

    public TestResultDtoResponse toDto(TestResult testResult){
        TestResultDtoResponse response = new TestResultDtoResponse();

        response.setId(testResult.getId());
        response.setTestCaseId(testResult.getTestCase().getId());
        response.setComment(testResult.getComment());
        response.setResult(testResult.getResult());
        response.setTestDate(testResult.getTestDate());

        return response;
    }

    public List<TestResultDtoResponse> toDto(List<TestResult> testResults){
        return testResults.stream().map(this::toDto).toList();
    }

    public void updateTestResult(UpdateTestResultDtoRequest request, TestResult testResult){
        String newComment = request.getComment();
        if(newComment != null && !newComment.trim().equals("")){
            testResult.setComment(newComment);
        }

        Result newResult = request.getResult();
        if(newResult != null){
            testResult.setResult(newResult);
        }

        LocalDateTime newTestDate = request.getTestDate();
        if(newTestDate != null){
            testResult.setTestDate(newTestDate);
        }
    }
}
