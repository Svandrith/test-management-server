package dev.tools.mapper;

import dev.domain.Project;
import dev.domain.TestSuite;
import dev.domain.enums.SuiteStatus;
import dev.dto.requests.testsuite.AddTestSuiteDtoRequest;
import dev.dto.requests.testsuite.UpdateTestSuiteDtoRequest;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Converts test suite model to dto and vice versa.
 */
@Component
public class TestSuiteMapper {
    public TestSuite toModel(AddTestSuiteDtoRequest request, Project project){
        TestSuite testSuite = new TestSuite();

        testSuite.setStatus(request.getStatus());
        testSuite.setTitle(request.getTitle());
        testSuite.setPreConditions(request.getPreConditions());
        testSuite.setDescription(request.getDescription());
        testSuite.setProject(project);
        testSuite.setTestPlans(new ArrayList<>());

        return testSuite;
    }

    public TestSuiteDtoResponse toDto(TestSuite testSuite){
        TestSuiteDtoResponse response = new TestSuiteDtoResponse();

        response.setId(testSuite.getId());
        response.setDescription(testSuite.getDescription());
        response.setTitle(testSuite.getTitle());
        response.setPreConditions(testSuite.getPreConditions());
        response.setStatus(testSuite.getStatus());
        response.setProjectId(testSuite.getProject().getId());

        return response;
    }

    public List<TestSuiteDtoResponse> toDto(List<TestSuite> testSuites){
        List<TestSuiteDtoResponse> output = new ArrayList<>();

        for(TestSuite suite : testSuites){
            output.add(toDto(suite));
        }

        return output;
    }

    public void updateTestSuite(UpdateTestSuiteDtoRequest request, TestSuite testSuite){
        String newTitle = request.getTitle();
        if(newTitle != null && !newTitle.trim().equals("")){
            testSuite.setTitle(newTitle);
        }

        SuiteStatus newStatus = request.getStatus();
        if(newStatus != null){
            testSuite.setStatus(newStatus);
        }

        String newDescription = request.getDescription();
        if(newDescription != null && !newDescription.trim().equals("")){
            testSuite.setDescription(newDescription);
        }

        String newPreConditions = request.getPreConditions();
        if(newPreConditions != null && !newPreConditions.trim().equals("")){
            testSuite.setPreConditions(newPreConditions);
        }
    }
}
