package dev.tools.mapper;

import dev.domain.*;
import dev.domain.enums.*;
import dev.dto.requests.testcase.AddTestCaseDtoRequest;
import dev.dto.requests.testcase.StepDtoRequest;
import dev.dto.requests.testcase.UpdateStepDtoRequest;
import dev.dto.requests.testcase.UpdateTestCaseDtoRequest;
import dev.dto.responses.testcase.ShortTestCaseDtoResponse;
import dev.dto.responses.testcase.StepDtoResponse;
import dev.dto.responses.testcase.TestCaseDtoResponse;
import dev.dto.responses.testcase.TestCaseDtoResponseWithoutSteps;
import dev.service.CheckService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Converts test case model to dto and vice versa.
 */
@Component
public class TestCaseMapper {

    private final CheckService checkService;

    public TestCaseMapper(CheckService checkService) {
        this.checkService = checkService;
    }

    public TestCase toModel(AddTestCaseDtoRequest request, Project project, TestSuite testSuite){
        TestCase testCase = new TestCase();

        testCase.setTitle(request.getTitle());
        testCase.setStatus(request.getStatus());
        testCase.setDescription(request.getDescription());
        testCase.setPreConditions(request.getPreConditions());
        testCase.setPostConditions(request.getPostConditions());
        testCase.setSteps(request.getSteps().stream().map(this::toModel).toList());
        testCase.setSeverity(request.getSeverity());
        testCase.setPriority(request.getPriority());
        testCase.setType(request.getType());
        testCase.setBehavior(request.getBehavior());
        testCase.setProject(project);
        testCase.setTestSuite(testSuite);

        return testCase;
    }

    public Step toModel(StepDtoRequest request){
        Step step = new Step();

        step.setExpectedResult(request.getExpectedResult());
        step.setDescription(request.getStepDescription());

        return step;
    }

    public StepDtoResponse toDto(Step step){
        StepDtoResponse response = new StepDtoResponse();
        response.setId(step.getId());
        response.setExpectedResult(step.getExpectedResult());
        response.setStepDescription(step.getDescription());

        return response;
    }

    public TestCaseDtoResponse toDto(TestCase testCase){
        TestCaseDtoResponse response = new TestCaseDtoResponse();

        response.setId(testCase.getId());
        response.setTitle(testCase.getTitle());
        response.setStatus(testCase.getStatus());
        response.setDescription(testCase.getDescription());
        response.setPreConditions(testCase.getPreConditions());
        response.setPostConditions(testCase.getPostConditions());
        response.setSteps(testCase.getSteps().stream().map(this::toDto).toList());
        response.setSeverity(testCase.getSeverity());
        response.setPriority(testCase.getPriority());
        response.setType(testCase.getType());
        response.setBehavior(testCase.getBehavior());
        response.setTestSuiteId(testCase.getTestSuite().getId());
        response.setProjectId(testCase.getProject().getId());

        return response;
    }

    public TestCaseDtoResponseWithoutSteps toTestCaseWithoutStepsDto(TestCase testCase, long testPlanId){
        TestCaseDtoResponseWithoutSteps response = new TestCaseDtoResponseWithoutSteps();

        response.setId(testCase.getId());
        response.setTitle(testCase.getTitle());
        response.setCaseStatus(testCase.getStatus());
        response.setDescription(testCase.getDescription());
        response.setPreConditions(testCase.getPreConditions());
        response.setPostConditions(testCase.getPostConditions());
        response.setSeverity(testCase.getSeverity());
        response.setPriority(testCase.getPriority());
        response.setType(testCase.getType());
        response.setBehavior(testCase.getBehavior());
        response.setTestSuiteId(testCase.getTestSuite().getId());
        response.setProjectId(testCase.getProject().getId());

        response.setResult(checkService.findResultForTestPlanCase(testCase, testPlanId));

        return response;
    }

    public void updateTestCase(TestCase testCase, UpdateTestCaseDtoRequest request, TestSuite newTestSuite){
        String newTitle = request.getTitle();
        if(newTitle != null && !newTitle.trim().equals("")){
            testCase.setTitle(newTitle);
        }

        CaseStatus newStatus = request.getStatus();
        if(newStatus != null){
            testCase.setStatus(newStatus);
        }

        String newDescription = request.getDescription();
        if(newDescription != null && !newDescription.trim().equals("")){
            testCase.setDescription(newDescription);
        }

        String newPreConditions = request.getPreConditions();
        if(newPreConditions != null && !newPreConditions.trim().equals("")){
            testCase.setPreConditions(newPreConditions);
        }

        String newPostConditions = request.getPostConditions();
        if(newPostConditions != null && !newPostConditions.trim().equals("")){
            testCase.setPostConditions(newPostConditions);
        }

        CaseSeverity newCaseSeverity = request.getSeverity();
        if(newCaseSeverity != null){
            testCase.setSeverity(newCaseSeverity);
        }

        CasePriority newCasePriority = request.getPriority();
        if(newCasePriority != null){
            testCase.setPriority(newCasePriority);
        }

        CaseType newCaseType = request.getType();
        if(newCaseType != null){
            testCase.setType(newCaseType);
        }

        CaseBehavior newBehavior = request.getBehavior();
        if(newBehavior != null){
            testCase.setBehavior(newBehavior);
        }

        if(newTestSuite != null){
            testCase.setTestSuite(newTestSuite);
        }

        List<UpdateStepDtoRequest> stepsToUpdate = request.getSteps();
        if(stepsToUpdate == null){
            return;
        }
        List<Step> steps = testCase.getSteps();
        List<Long> updateStepsId = stepsToUpdate.stream().map(UpdateStepDtoRequest::getId).toList();
        List<Step> stepsToDelete = new ArrayList<>();
        if(!stepsToUpdate.isEmpty()){
            for(Step step : steps){
                if(!updateStepsId.contains(step.getId())){
                    stepsToDelete.add(step);
                }
            }
            for(Step stepToDelete: stepsToDelete){
                steps.remove(stepToDelete);
            }
            for(UpdateStepDtoRequest stepRequest : stepsToUpdate){
                if(stepRequest.getId() == 0){
                    Step step = new Step();
                    step.setExpectedResult(stepRequest.getExpectedResult());
                    step.setDescription(stepRequest.getStepDescription());
                    steps.add(step);
                    continue;
                }
                for(Step step : steps){
                    if(step.getId() == stepRequest.getId()){
                        updateStep(step, stepRequest);
                        break;
                    }
                }
            }
        }
    }

    public void updateStep(Step step, UpdateStepDtoRequest request){
        String newDescription = request.getStepDescription();
        if(newDescription != null && !newDescription.trim().equals("")){
            step.setDescription(newDescription);
        }

        String newExpectedResult = request.getExpectedResult();
        if(newExpectedResult != null && !newExpectedResult.trim().equals("")){
            step.setExpectedResult(newExpectedResult);
        }
    }

    public ShortTestCaseDtoResponse testCaseToDto(TestCase testCase){
        ShortTestCaseDtoResponse response = new ShortTestCaseDtoResponse();

        response.setId(testCase.getId());
        response.setTitle(testCase.getTitle());
        response.setStatus(testCase.getStatus());
        response.setDescription(testCase.getDescription());
        response.setPreConditions(testCase.getPreConditions());
        response.setPostConditions(testCase.getPostConditions());
        response.setSteps(testCase.getSteps().stream().map(this::toDto).toList());
        response.setSeverity(testCase.getSeverity());
        response.setPriority(testCase.getPriority());
        response.setType(testCase.getType());
        response.setBehavior(testCase.getBehavior());
        return response;
    }
}
