package dev.tools.mapper;

import dev.domain.User;
import dev.dto.requests.user.RegisterUserDtoRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


/**
 * Converts user model to dto and vice versa.
 */
@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;

    public UserMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User toUser(RegisterUserDtoRequest request, String activationToken){
        User user = new User();

        user.setEmail(request.getEmail());
        user.setEncodedPassword(passwordEncoder.encode(request.getPassword()));
        user.setActivated(false);
        user.setActivationToken(activationToken);

        return user;
    }
}
