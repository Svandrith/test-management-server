package dev.tools.mapper;

import dev.domain.Project;
import dev.domain.Step;
import dev.domain.TestPlan;
import dev.domain.TestSuite;
import dev.domain.enums.PlanStatus;
import dev.dto.requests.testcase.UpdateStepDtoRequest;
import dev.dto.requests.testplan.AddTestPlanDtoRequest;
import dev.dto.requests.testplan.UpdateTestPlanDtoRequest;
import dev.dto.responses.testplan.TestPlanDtoResponse;
import dev.dto.responses.testplan.TestPlanWithShortTestCasesDtoResponse;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;
import dev.dto.responses.testsuite.TestSuiteWithCasesWithoutStepsDtoResponse;
import dev.repos.TestSuiteRepo;
import dev.service.CheckService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Converts test plan model to dto and vice versa.
 */
@Component
public class TestPlanMapper {

    private TestSuiteMapper testSuiteMapper;
    private CheckService checkService;

    public TestPlanMapper(TestSuiteMapper testSuiteMapper, CheckService checkService) {
        this.testSuiteMapper = testSuiteMapper;
        this.checkService = checkService;
    }

    public TestPlan toModel(AddTestPlanDtoRequest request, Project project) {
        TestPlan testPlan = new TestPlan();

        testPlan.setDescription(request.getDescription());
        testPlan.setName(request.getName());
        testPlan.setProject(project);

        LocalDateTime dateTime = LocalDateTime.now();
        testPlan.setCreationDate(dateTime);
        testPlan.setLastUpdateDate(dateTime);

        testPlan.setStatus(request.getStatus());
        return testPlan;
    }

    public TestPlanDtoResponse toDto(TestPlan testPlan) {
        TestPlanDtoResponse response = new TestPlanDtoResponse();

        response.setProjectId(testPlan.getProject().getId());
        response.setDescription(testPlan.getDescription());
        response.setName(testPlan.getName());
        response.setId(testPlan.getId());
        response.setStatus(testPlan.getStatus());

        List<TestSuiteDtoResponse> testSuites = new ArrayList<>();
        for (TestSuite suite : testPlan.getTestSuite()) {
            testSuites.add(testSuiteMapper.toDto(suite));
        }
        response.setTestSuites(testSuites);
        return response;
    }

    public TestPlanWithShortTestCasesDtoResponse toDto(TestPlan testPlan, List<TestSuiteWithCasesWithoutStepsDtoResponse> list){
        TestPlanWithShortTestCasesDtoResponse response = new TestPlanWithShortTestCasesDtoResponse();

        response.setProjectId(testPlan.getProject().getId());
        response.setDescription(testPlan.getDescription());
        response.setName(testPlan.getName());
        response.setId(testPlan.getId());
        response.setStatus(testPlan.getStatus());

        response.setTestSuites(list);
        return response;
    }

    public List<TestPlanDtoResponse> toDto(List<TestPlan> testPlans){
        return testPlans.stream().map(this::toDto).toList();
    }

    public void updateTestPlan(UpdateTestPlanDtoRequest request, TestPlan testPlan) {
        String newName = request.getName();
        if (newName != null && !newName.trim().equals("")) {
            testPlan.setName(newName);
        }

        String newDescription = request.getDescription();
        if (newDescription != null && !newDescription.trim().equals("")) {
            testPlan.setDescription(newDescription);
        }

        PlanStatus newStatus = request.getStatus();
        if (newStatus != null) {
            testPlan.setStatus(newStatus);
        }

        Project project = testPlan.getProject();
        List<Long> suitesToUpdate = request.getTestSuitesId();
        if (suitesToUpdate == null) {
            return;
        }

        LocalDateTime updateTime = LocalDateTime.now();
        testPlan.setLastUpdateDate(updateTime);

        List<TestSuite> testSuites = testPlan.getTestSuite();
        List<TestSuite> suitesToDelete = new ArrayList<>();
        if (!suitesToUpdate.isEmpty()) {
            for (TestSuite suite : testSuites) {
                if (!suitesToUpdate.contains(suite.getId())) {
                    suitesToDelete.add(suite);
                }
            }
            for (TestSuite suiteToDelete : suitesToDelete) {
                testSuites.remove(suiteToDelete);
            }
            for (Long suiteId : suitesToUpdate) {
                TestSuite testSuite = checkService.findTestSuiteByIdAndUserAndProject(suiteId, project);
                if (!testSuites.contains(testSuite)) {
                    testSuites.add(testSuite);
                }
            }
        }
    }
}
