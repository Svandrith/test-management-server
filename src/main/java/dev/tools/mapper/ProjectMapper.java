package dev.tools.mapper;

import dev.domain.Project;
import dev.domain.User;
import dev.dto.requests.project.CreateProjectDtoRequest;
import dev.dto.requests.project.UpdateProjectDtoRequest;
import dev.dto.responses.project.ProjectDtoResponse;
import org.springframework.stereotype.Component;

import java.util.List;
/**
 * Converts project model to dto and vice versa.
 */
@Component
public class ProjectMapper {

    public Project toProject(CreateProjectDtoRequest request, User user){
        Project project = new Project();

        project.setId(0L);
        project.setName(request.getName());
        project.setDescription(request.getDescription());
        project.setUser(user);

        return project;
    }

    public ProjectDtoResponse toDto(Project project){
        ProjectDtoResponse response = new ProjectDtoResponse();

        response.setId(project.getId());
        response.setName(project.getName());
        response.setDescription(project.getDescription());

        return response;
    }

    public List<ProjectDtoResponse> toDto(List<Project> projectList){
        return projectList.stream().map(this::toDto).toList();
    }

    public void updateProjectByDto(Project project, UpdateProjectDtoRequest request){
        String newName = request.getName();
        String newDescription = request.getDescription();

        if(newName != null && !newName.trim().equals("")){
            project.setName(newName);
        }
        if (newDescription != null && !newDescription.trim().equals("")){
            project.setDescription(request.getDescription());
        }
    }
}
