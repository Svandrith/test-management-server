package dev.tools;

import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Service
public class MyMailSender {

    private final Session session;

    public MyMailSender(Session session) {
        this.session = session;
    }

    private String readHTML(String link)  {
        File file = new File("./assets/letter_confirm-password.html");
        StringBuilder output = new StringBuilder();
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while((line = reader.readLine()) != null){
                if(line.contains("Insert link here")){
                    line = line.replace("Insert link here", link);
                }
                output.append(line).append("\n");
            }
        } catch (IOException e){
            throw new ServerException("HTML can't be read.", ServerErrorCode.HTML_NON_READABLE);
        }
        return output.toString();
    }

    public void sendRegistrationMessage(String to, String subject, String link) {
        String from = "t.m.s123264@gmail.com";
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setContent(readHTML(link), "text/html");
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}