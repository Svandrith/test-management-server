package dev.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Component
public class AppPropertiesUtils{
    private int minPasswordLength;
    private int maxPasswordLength;
    private int minMailLength;
    private int maxMailLength;

    private int maxProjectDescriptionLength;
    private int maxProjectNameLength;

    @Autowired
    public AppPropertiesUtils(@Value("${min_password_length}") int minPasswordLength,
                              @Value("${max_password_length}") int maxPasswordLength,
                              @Value("${min_mail_length}") int minMailLength,
                              @Value("${max_mail_length}") int maxMailLength,
                              @Value("${max_project_name_length}") int maxProjectNameLength,
                              @Value("${max_project_description_length}") int maxProjectDescriptionLength){
        this.minPasswordLength = minPasswordLength;
        this.maxPasswordLength = maxPasswordLength;
        this.minMailLength = minMailLength;
        this.maxMailLength = maxMailLength;
        this.maxProjectNameLength = maxProjectNameLength;
        this.maxProjectDescriptionLength = maxProjectDescriptionLength;
    }

    public int getMinPasswordLength() {
        return minPasswordLength;
    }

    public void setMinPasswordLength(int minPasswordLength) {
        this.minPasswordLength = minPasswordLength;
    }

    public int getMaxPasswordLength() {
        return maxPasswordLength;
    }

    public void setMaxPasswordLength(int maxPasswordLength) {
        this.maxPasswordLength = maxPasswordLength;
    }

    public int getMinMailLength() {
        return minMailLength;
    }

    public void setMinMailLength(int minMailLength) {
        this.minMailLength = minMailLength;
    }

    public int getMaxMailLength() {
        return maxMailLength;
    }

    public void setMaxMailLength(int maxMailLength) {
        this.maxMailLength = maxMailLength;
    }

    public int getMaxProjectDescriptionLength() {
        return maxProjectDescriptionLength;
    }

    public void setMaxProjectDescriptionLength(int maxProjectDescriptionLength) {
        this.maxProjectDescriptionLength = maxProjectDescriptionLength;
    }

    public int getMaxProjectNameLength() {
        return maxProjectNameLength;
    }

    public void setMaxProjectNameLength(int maxProjectNameLength) {
        this.maxProjectNameLength = maxProjectNameLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppPropertiesUtils that = (AppPropertiesUtils) o;
        return minPasswordLength == that.minPasswordLength && maxPasswordLength == that.maxPasswordLength && minMailLength == that.minMailLength && maxMailLength == that.maxMailLength && maxProjectDescriptionLength == that.maxProjectDescriptionLength && maxProjectNameLength == that.maxProjectNameLength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(minPasswordLength, maxPasswordLength, minMailLength, maxMailLength, maxProjectDescriptionLength, maxProjectNameLength);
    }
}
