package dev.config;

import dev.repos.UserRepo;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Represents UserDetailsService implementation for Spring security httpBasic authentication.
 */
@Service
public class ApplicationUserService implements UserDetailsService {

    private final UserRepo userRepo;

    public ApplicationUserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<dev.domain.User> user = userRepo.findByEmail(email);
        if(user.isEmpty() || !user.get().isActivated()){
            throw new UsernameNotFoundException(String.format("User with email %s not found", email));
        }
        return new User(email, user.get().getEncodedPassword(), new ArrayList<>());
    }
}
