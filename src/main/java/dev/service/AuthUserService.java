package dev.service;

import dev.domain.User;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AuthUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthUserService.class);
    private final UserRepo userRepo;

    public AuthUserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public User getUserByEmail(String email) throws ServerException {
        LOGGER.info("Auth User Service: get user by email {}", email);
        return userRepo.findByEmail(email).orElseThrow(
                () -> new ServerException(
                        String.format("User with email %s not found", email),
                        ServerErrorCode.USER_NOT_FOUND
                )
        );
    }
}
