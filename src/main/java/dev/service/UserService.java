package dev.service;

import dev.domain.User;
import dev.dto.requests.user.RegisterUserDtoRequest;
import dev.exception.RegistrationException;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.UserRepo;
import dev.tools.MyConstants;
import dev.tools.MyMailSender;
import dev.tools.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;


/**
 * User endpoints processing service.
 */
@Service
public class UserService {

    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepo userRepo;

    private final MyMailSender myMailSender;

    private final UserMapper userMapper;

    private final AuthUserService authUserService;

    public UserService(UserRepo userRepo, UserMapper userMapper, MyMailSender myMailSender, AuthUserService authUserService) {
        this.userRepo = userRepo;
        this.userMapper = userMapper;
        this.myMailSender = myMailSender;
        this.authUserService = authUserService;
    }

    public void registerUser(RegisterUserDtoRequest registerUserDtoRequest) {
        LOGGER.info("User service: Register user with request {}", registerUserDtoRequest);

        if (!registerUserDtoRequest.getPassword().equals(registerUserDtoRequest.getConfirmPassword())){
            throw new ServerException("Password != Confirm Password", ServerErrorCode.WRONG_CONFIRM_PASSWORD);
        }

        String registrationToken = UUID.randomUUID().toString();
        User user = userMapper.toUser(registerUserDtoRequest, registrationToken);

        try{
            userRepo.save(user);
        } catch (DataAccessException ex){
            throw new RegistrationException("User with this email is already registered!", ServerErrorCode.ALREADY_REGISTERED);
        }
        String link = String.format("http://localhost:8081/register/confirm/%s", registrationToken);
        myMailSender.sendRegistrationMessage(registerUserDtoRequest.getEmail(), MyConstants.ACCEPT_REGISTRATION_SUBJECT, link);
    }

    public void acceptRegistration(String token, HttpServletResponse response) throws IOException {
        LOGGER.info("User service: Accept registration of user with token {}", token);

        Optional<User> optionalUser = userRepo.findByActivationToken(token);
        if(optionalUser.isEmpty()){
            response.sendRedirect("https://www.google.com/"); //пользователь активирован, либо неправильный URL ??
            return;
        }
        User user = optionalUser.get();

        user.setActivated(true);
        user.setActivationToken(null);
        userRepo.save(user);
        response.sendRedirect("http://7quizzes.local/confirmation");
    }

    public void deleteUser(String email){
        LOGGER.info("User service: Delete user with email {}", email);

        User user = authUserService.getUserByEmail(email);
        userRepo.delete(user);
    }
}
