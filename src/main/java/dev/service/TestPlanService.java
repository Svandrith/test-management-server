package dev.service;

import dev.domain.*;
import dev.domain.enums.Result;
import dev.dto.requests.testPlanCaseResult.TestPlanCaseDtoRequest;
import dev.dto.requests.testplan.AddTestPlanDtoRequest;
import dev.dto.requests.testplan.UpdateTestPlanDtoRequest;
import dev.dto.responses.testplan.TestPlanDtoResponse;
import dev.dto.responses.testplan.TestPlanWithShortTestCasesDtoResponse;
import dev.dto.responses.testsuite.TestSuiteWithCasesWithoutStepsDtoResponse;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.PlanCaseResultRepo;
import dev.repos.TestCaseRepo;
import dev.repos.TestPlanRepo;
import dev.repos.TestSuiteRepo;
import dev.tools.mapper.TestCaseMapper;
import dev.tools.mapper.TestPlanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class TestPlanService {

    private final Logger LOGGER = LoggerFactory.getLogger(TestCaseService.class);
    private final AuthUserService authUserService;
    private final TestPlanRepo testPlanRepo;
    private final TestSuiteRepo testSuiteRepo;
    private final TestCaseRepo testCaseRepo;
    private final CheckService checkService;
    private final TestPlanMapper testPlanMapper;
    private final TestCaseMapper testCaseMapper;
    private final PlanCaseResultRepo planCaseResultRepo;

    public TestPlanService(AuthUserService authUserService, TestPlanRepo testPlanRepo, TestSuiteRepo testSuiteRepo, TestCaseRepo testCaseRepo, CheckService checkService, TestPlanMapper testPlanMapper, TestCaseMapper testCaseMapper, PlanCaseResultRepo planCaseResultRepo) {
        this.authUserService = authUserService;
        this.testPlanRepo = testPlanRepo;
        this.testSuiteRepo = testSuiteRepo;
        this.testCaseRepo = testCaseRepo;
        this.checkService = checkService;
        this.testPlanMapper = testPlanMapper;
        this.testCaseMapper = testCaseMapper;
        this.planCaseResultRepo = planCaseResultRepo;
    }

    public TestPlanDtoResponse addTestPlan(String email, long projectId, AddTestPlanDtoRequest request){
        LOGGER.info("Test Plan Service: add test plan with request {} of project with id {} by user with email {}", request, projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestPlan testPlan = testPlanMapper.toModel(request, project);

        List<TestSuite> planTestSuites = new ArrayList<>();
        for(Long testSuiteId: request.getTestSuitesId()){
            Optional<TestSuite> optionalTestSuite = testSuiteRepo.findByIdAndProject(testSuiteId, project);
            optionalTestSuite.ifPresent(planTestSuites::add);
        }
        testPlan.setTestSuite(planTestSuites);

        return testPlanMapper.toDto(testPlanRepo.save(testPlan));
    }

    public void deleteTestPlan(String email, long projectId, long testPlanId){
        LOGGER.info("Test Plan Service: delete test plan with id {} from project with id {} by user with email {}", testPlanId, projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestPlan testPlan = findTestPlanByIdAndProject(testPlanId, project);
        testPlanRepo.delete(testPlan);
    }

    public TestPlanDtoResponse updateTestPlan(String email, long projectId, long testPlanId, UpdateTestPlanDtoRequest request){
        LOGGER.info("Test Plan Service: update test plan (by request {}) with id {} from project with id {} by user with email {}", request, testPlanId, projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestPlan testPlan = findTestPlanByIdAndProject(testPlanId, project);

        testPlanMapper.updateTestPlan(request, testPlan);
        return testPlanMapper.toDto(testPlanRepo.save(testPlan));
    }

    public TestPlanWithShortTestCasesDtoResponse findTestPlanById(String email, long projectId, long testPlanId){
        LOGGER.info("Test Plan Service: find test plan with id {} from project with id {} by user with email {}", testPlanId, projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestPlan testPlan = findTestPlanByIdAndProject(testPlanId, project);

        List<TestSuiteWithCasesWithoutStepsDtoResponse> responseSuiteList = new ArrayList<>();
        for(TestSuite testSuite : testPlan.getTestSuite()){
            responseSuiteList.add(
                    new TestSuiteWithCasesWithoutStepsDtoResponse(testSuite.getId(), testSuite.getTitle(), testSuite.getStatus(), testSuite.getDescription(),
                            testSuite.getPreConditions(), testCaseRepo.findAllByTestSuite(testSuite).stream().map(testCase -> testCaseMapper.toTestCaseWithoutStepsDto(testCase, testPlanId)).toList())
            );
        }

        return testPlanMapper.toDto(testPlan, responseSuiteList);
    }

    public List<TestPlanDtoResponse> findAllProjectTestPlan(String email, long projectId){
        LOGGER.info("Test Plan Service: find all test plans from project with id {} by user with email {}", projectId, email);
        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        Optional<List<TestPlan>> testPlanList = testPlanRepo.findAllByProjectOrderByLastUpdateDateDesc(project);
        if(testPlanList.isEmpty()){
            return new ArrayList<>();
        }

        return testPlanMapper.toDto(testPlanList.get());
    }

    public void addTestPlanCaseResult(String email, long projectId, long caseId, long planId, TestPlanCaseDtoRequest request){
        LOGGER.info("Test Plan Service: add test plan case result from project with id {} by user with email {}", projectId, email);
        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, caseId, projectId);
        TestPlan testPlan = findTestPlanByIdAndProject(planId, project);
        if(testPlan.getTestSuite().stream().map(TestSuite::getId).noneMatch(testSuiteId -> Objects.equals(testCase.getTestSuite().getId(), testSuiteId))){
            throw new ServerException(String.format("Test plan with id %d doesn't contain test case with id %d", planId, caseId), ServerErrorCode.NOT_PLANS_TEST_CASE);
        }

        PlanCaseResult planCaseResult = new PlanCaseResult(new PlanCaseId(caseId, planId), request.getResult(), testCase, testPlan);
        planCaseResultRepo.save(planCaseResult);
    }

    public void updateTestPlanCaseResult(String email, long projectId, long caseId, long planId, TestPlanCaseDtoRequest request){
        LOGGER.info("Test Plan Service: update test plan case result from project with id {} by user with email {}", projectId, email);
        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, caseId, projectId);
        TestPlan testPlan = findTestPlanByIdAndProject(planId, project);
        if(testPlan.getTestSuite().stream().map(TestSuite::getId).noneMatch(testSuiteId -> Objects.equals(testCase.getTestSuite().getId(), testSuiteId))){
            throw new ServerException(String.format("Test plan with id %d doesn't contain test case with id %d", planId, caseId), ServerErrorCode.NOT_PLANS_TEST_CASE);
        }

        PlanCaseResult planCaseResult = planCaseResultRepo.findById(new PlanCaseId(caseId, planId)).orElseThrow( () ->
                new ServerException(String.format("Result of test case with id %d in test plan with id %d not found", caseId, planId), ServerErrorCode.PLAN_CASE_RESULT_NOT_FOUND)
        );
        planCaseResult.setResult(request.getResult());
        planCaseResultRepo.save(planCaseResult);
    }

    private TestPlan findTestPlanByIdAndProject(long testPlanId, Project project){
        return testPlanRepo.findByIdAndProject(testPlanId, project).orElseThrow(
                () -> new ServerException(String.format("Test plan with id %d and project id %d not found", testPlanId, project.getId()), ServerErrorCode.TEST_PLAN_NOT_FOUND)
        );
    }
}
