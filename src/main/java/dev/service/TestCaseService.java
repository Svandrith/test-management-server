package dev.service;

import dev.domain.*;
import dev.dto.requests.testcase.AddTestCaseDtoRequest;
import dev.dto.requests.testcase.UpdateTestCaseDtoRequest;
import dev.dto.responses.testcase.TestCaseDtoResponse;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.StepRepo;
import dev.repos.TestCaseRepo;
import dev.repos.TestSuiteRepo;
import dev.tools.mapper.TestCaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TestCaseService {
    private final Logger LOGGER = LoggerFactory.getLogger(TestCaseService.class);
    private final TestCaseMapper testCaseMapper;
    private final StepRepo stepRepo;
    private final TestCaseRepo testCaseRepo;
    private final TestSuiteRepo testSuiteRepo;
    private final AuthUserService authUserService;
    private final CheckService checkService;

    public TestCaseService(TestCaseMapper testCaseMapper, StepRepo stepRepo, TestCaseRepo testCaseRepo, TestSuiteRepo testSuiteRepo, AuthUserService authUserService, CheckService checkService) {
        this.testCaseMapper = testCaseMapper;
        this.stepRepo = stepRepo;
        this.testCaseRepo = testCaseRepo;
        this.testSuiteRepo = testSuiteRepo;
        this.authUserService = authUserService;
        this.checkService = checkService;
    }

    public TestCaseDtoResponse addTestCase(String email, long projectId, AddTestCaseDtoRequest request) {
        LOGGER.info("Test Case Service: add test case with request {} to project with id {} by user with email {}", request, projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestSuite suite = testSuiteRepo.findById(request.getTestSuiteId()).orElseThrow(
                () -> new ServerException(String.format("Test suite with id %d not found.", request.getTestSuiteId()), ServerErrorCode.TEST_SUITE_NOT_FOUND)
        );

        TestCase testCase = testCaseMapper.toModel(request, project, suite);
        return testCaseMapper.toDto(testCaseRepo.save(testCase));
    }

    public void deleteTestCase(String email, long testCaseId, long projectId) {
        LOGGER.info("Test Case Service: from project with id {} delete test case with id {} by user with email {}", projectId, testCaseId, email);

        User user = authUserService.getUserByEmail(email);

        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId);

        stepRepo.deleteAll(testCase.getSteps());

        testCaseRepo.delete(testCase);
    }

    public TestCaseDtoResponse findTestCaseById(String email, long testCaseId, long projectId) {
        LOGGER.info("Test Case Service: from project with id {} find test case with id {} by user with email {}", projectId, testCaseId, email);

        User user = authUserService.getUserByEmail(email);

        return testCaseMapper.toDto(checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId));
    }

    public TestCaseDtoResponse updateTestCase(String email, long testCaseId, long projectId, UpdateTestCaseDtoRequest request){
        LOGGER.info("Test Case Service: update test case with id {} from project with id {} by user with email {}", testCaseId, projectId, email);

        User user = authUserService.getUserByEmail(email);

        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId);

        TestSuite newSuite = null;
        Long newSuiteId = request.getTestSuiteId();

        if(newSuiteId != null){
            newSuite = findTestSuiteByIdAndProject(testCase.getProject(), newSuiteId);
        }

        testCaseMapper.updateTestCase(testCase, request, newSuite);

        return testCaseMapper.toDto(testCaseRepo.save(testCase));
    }

    //DRAFT
    public void deleteTestCaseStep(String email, long projectId, long testCaseId, long stepToDeleteId){
        LOGGER.info("Test Case Service: delete test case step with id {} from project with id {} from test case with id {} by user with email {}", stepToDeleteId, projectId, testCaseId, email);

        User user = authUserService.getUserByEmail(email);

        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId);

        Step stepToDelete = null;

        for(Step step : testCase.getSteps()){
            if(step.getId() == stepToDeleteId){
                stepToDelete = step;
                break;
            }
        }

        if(stepToDelete != null){
            testCase.getSteps().remove(stepToDelete);
        }

        testCaseRepo.save(testCase);
    }

    private TestSuite findTestSuiteByIdAndProject(Project project, long testSuiteId){
        return testSuiteRepo.findByIdAndProject(testSuiteId, project).orElse(null);
    }
}
