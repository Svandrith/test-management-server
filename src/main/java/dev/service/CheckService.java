package dev.service;

import dev.domain.*;
import dev.domain.enums.Result;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.PlanCaseResultRepo;
import dev.repos.ProjectRepo;
import dev.repos.TestCaseRepo;
import dev.repos.TestSuiteRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CheckService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CheckService.class);
    private final ProjectRepo projectRepo;
    private final TestCaseRepo testCaseRepo;
    private final TestSuiteRepo testSuiteRepo;
    private final PlanCaseResultRepo planCaseResultRepo;

    public CheckService(ProjectRepo projectRepo, TestCaseRepo testCaseRepo, TestSuiteRepo testSuiteRepo, PlanCaseResultRepo planCaseResultRepo) {
        this.projectRepo = projectRepo;
        this.testCaseRepo = testCaseRepo;
        this.testSuiteRepo = testSuiteRepo;
        this.planCaseResultRepo = planCaseResultRepo;
    }

    public Project findProjectByIdAndUser(long projectId, User user){
        LOGGER.info("Check service: get project by id {} and user {}", projectId, user);
        Optional<Project> optionalProject = projectRepo.findByIdAndUser(projectId, user);
        if (optionalProject.isEmpty()) {
            throw new ServerException(String.format("Project with id %d not found", projectId), ServerErrorCode.PROJECT_NOT_FOUND);
        }
        return optionalProject.get();
    }

    public TestCase findTestCaseByIdAndUserAndProjectId(User user, long testCaseId, long projectId) {
        TestCase testCase = testCaseRepo.findById(testCaseId).orElseThrow(() ->
                new ServerException(String.format("Test case with id %s not found", testCaseId),
                        ServerErrorCode.TEST_CASE_NOT_FOUND)
        );

        Project project = testCase.getProject();

        if (!project.getUser().equals(user) || project.getId() != projectId) {
            throw new ServerException(String.format("Test case with id %d doesn't belong to user with email %s and project with id %d", testCaseId, user.getEmail(), projectId),
                    ServerErrorCode.NOT_USERS_TEST_CASE);
        }
        return testCase;
    }

    public TestSuite findTestSuiteByIdAndUserAndProject(long testSuiteId, Project project) {
        Optional<TestSuite> optionalTestSuite = testSuiteRepo.findByIdAndProject(testSuiteId, project);
        if (optionalTestSuite.isEmpty()) {
            throw new ServerException(String.format("Test suite with id %d and project id %d not found", testSuiteId, project.getId()), ServerErrorCode.TEST_SUITE_NOT_FOUND);
        }
        return optionalTestSuite.get();
    }

    public Result findResultForTestPlanCase(TestCase testCase, long testPlanId){
        Optional<PlanCaseResult> planCaseResult = planCaseResultRepo.findById(new PlanCaseId(testCase.getId(), testPlanId));
        return planCaseResult.map(PlanCaseResult::getResult).orElse(null);
    }
}
