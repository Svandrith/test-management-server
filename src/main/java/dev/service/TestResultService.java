package dev.service;

import dev.domain.Project;
import dev.domain.TestCase;
import dev.domain.TestResult;
import dev.domain.User;
import dev.dto.requests.testresult.AddTestResultDtoRequest;
import dev.dto.requests.testresult.UpdateTestResultDtoRequest;
import dev.dto.responses.testresult.TestResultDtoResponse;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.TestResultRepo;
import dev.tools.mapper.TestResultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TestResultService {
    private final static Logger LOGGER = LoggerFactory.getLogger(TestResultService.class);
    private final TestResultRepo testResultRepo;
    private final TestResultMapper testResultMapper;
    private final AuthUserService authUserService;
    private final CheckService checkService;

    public TestResultService(TestResultRepo testResultRepo, TestResultMapper testResultMapper, AuthUserService authUserService, CheckService checkService) {
        this.testResultRepo = testResultRepo;
        this.testResultMapper = testResultMapper;
        this.authUserService = authUserService;
        this.checkService = checkService;
    }

    public TestResultDtoResponse addTestResult(String email, long projectId, long testCaseId, AddTestResultDtoRequest request){
        LOGGER.info("Test Result Service: add test result with request {} to test case with id {}" +
                " from project with id {} by user with email {}", request, testCaseId, projectId, email);

        User user = authUserService.getUserByEmail(email);
        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId);

        return testResultMapper.toDto(testResultRepo.save(testResultMapper.toModel(request, testCase)));
    }

    public void deleteTestResult(String email, long projectId, long testCaseId, long testResultId){
        LOGGER.info("Test Result Service: delete test result with id {} of test case with id {}" +
                " from project with id {} by user with email {}", testResultId, testCaseId, projectId, email);

        User user = authUserService.getUserByEmail(email);
        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId);

        testResultRepo.delete(findTestResultByIdAndTestCase(testResultId, testCase));
    }

    public TestResultDtoResponse findTestResultById(String email, long projectId, long testCaseId, long testResultId){
        LOGGER.info("Test Result Service: find test result with id {} of test case with id {}" +
                " from project with id {} by user with email {}", testResultId, testCaseId, projectId, email);

        User user = authUserService.getUserByEmail(email);
        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId);

        return testResultMapper.toDto(findTestResultByIdAndTestCase(testResultId, testCase));
    }

    public TestResultDtoResponse updateTestResult(String email, long projectId, long testCaseId, long testResultId,
                                                  UpdateTestResultDtoRequest request){
        LOGGER.info("Test Result Service: update test result by request {} with id {} of test case with id {}" +
                " from project with id {} by user with email {}", request, testResultId, testCaseId, projectId, email);

        User user = authUserService.getUserByEmail(email);
        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId);
        TestResult testResult = findTestResultByIdAndTestCase(testResultId, testCase);

        testResultMapper.updateTestResult(request, testResult);
        return testResultMapper.toDto(testResultRepo.save(testResult));
    }

    public List<TestResultDtoResponse> findAllTestCaseResults(String email, long projectId, long testCaseId){
        LOGGER.info("Test Result Service: find all test results of test case with id {}" +
                " from project with id {} by user with email {}", testCaseId, projectId, email);

        User user = authUserService.getUserByEmail(email);
        TestCase testCase = checkService.findTestCaseByIdAndUserAndProjectId(user, testCaseId, projectId);

        return testResultMapper.toDto(testResultRepo.findAllByTestCaseOrderByIdDesc(testCase).orElse(new ArrayList<>()));
    }

    private TestResult findTestResultByIdAndTestCase(long testResultId, TestCase testCase){
        return testResultRepo.findByIdAndTestCase(testResultId, testCase).orElseThrow(
                () -> new ServerException(String.format("Test result with id %d from test case with id %d not found", testResultId, testCase.getId()), ServerErrorCode.TEST_RESULT_NOT_FOUND)
        );
    }


}
