package dev.service;

import dev.domain.Project;
import dev.domain.TestSuite;
import dev.domain.User;
import dev.dto.requests.testsuite.AddTestSuiteDtoRequest;
import dev.dto.requests.testsuite.UpdateTestSuiteDtoRequest;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.TestSuiteRepo;
import dev.tools.mapper.TestSuiteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TestSuiteService {
    private final Logger LOGGER = LoggerFactory.getLogger(TestSuiteService.class);

    private final TestSuiteRepo testSuiteRepo;
    private final TestSuiteMapper testSuiteMapper;
    private final AuthUserService authUserService;
    private final CheckService checkService;

    public TestSuiteService(TestSuiteRepo testSuiteRepo, TestSuiteMapper testSuiteMapper, AuthUserService authUserService, CheckService checkService) {
        this.testSuiteRepo = testSuiteRepo;
        this.testSuiteMapper = testSuiteMapper;
        this.authUserService = authUserService;
        this.checkService = checkService;
    }

    public TestSuiteDtoResponse addTestSuite(String email, long projectId, AddTestSuiteDtoRequest request) {
        LOGGER.info("Test Suite service: add test suite with request {} to project with id {} by user with id {}", request, projectId, email);

        User user = authUserService.getUserByEmail(email);
        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestSuite testSuite = testSuiteMapper.toModel(request, project);

        return testSuiteMapper.toDto(testSuiteRepo.save(testSuite));
    }

    public List<TestSuiteDtoResponse> findAllTestSuitesByProject(String email, long projectId) {
        LOGGER.info("Test Suite service: find all test suites from project with id = {} by user with email = {}", projectId, email);

        User user = authUserService.getUserByEmail(email);
        Project project = checkService.findProjectByIdAndUser(projectId, user);

        Optional<List<TestSuite>> optionalTestSuites = testSuiteRepo.findAllByProject(project);
        if (optionalTestSuites.isEmpty()) {
            return new ArrayList<>();
        }
        return testSuiteMapper.toDto(optionalTestSuites.get());
    }

    public TestSuiteDtoResponse updateTestSuite(String email, long projectId, long suiteId, UpdateTestSuiteDtoRequest request) {
        LOGGER.info("Test Suite service: Update test suite (request = {}) with id {} from project with id = {} by user with email = {}", request, suiteId, projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestSuite testSuite = checkService.findTestSuiteByIdAndUserAndProject(suiteId, project);

        if (testSuite.getTitle().equals("defaultTestSuite")) {
            throw new ServerException("Default test suite can't be updated", ServerErrorCode.CANT_UPDATE_DEFAULT_TEST_SUITE);
        }

        testSuiteMapper.updateTestSuite(request, testSuite);

        return testSuiteMapper.toDto(testSuiteRepo.save(testSuite));
    }

    public void deleteTestSuite(String email, long projectId, long suiteId) {
        LOGGER.info("Test Suite service: Delete test suite with id {} from project with id = {} by user with email = {}", suiteId, projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        TestSuite testSuite = checkService.findTestSuiteByIdAndUserAndProject(suiteId, project);

        if (testSuite.getTitle().equals("defaultTestSuite")) {
            throw new ServerException("Default test suite can't be deleted", ServerErrorCode.CANT_DELETE_DEFAULT_TEST_SUITE);
        }
        testSuiteRepo.delete(testSuite);
    }

    public TestSuiteDtoResponse findTestSuiteById(String email, long projectId, long suiteId) {
        LOGGER.info("Test Suite service: Find test suite with id {} from project with id = {} by user with email = {}", suiteId, projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        return testSuiteMapper.toDto(checkService.findTestSuiteByIdAndUserAndProject(suiteId, project));
    }
}
