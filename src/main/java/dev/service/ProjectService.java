package dev.service;

import dev.domain.Project;
import dev.domain.TestSuite;
import dev.domain.User;
import dev.domain.enums.SuiteStatus;
import dev.dto.requests.project.CreateProjectDtoRequest;
import dev.dto.requests.project.UpdateProjectDtoRequest;
import dev.dto.responses.project.ProjectDtoResponse;
import dev.dto.responses.testsuite.ProjectTestSuiteWithCasesDtoResponse;
import dev.dto.responses.project.ProjectWithTestCountDtoResponse;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.ProjectRepo;
import dev.repos.TestCaseRepo;
import dev.repos.TestSuiteRepo;
import dev.repos.UserRepo;
import dev.tools.mapper.ProjectMapper;
import dev.tools.mapper.TestCaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {
    private final Logger LOGGER = LoggerFactory.getLogger(ProjectService.class);

    private final UserRepo userRepo;

    private final ProjectMapper projectMapper;

    private final ProjectRepo projectRepo;

    private final AuthUserService authUserService;
    private final TestCaseRepo testCaseRepo;
    private final TestSuiteRepo testSuiteRepo;
    private final CheckService checkService;
    private final TestCaseMapper testCaseMapper;

    public ProjectService(UserRepo userRepo, ProjectMapper projectMapper, ProjectRepo projectRepo, AuthUserService authUserService, TestCaseRepo testCaseRepo, TestSuiteRepo testSuiteRepo, CheckService checkService, TestCaseMapper testCaseMapper) {
        this.userRepo = userRepo;
        this.projectMapper = projectMapper;
        this.projectRepo = projectRepo;
        this.authUserService = authUserService;
        this.testCaseRepo = testCaseRepo;
        this.testSuiteRepo = testSuiteRepo;
        this.checkService = checkService;
        this.testCaseMapper = testCaseMapper;
    }

    public ProjectDtoResponse createProject(CreateProjectDtoRequest request, String email){
        LOGGER.info("Project Service : Create project with request {} by user with email = {}", request, email);

        Optional<User> optUser = userRepo.findByEmail(email);
        if(optUser.isEmpty()){
            LOGGER.warn("User with email {} doesn't exist", email);
            throw new ServerException("User with this email doesn't exist", ServerErrorCode.USER_NOT_FOUND);
        }

        User user = optUser.get();

        if(!user.isActivated()){
            LOGGER.warn("User with email {} not activated", email);
            throw new ServerException("User not activated(check your mailbox)", ServerErrorCode.USER_NOT_ACTIVATED);
        }


        Project project = projectRepo.save(projectMapper.toProject(request, user));

        //добавление дефолтного сьюта
        TestSuite defaultTestSuite = new TestSuite(0L, "defaultTestSuite", SuiteStatus.Active, "", "", project, new ArrayList<>());
        testSuiteRepo.save(defaultTestSuite);

        return projectMapper.toDto(project);
    }

    public void deleteProject(String email, long projectId){
        LOGGER.info("Project service: Delete project with id {} by user with email {}", projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);
        projectRepo.delete(project);
    }

    public List<ProjectDtoResponse> findAllUserProjects(String email){
        LOGGER.info("Project service: Get all projects by user with email {}", email);

        User user = authUserService.getUserByEmail(email);

        Optional<List<Project>> projectList = projectRepo.findAllByUser(user);
        if(projectList.isEmpty()){
            return new ArrayList<>();
        }
        return projectMapper.toDto(projectList.get());
    }

    public ProjectDtoResponse updateProject(long projectId, String email, UpdateProjectDtoRequest request){
        LOGGER.info("Project service: Update project with id {} by user with email {}", projectId, email);

        User user = authUserService.getUserByEmail(email);
        Project project = checkService.findProjectByIdAndUser(projectId, user);

        projectMapper.updateProjectByDto(project, request);

        return projectMapper.toDto(projectRepo.save(project));
    }

    public ProjectDtoResponse findProjectById(String email, long projectId){
        LOGGER.info("Project service: get project with id = {} by user with email = {}", projectId, email);

        User user = authUserService.getUserByEmail(email);
        Project project = checkService.findProjectByIdAndUser(projectId, user);

        return projectMapper.toDto(project);
    }

    public List<ProjectWithTestCountDtoResponse> findUserProjectsByName(String email, String projectName){
        LOGGER.info("Project service: get user projects with name = {} by user with email = {}", projectName, email);

        User user = authUserService.getUserByEmail(email);

        Optional<List<Project>> optionalProjectList = projectRepo.findByUserAndNameLikeIgnoreCase(user, "%" + projectName + "%");
        if(optionalProjectList.isEmpty()){
            return new ArrayList<>();
        }

        List<ProjectWithTestCountDtoResponse> output = new ArrayList<>();

        for(Project project : optionalProjectList.get()){
            int projectTestCaseCount = testCaseRepo.countAllByProject(project).orElse(0);
            int projectTestSuiteCount = testSuiteRepo.countAllByProject(project).orElse(0);
            output.add(new ProjectWithTestCountDtoResponse(project.getId(), project.getName(), project.getDescription(), projectTestCaseCount, projectTestSuiteCount));
        }

        return output;
    }

    public List<ProjectWithTestCountDtoResponse> findAllProjectsWithTestCasesAndSuitesCount(String email){
        LOGGER.info("Project service: get user projects with test case and suite count by user with email = {}", email);
        //!!!по хорошему сделать через джоин и проекции.
        //пока что пусть будет так
        User user = authUserService.getUserByEmail(email);

        Optional<List<Project>> optionalUserProjectList = projectRepo.findAllByUserOrderByIdDesc(user);
        if(optionalUserProjectList.isEmpty()){
            return new ArrayList<>();
        }

        List<ProjectWithTestCountDtoResponse> output = new ArrayList<>();

        for(Project project : optionalUserProjectList.get()){
            int projectTestCaseCount = testCaseRepo.countAllByProject(project).orElse(0);
            int projectTestSuiteCount = testSuiteRepo.countAllByProject(project).orElse(0);
            output.add(new ProjectWithTestCountDtoResponse(project.getId(), project.getName(), project.getDescription(), projectTestCaseCount, projectTestSuiteCount));
        }

        return output;
    }

    public List<ProjectTestSuiteWithCasesDtoResponse> findProjectTestSuiteWithCases(String email, long projectId){
        LOGGER.info("Project service: get all project(with id {}) test suites and their cases by user with email = {}", projectId, email);

        User user = authUserService.getUserByEmail(email);

        Project project = checkService.findProjectByIdAndUser(projectId, user);

        Optional<List<TestSuite>> projectTestSuites = testSuiteRepo.findAllByProject(project);
        if(projectTestSuites.isEmpty()) {
            return new ArrayList<>();
        }

        List<ProjectTestSuiteWithCasesDtoResponse> output = new ArrayList<>();
        for(TestSuite suite : projectTestSuites.get()){
            ProjectTestSuiteWithCasesDtoResponse item = new ProjectTestSuiteWithCasesDtoResponse(suite.getId(),
                    suite.getTitle(), suite.getStatus(), suite.getDescription(), suite.getPreConditions(),
                    testCaseRepo.findAllByTestSuite(suite).stream().map(testCaseMapper::testCaseToDto).toList());
            output.add(item);
        }
        return output;
    }
}
