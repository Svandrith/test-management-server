CREATE TABLE IF NOT EXISTS public.test_suite_test_plan (
                                             test_suite_id int8 NOT NULL,
                                             test_plan_id int8 NOT NULL,
                                             FOREIGN KEY (test_suite_id) REFERENCES test_suite(id) ON DELETE CASCADE,
                                             FOREIGN KEY (test_plan_id) REFERENCES test_plan(id) ON DELETE CASCADE
);