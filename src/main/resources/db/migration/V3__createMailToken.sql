CREATE TABLE IF NOT EXISTS mail_token (

                        user_id               bigserial    NOT NULL,
                        user_email            VARCHAR(70)  NOT NULL,
                        accept_token          VARCHAR(255) NOT NULL,

                        PRIMARY KEY (user_id),
                        FOREIGN KEY (user_id) REFERENCES "user"(id)
);