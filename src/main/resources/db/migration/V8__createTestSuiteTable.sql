CREATE TABLE IF NOT EXISTS test_suite (
                                   id               bigserial   NOT NULL,
                                   description      varchar(255) NULL,
                                   pre_conditions   varchar(255) NULL,
                                   status           varchar(9) NULL,
                                   title            varchar(255) NULL,
                                   project_id       int8 NOT NULL,

                                   PRIMARY KEY (id),
                                   FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE CASCADE
);