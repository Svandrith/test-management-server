CREATE TABLE IF NOT EXISTS plan_case_result (
                                         case_id int8 NOT NULL,
                                         plan_id int8 NOT NULL,
                                         "result" varchar(255) NOT NULL,
                                         CONSTRAINT plan_case_result_pkey PRIMARY KEY (case_id, plan_id),
                                         FOREIGN KEY (plan_id) REFERENCES test_plan(id) ON DELETE CASCADE,
                                         FOREIGN KEY (case_id) REFERENCES test_case(id) ON DELETE CASCADE
);