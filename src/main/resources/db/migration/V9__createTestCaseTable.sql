CREATE TABLE IF NOT EXISTS test_case (
                                  id                bigserial   NOT NULL,
                                  description       varchar(255) NULL,
                                  post_conditions   varchar(255) NULL,
                                  pre_conditions    varchar(255) NULL,
                                  behavior          varchar(12) NULL,
                                  priority          varchar(7) NULL,
                                  severity          varchar(9) NULL,
                                  status            varchar(8) NOT NULL,
                                  title             varchar(255) NULL,
                                  "type"            varchar(11) NULL,
                                  project_id        int8 NOT NULL,
                                  test_suite_id     int8 NOT NULL,

                                  PRIMARY KEY (id),
                                  FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE CASCADE,
                                  FOREIGN KEY (test_suite_id) REFERENCES test_suite(id) ON DELETE CASCADE
);