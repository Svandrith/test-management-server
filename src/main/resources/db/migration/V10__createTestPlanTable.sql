CREATE TABLE IF NOT EXISTS test_plan (
                                  id                bigserial not null,
                                  creation_date     timestamp not NULL,
                                  description       varchar(255) NULL,
                                  last_update_date  timestamp NULL,
                                  "name" varchar(255) NULL,
                                  status varchar(255) not NULL,
                                  project_id int8 NOT NULL,
                                  PRIMARY KEY (id),
                                  FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE CASCADE
);