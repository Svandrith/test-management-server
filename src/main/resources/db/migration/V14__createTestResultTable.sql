CREATE TABLE IF NOT EXISTS test_result (
                                    id bigserial NOT NULL,
                                    "comment" varchar(255) NULL,
                                    "result" varchar(8) NULL,
                                    test_date timestamp NULL,
                                    test_case_id int8 NOT NULL,
                                    PRIMARY KEY (id),
                                    FOREIGN KEY (test_case_id) REFERENCES test_case(id) ON DELETE CASCADE
);