CREATE TABLE IF NOT EXISTS projects (

                        id               bigserial    NOT NULL,
                        name             VARCHAR(500) NOT NULL,
                        description      VARCHAR(1500),
                        user_id          bigint  NOT NULL,

                        PRIMARY KEY (id),
                        FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE
);