ALTER TABLE test_suite ADD CONSTRAINT test_suite_unique_project_id_title UNIQUE (title, project_id);
ALTER TABLE test_case ADD CONSTRAINT test_case_unique_project_id_title UNIQUE (title, project_id);
