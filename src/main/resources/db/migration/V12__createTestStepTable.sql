CREATE TABLE IF NOT EXISTS step (
                             id bigserial NOT NULL,
                             description varchar(255) NULL,
                             expected_result varchar(255) NULL,
                             test_case_id int8 NULL,
                             PRIMARY KEY (id),
                             FOREIGN KEY (test_case_id) REFERENCES test_case(id) ON DELETE CASCADE
);