package tester.registration;

import io.restassured.RestAssured;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;




public class RegistrationNegativeTests extends Basic {

    @Test
    public void regNeg2Test() throws JSONException {
        String email = "@g.c";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);




        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }
    @Test
    public void regNeg3Test() throws JSONException {
        String email = "sd@g.c";
        String password = "1234#qw";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg4Test() throws JSONException {
        String email = "sd@g.c";
        String password = "1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerttyqwertrt";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg5Test() throws JSONException {
        String email = "sd@g.c";
        String password = "1234@qwerty";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg6Test() throws JSONException {
        String email = "sd@g.c";
        String password = "1234qwerty";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg7Test() throws JSONException {
        String email = "sd@g.c";
        String password = "qwer#qwerty";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }
    @Test
    public void regNeg8Test() throws JSONException {
        String email = "sd@g.c";
        String password = "1234#1234";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg9Test() throws JSONException {
        String email = "sd@g.c";
        String password = "1234qwerty";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg10Test() throws JSONException {
        String email = "viktorias";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg11Test() throws JSONException {
        String email = "viktorias@";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }
    @Test
    public void regNeg12Test() throws JSONException {
        String email = "viktorias@gmail";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }
    @Test
    public void regNeg13Test() throws JSONException {
        String email = "viktorias@gmail.";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg15Test() throws JSONException {
        String email = ".mailcomdot";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg16Test() throws JSONException {
        String email = ".mailcomdot.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg17Test() throws JSONException {
        String email = "@mailcomdot.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg18Test() throws JSONException {
        String email = "viktoriasgmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg19Test() throws JSONException {
        String email = "viktorias@.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg20Test() throws JSONException {
        String email = "viktorias@qwercom";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }


    @Test
    public void regNeg22Test() throws JSONException {
        String email = "viktorias@.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg24Test() throws JSONException {
        String email = "viktorias.gmail@com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg25Test() throws JSONException {
        String email = "viktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasvi@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg26Test() throws JSONException {
        String email = "viktorias@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword","1234#qwerty");


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

    @Test
    public void regNeg27Test() throws JSONException {
        String email = "viktoriaszz@@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }
    @Test
    public void regNeg28Test() throws JSONException {
        String email = "viktoriasas@gmail..com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.port = 8081;
        RestAssured.baseURI = RestAssured.DEFAULT_URI;


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(400);
    }

}



