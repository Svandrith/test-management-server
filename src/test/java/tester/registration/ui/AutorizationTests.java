package tester.registration.ui;

import io.restassured.RestAssured;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import tester.registration.Basic;

public class AutorizationTests extends Basic {
    private static WebDriver driver;

    @BeforeAll
    public static void setUp(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\79136\\Desktop\\diploma\\drivers\\chromedriver.exe");
        ChromeOptions ops = new ChromeOptions();
        ops.addArguments("--remote-allow-origins=*");
         driver = new ChromeDriver(ops);
    }

    @Test
    public void AuthTest2() throws JSONException {
        String email = "vitekRealkrasavchik@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


//        RestAssured.given().
//                header("Content-Type", "application/json").
//                body(body.toString()).
//                post("/register").
//                then().log().ifValidationFails().
//                assertThat().statusCode(200);



            driver.get("http://localhost:3000/signin");

            driver.findElement(By.name("email")).sendKeys("vitekRealkrasavchik@gmail.com");
            driver.findElement(By.name("password")).sendKeys("1234#qwer");
            if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
                driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
            }

            RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                    get("/whoami").
                    then().assertThat().statusCode(200);


        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

    @Test
    public void AuthTest3() throws JSONException {
        String email = "vitekRealkrasavchik@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);


        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekRealkrasavchik@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(200);


        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }


    @Test
    public void AuthTest4() throws JSONException {
        String email = "vitekRealkrasavchik@gmail.com";
        String password = "qwerty#1234qwerty#1234qwerty#1";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);


        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekRealkrasavchik@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(200);


        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

    @Test
    public void AuthTest5() throws JSONException {
        String email = "vitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavch@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);


        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavch@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavch@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(200);


        RestAssured.given().auth().basic("vitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavch@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }


    @Test
    public void AuthTest6() throws JSONException {
        String email = "vitekKresavchikvitek@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);


        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekKresavchikvitek@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekKresavchikvitek@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(200);


        RestAssured.given().auth().basic("vitekKresavchikvitek@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }


    @Test
    public void AuthTest7() throws JSONException {

        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekKresavchikvitek@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekKresavchikvitek@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(200);


        RestAssured.given().auth().basic("vitekKresavchikvitek@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

}
