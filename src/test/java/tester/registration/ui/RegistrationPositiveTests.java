package tester.registration.ui;

import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import tester.registration.Basic;

public class RegistrationPositiveTests extends Basic {
    private WebDriver driver;

    @Test
    public void regPos2Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekRealkrasavchik@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

    @Test
    public void regPos3Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekRealkrasavchik@gmail.com");
        driver.findElement(By.name("password")).sendKeys("qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#");
        driver.findElement(By.name("confirmPassword")).sendKeys("qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

    @Test
    public void regPos4Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekRealkrasavchik@gmail.com");
        driver.findElement(By.name("password")).sendKeys("qwerty#1234qwerty#1234qwerty#1");
        driver.findElement(By.name("confirmPassword")).sendKeys("qwerty#1234qwerty#1234qwerty#1");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","qwerty#1234qwerty#1234qwerty#1").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

    @Test
    public void regPos5Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavch@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavch@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

    @Test
    public void regPos6Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekKresavchikvitek@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekKresavchikvitek@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }
    @AfterMethod
    public void BrowsQuit() {
        driver.quit();
    }
}



