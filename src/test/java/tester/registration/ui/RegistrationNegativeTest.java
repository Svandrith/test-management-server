package tester.registration.ui;

import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import tester.registration.Basic;

public class RegistrationNegativeTest extends Basic {

    private WebDriver driver;

    @Test
    public void regNegPos2Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("vitekRealkrasavchik@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
}

    @Test
    public void regNegPos3Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("sd@g.c");
        driver.findElement(By.name("password")).sendKeys("1234#qw");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qw");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("sd@g.c","1234#qw").
                get("/whoami").
                then().assertThat().statusCode(400);
}

    @Test
    public void regNegPos4Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("sd@g.c");
        driver.findElement(By.name("password")).sendKeys("1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerttyqwertrt");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerttyqwertrt");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("sd@g.c","1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerty1234#qwerttyqwertrt").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    @Test
    public void regNegPos5Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("sd@g.c");
        driver.findElement(By.name("password")).sendKeys("1234@qwerty");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234@qwerty");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("sd@g.c","1234@qwerty").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos6Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("sd@g.c");
        driver.findElement(By.name("password")).sendKeys("1234qwerty");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234qwerty");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("sd@g.c","1234qwerty").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos7Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("sd@g.c");
        driver.findElement(By.name("password")).sendKeys("qwer#qwerty");
        driver.findElement(By.name("confirmPassword")).sendKeys("qwer#qwerty");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("sd@g.c","qwer#qwerty").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos8Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("sd@g.c");
        driver.findElement(By.name("password")).sendKeys("1234#1234");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#1234");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("sd@g.c","1234#1234").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos9Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("sd@g.c");
        driver.findElement(By.name("password")).sendKeys("1234qwerty");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234qwerty");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("sd@g.c","1234qwerty").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos10Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias");
        driver.findElement(By.name("password")).sendKeys("1234qwerty");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234qwerty");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias","1234qwerty").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos11Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias@");
        driver.findElement(By.name("password")).sendKeys("1234qwerty");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234qwerty");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias@","1234qwerty").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos12Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias@gmail");
        driver.findElement(By.name("password")).sendKeys("1234qwerty");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234qwerty");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias@gmail","1234qwerty").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos13Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias@gmail.");
        driver.findElement(By.name("password")).sendKeys("1234qwerty");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234qwerty");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias@gmail.","1234qwerty").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos14Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("mailcomdot");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("mailcomdot","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos15Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys(".mailcomdot");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic(".mailcomdot","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos16Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys(".mailcomdot.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic(".mailcomdot.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos17Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("@mailcomdot.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("@mailcomdot.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos18Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktoriasgmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktoriasgmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos19Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias@.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias@.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos20Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias@qwercom");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias@qwercom","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos21Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias@.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias@.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos22Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias@.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias@.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos23Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias.gmail@com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias.gmail@com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos24Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasvi@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasviktoriasvi@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos25Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktorias@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktorias@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos26Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktoriaszz@@gmail.com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktoriaszz@@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

    public void regNegPos27Test() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        driver=new ChromeDriver();
        driver.get("http://localhost:3000/signup");

        driver.findElement(By.name("email")).sendKeys("viktoriasas@gmail..com");
        driver.findElement(By.name("password")).sendKeys("1234#qwer");
        driver.findElement(By.name("confirmPassword")).sendKeys("1234#qwer");
        if(driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).isEnabled()) {
            driver.findElement(By.name("ant-btn ant-btn-default button signin__button")).click();
        }

        RestAssured.given().auth().basic("viktoriasas@gmail..com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(400);
    }

}