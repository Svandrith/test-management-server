package tester.registration;
import io.restassured.RestAssured;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;


public class CreateProjectTests extends Basic {
    //tmstests64@gmail.com//TMSTests#1
    @Test
    public void AuthTest2() throws JSONException {
        String name = "projectCheck";
        String description = "DescriptionCheck";
        JSONObject body = new JSONObject();
        body.put("name", name);
        body.put("description", description);


        RestAssured.given().header("Content-Type", "application/json").auth().basic("tmstests64@gmail.com", "1234#qwer").
                body(body.toString()).post("/api/v1/projects").then().statusCode(200);


//        System.out.println(body.toString());
//        RestAssured.given().header("Content-Type", "application/json").
//                body(body.toString()).
//                post("/api/v1/projects").then().statusCode(200);


}}