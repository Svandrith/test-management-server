package tester.registration;
import io.restassured.RestAssured;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;



public class LogoutTests extends Basic {

    @Test
    public void logOut2() throws JSONException {
        String email = "vitekRealkrasavchik@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                get("/logout").
                then().assertThat().statusCode(204);

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

    @Test
    public void logOutTest3() throws JSONException {


        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com", "1234#qwer").
                get("/login").
                then().assertThat().statusCode(401);

    }
}
