package tester.registration;
import io.restassured.RestAssured;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;




public class RegistrationPositiveTests extends Basic{


    @Test
    public void regPos2Test() throws JSONException {
        String email = "vitekRealkrasavchik@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }



    @Test
    public void regPos3Test() throws JSONException {
        String email = "vitekRealkrasavchik@gmail.com";
        String password = "qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#1234qwerty#").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }



    @Test
    public void regPos4Test() throws JSONException {
        String email = "vitekRealkrasavchik@gmail.com";
        String password = "qwerty#1234qwerty#1234qwerty#1";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);

        RestAssured.given().auth().basic("vitekRealkrasavchik@gmail.com","qwerty#1234qwerty#1234qwerty#1").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);

    }


    @Test
    public void regPos5Test() throws JSONException {
        String email = "vitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavch@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);


        RestAssured.given().auth().basic("vitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavchikvitekKresavch@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }

    @Test
    public void regPos6Test() throws JSONException {
        String email = "vitekKresavchikvitek@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);

        RestAssured.given().auth().basic("vitekKresavchikvitek@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }




}
