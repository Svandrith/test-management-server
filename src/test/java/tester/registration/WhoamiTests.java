package tester.registration;

import io.restassured.RestAssured;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

public class WhoamiTests extends Basic {

    @Test
    public void whoAmITest() throws JSONException {
        String email = "vitekKresavchikvitek@gmail.com";
        String password = "1234#qwer";
        JSONObject body = new JSONObject();
        body.put("email",email);
        body.put("password",password);
        body.put("confirmPassword",password);


        RestAssured.given().
                header("Content-Type", "application/json").
                body(body.toString()).
                post("/register").
                then().log().ifValidationFails().
                assertThat().statusCode(200);;

        RestAssured.given().auth().basic("vitekKresavchikvitek@gmail.com","1234#qwer").
                get("/whoami").
                then().assertThat().statusCode(200);

        RestAssured.given().auth().basic("vitekKresavchikvitek@gmail.com","1234#qwer").
                delete("/api/v1/users").
                then().assertThat().statusCode(204);
    }
}
