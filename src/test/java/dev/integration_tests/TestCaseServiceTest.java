package dev.integration_tests;

import dev.domain.*;
import dev.domain.enums.*;
import dev.dto.requests.testcase.UpdateStepDtoRequest;
import dev.dto.requests.testcase.UpdateTestCaseDtoRequest;
import dev.dto.responses.testcase.TestCaseDtoResponse;
import dev.repos.*;
import dev.service.AuthUserService;
import dev.service.CheckService;
import dev.service.TestCaseService;
import dev.tools.mapper.TestCaseMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop",
        "spring.flyway.enabled=false"
})
public class TestCaseServiceTest {
    @Autowired
    private ProjectRepo projectRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private TestCaseRepo testCaseRepo;
    @Autowired
    private StepRepo stepRepo;
    @Autowired
    private TestSuiteRepo testSuiteRepo;
    @Autowired
    private PlanCaseResultRepo planCaseResultRepo;
    private TestCaseMapper testCaseMapper;
    private AuthUserService authUserService;
    private CheckService checkService;
    private TestCaseService testCaseService;

    @BeforeEach
    public void setUp(){
        testCaseMapper = new TestCaseMapper(checkService);
        authUserService = new AuthUserService(userRepo);
        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);
        testCaseService = new TestCaseService(testCaseMapper, stepRepo, testCaseRepo, testSuiteRepo, authUserService, checkService);
    }

    @Test
    public void testAddTestCase(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(1L, project, new ArrayList<>()));

        TestCaseDtoResponse response = testCaseService.addTestCase("email", project.getId(), addTestCaseDtoRequest(testSuite.getId()));
        assertAll(
                () -> assertEquals("title", response.getTitle()),
                () -> assertEquals("description", response.getDescription()),
                () -> assertEquals("preCond", response.getPreConditions()),
                () -> assertEquals("postCond", response.getPostConditions()),
                () -> assertEquals(2, response.getSteps().size()),
                () -> assertEquals(CaseStatus.Active, response.getStatus()),
                () -> assertEquals(CaseSeverity.Medium, response.getSeverity()),
                () -> assertEquals(CasePriority.Low, response.getPriority()),
                () -> assertEquals(CaseType.Usability, response.getType()),
                () -> assertEquals(CaseBehavior.Positive, response.getBehavior()),
                () -> assertEquals(project.getId(), response.getProjectId()),
                () -> assertEquals(testSuite.getId(), response.getTestSuiteId())
        );
    }

    @Test
    public void testAddTestCase_sameCasesToAdd(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(1L, project, new ArrayList<>()));

        testCaseService.addTestCase("email", project.getId(), addTestCaseDtoRequest(testSuite.getId()));

        assertThrows(DataIntegrityViolationException.class, () -> testCaseService.addTestCase("email", project.getId(), addTestCaseDtoRequest(testSuite.getId())));
    }

    @Test
    public void testDeleteTestCase(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(1L, project, new ArrayList<>()));
        Step step1 = step(1, "desc", "exp1");
        Step step2 = step(2, "desc2", "exp2");
        TestCase testCase = testCaseRepo.save(testCase(1, project, testSuite, Arrays.asList(step1, step2)));

        testCaseService.deleteTestCase("email", testCase.getId(), project.getId());

        assertTrue(testCaseRepo.findById(testCase.getId()).isEmpty());

        //Проверка, что удалились шаги, соответствующие этому кейсу
        Iterable<Step> list = stepRepo.findAll();
        assertFalse(list.iterator().hasNext());
    }

    @Test
    public void testUpdateTestCase(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(1L, project, new ArrayList<>()));
        TestSuite newTestSuite = testSuiteRepo.save(testSuite(2L, "title2", project, new ArrayList<>()));
        Step step1 = step(1, "desc", "exp1");
        Step step2 = step(2, "desc2", "exp2");
        TestCase testCase = testCaseRepo.save(testCase(1, project, testSuite, Arrays.asList(step1, step2)));

        UpdateTestCaseDtoRequest request = updateTestCaseDtoRequest();
        List<UpdateStepDtoRequest> newSteps = new ArrayList<>();
        for(Step step : testCase.getSteps()){
            newSteps.add(updateStepDtoRequest(step.getId(), "new_desc", "new_exp"));
        }

        request.setSteps(newSteps);
        request.setTestSuiteId(newTestSuite.getId());

        testCaseService.updateTestCase("email", testCase.getId(), project.getId(), request);

        Optional<TestCase> optionalUpdatedTestCase = testCaseRepo.findById(testCase.getId());
        assertTrue(optionalUpdatedTestCase.isPresent());
        TestCase updatedTestCase = optionalUpdatedTestCase.get();
        List<Step> updatedSteps = updatedTestCase.getSteps();

        Step firstUpdatedStep = updatedSteps.get(0);
        Step secondUpdatedStep = updatedSteps.get(1);

        assertAll(
                () -> assertEquals("new_title", updatedTestCase.getTitle()),
                () -> assertEquals("new_descriptions", updatedTestCase.getDescription()),
                () -> assertEquals("new_preConditions", updatedTestCase.getPreConditions()),
                () -> assertEquals("new_postConditions", updatedTestCase.getPostConditions()),
                () -> assertEquals(newTestSuite.getId(), updatedTestCase.getTestSuite().getId()),
                () -> assertEquals("new_desc", firstUpdatedStep.getDescription()),
                () -> assertEquals("new_desc", secondUpdatedStep.getDescription()),
                () -> assertEquals("new_exp", secondUpdatedStep.getExpectedResult()),
                () -> assertEquals("new_exp", secondUpdatedStep.getExpectedResult())
        );
    }

    @Test
    public void testFindByIdTestCase(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(1L, project, new ArrayList<>()));
        Step step1 = step(1, "desc", "exp1");
        Step step2 = step(2, "desc2", "exp2");
        TestCase testCase = testCaseRepo.save(testCase(1, project, testSuite, Arrays.asList(step1, step2)));

        Optional<TestCase> optionalFoundTestCase = testCaseRepo.findById(testCase.getId());
        assertTrue(optionalFoundTestCase.isPresent());
        TestCase foundTestCase = optionalFoundTestCase.get();

        assertAll(
                () -> assertEquals("title", foundTestCase.getTitle()),
                () -> assertEquals("desc", foundTestCase.getDescription()),
                () -> assertEquals("preCond", foundTestCase.getPreConditions()),
                () -> assertEquals("postCond", foundTestCase.getPostConditions()),
                () -> assertEquals(CaseStatus.Active, foundTestCase.getStatus()),
                () -> assertEquals(CaseSeverity.Medium, foundTestCase.getSeverity()),
                () -> assertEquals(CasePriority.Low, foundTestCase.getPriority()),
                () -> assertEquals(CaseType.Usability, foundTestCase.getType()),
                () -> assertEquals(CaseBehavior.Positive, foundTestCase.getBehavior())
        );
    }
}
