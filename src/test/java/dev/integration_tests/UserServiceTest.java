package dev.integration_tests;

import dev.domain.User;
import dev.repos.UserRepo;
import dev.service.AuthUserService;
import dev.service.UserService;
import dev.tools.MyMailSender;
import dev.tools.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.mail.Session;
import java.util.Optional;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop",
        "spring.flyway.enabled=false"
})
public class UserServiceTest {
    @Autowired
    private UserRepo userRepo;
    private UserMapper userMapper;
    private MyMailSender mailSender;
    private AuthUserService authUserService;
    private PasswordEncoder passwordEncoder;
    private UserService userService;
    private Session session;

    @BeforeEach
    public void setUp(){
        passwordEncoder = new BCryptPasswordEncoder();
        mailSender = new MyMailSender(session);
        userMapper = new UserMapper(passwordEncoder);
        authUserService = new AuthUserService(userRepo);
        userService = new UserService(userRepo, userMapper, mailSender, authUserService);
    }

    @Test
    public void testDeleteUser(){
        User savedUser = userRepo.save(user());
        userService.deleteUser(savedUser.getEmail());

        Optional<User> userAfterDelete = userRepo.findById(savedUser.getId());
        assertTrue(userAfterDelete.isEmpty());
    }




}
