package dev.integration_tests;

import dev.domain.Project;
import dev.domain.TestCase;
import dev.domain.TestSuite;
import dev.domain.User;
import dev.domain.enums.SuiteStatus;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;
import dev.repos.*;
import dev.service.AuthUserService;
import dev.service.CheckService;
import dev.service.TestSuiteService;
import dev.tools.mapper.TestSuiteMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static dev.TestData.*;
import static dev.TestData.addTestCaseDtoRequest;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop",
        "spring.flyway.enabled=false",
        "spring.jpa.generate-ddl=true"
        })
public class TestSuiteServiceTest {
    @Autowired
    private TestSuiteRepo testSuiteRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ProjectRepo projectRepo;
    @Autowired
    private TestCaseRepo testCaseRepo;
    @Autowired
    private PlanCaseResultRepo planCaseResultRepo;
    private TestSuiteMapper testSuiteMapper;
    private AuthUserService authUserService;
    private CheckService checkService;
    private TestSuiteService testSuiteService;

    @BeforeEach
    void setUp() {
        testSuiteMapper = new TestSuiteMapper();
        authUserService = new AuthUserService(userRepo);

        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);
        testSuiteService = new TestSuiteService(testSuiteRepo, testSuiteMapper, authUserService, checkService);
    }

    @Test
    public void testAddTestSuite(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));

        TestSuiteDtoResponse response = testSuiteService.addTestSuite("email", project.getId(), addTestSuiteDtoRequest());

        assertAll(
                () -> assertEquals("title",response.getTitle()),
                () -> assertEquals(SuiteStatus.Active, response.getStatus()),
                () -> assertEquals("desc", response.getDescription()),
                () -> assertEquals("preCond", response.getPreConditions())
        );
    }
    @Test
    public void testAddTestSuite_sameSuitesToAdd(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));

        testSuiteService.addTestSuite("email", project.getId(), addTestSuiteDtoRequest());

        assertThrows(DataIntegrityViolationException.class, () -> testSuiteService.addTestSuite("email", project.getId(), addTestSuiteDtoRequest()));
    }

    @Test
    public void testFindAllTestSuitesByProject(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite firstTestSuite = testSuiteRepo.save(testSuite(1L, "firstSuite", project, new ArrayList<>()));
        TestSuite secondTestSuite = testSuiteRepo.save(testSuite(2L, "secondSuite" ,project, new ArrayList<>()));
        TestSuite thirdTestSuite = testSuiteRepo.save(testSuite(3L, "thirdSuite", project, new ArrayList<>()));

        List<TestSuiteDtoResponse> response =  testSuiteService.findAllTestSuitesByProject("email", project.getId());

        assertAll(
                () -> assertEquals(3, response.size())
        );
    }

    @Test
    public void testUpdateTestSuite(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(1L, project, new ArrayList<>()));

        TestSuiteDtoResponse updatedTestSuiteResponse = testSuiteService.updateTestSuite("email", project.getId(), testSuite.getId(), updateTestSuiteDtoRequest("newTitle", SuiteStatus.Outdated, "newDesc", "newPreCond"));

        assertAll(
                () -> assertEquals("newTitle",updatedTestSuiteResponse.getTitle()),
                () -> assertEquals(SuiteStatus.Outdated, updatedTestSuiteResponse.getStatus()),
                () -> assertEquals("newDesc", updatedTestSuiteResponse.getDescription()),
                () -> assertEquals("newPreCond", updatedTestSuiteResponse.getPreConditions())
        );
    }

    @Test
    public void testDeleteTestSuite(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestCase testCase = testCaseRepo.save(testCase(0, project, testSuite, new ArrayList<>()));

        testSuiteService.deleteTestSuite("email", project.getId(), testSuite.getId());

        assertTrue(testSuiteRepo.findById(testSuite.getId()).isEmpty());
    }

    @Test
    public void testFindTestSuiteById(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(1L, project, new ArrayList<>()));

        TestSuiteDtoResponse response = testSuiteService.findTestSuiteById("email", project.getId(), testSuite.getId());

        assertAll(
                () -> assertEquals("title",response.getTitle()),
                () -> assertEquals(SuiteStatus.Active, response.getStatus()),
                () -> assertEquals("description", response.getDescription()),
                () -> assertEquals("preConditions", response.getPreConditions())
        );
    }
}
