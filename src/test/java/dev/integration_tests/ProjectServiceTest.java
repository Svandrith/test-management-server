package dev.integration_tests;

import dev.domain.Project;
import dev.domain.TestCase;
import dev.domain.TestSuite;
import dev.domain.User;
import dev.dto.requests.project.CreateProjectDtoRequest;
import dev.dto.responses.project.ProjectDtoResponse;
import dev.dto.responses.testsuite.ProjectTestSuiteWithCasesDtoResponse;
import dev.dto.responses.project.ProjectWithTestCountDtoResponse;
import dev.repos.*;
import dev.service.AuthUserService;
import dev.service.CheckService;
import dev.service.ProjectService;
import dev.tools.mapper.ProjectMapper;
import dev.tools.mapper.TestCaseMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.*;



@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop",
        "spring.flyway.enabled=false"
})
public class ProjectServiceTest {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ProjectRepo projectRepo;
    @Autowired
    private TestCaseRepo testCaseRepo;
    @Autowired
    private TestSuiteRepo testSuiteRepo;
    @Autowired
    private PlanCaseResultRepo planCaseResultRepo;
    private ProjectMapper projectMapper;
    private TestCaseMapper testCaseMapper;
    private AuthUserService authUserService;
    private ProjectService projectService;
    private CheckService checkService;

    @BeforeEach
    public void setUp(){
        projectMapper = new ProjectMapper();
        testCaseMapper = new TestCaseMapper(checkService);
        authUserService = new AuthUserService(userRepo);
        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);
        projectService = new ProjectService(userRepo, projectMapper, projectRepo, authUserService, testCaseRepo, testSuiteRepo, checkService, testCaseMapper);
    }

    @Test
    public void testCreateProject(){
        User user = userRepo.save(user());
        CreateProjectDtoRequest request = createProjectDtoRequest();

        ProjectDtoResponse response = projectService.createProject(request, user.getEmail());
        Optional<Project> project = projectRepo.findById(response.getId());
        assertFalse(project.isEmpty());
    }

    @Test
    public void testDeleteProject(){
        User savedUser = userRepo.save(user());
        Project savedProject = projectRepo.save(project(savedUser));

        projectService.deleteProject("email", savedProject.getId());

        assertTrue(projectRepo.findById(savedProject.getId()).isEmpty());
    }

    @Test
    public void testUpdateProject(){
        User savedUser = userRepo.save(user());
        Project savedProject = projectRepo.save(project(savedUser));

        ProjectDtoResponse response = projectService.updateProject(savedProject.getId(), "email", updateProjectDtoRequest());

        assertAll(
                () -> assertEquals(response.getId(), savedProject.getId()),
                () -> assertEquals("newName", savedProject.getName()),
                () -> assertEquals("newDesc", savedProject.getDescription())
        );
    }

    @Test
    public void testUpdateProject_updateOnlyName(){
        User savedUser = userRepo.save(user());
        Project savedProject = projectRepo.save(project(savedUser));
        //String oldName = savedProject.getName();

        ProjectDtoResponse response = projectService.updateProject(savedProject.getId(), "email", updateProjectDtoRequest("newName"));
        assertAll(
                () -> assertEquals(response.getId(), savedProject.getId()),
                () -> assertEquals("newName", savedProject.getName()),
                () -> assertEquals("description", savedProject.getDescription())
        );
    }

    /*@Test
    public void testFindAllProjects(){
        User user = userRepo.save(user());
        projectRepo.save(project(user));
        projectRepo.save(project(user));

        List<ProjectDtoResponse> actual = projectService.findAllUserProjects("email");

        assertEquals(2, actual.size());

        for(ProjectDtoResponse item : actual){
            assertAll(
                    () -> assertEquals("name", item.getName()),
                    () -> assertEquals("description", item.getDescription())
            );
        }
    }*/

    @Test
    public void testFindProjectById(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));

        ProjectDtoResponse projectDtoResponse = projectService.findProjectById(user.getEmail(), project.getId());
        assertAll(
                () -> assertEquals(project.getId(), projectDtoResponse.getId()),
                () -> assertEquals(project.getDescription(), projectDtoResponse.getDescription()),
                () -> assertEquals(project.getName(), projectDtoResponse.getName())
        );
    }

    @Test
    public void testFindUserProjectsWithTestCount(){
        User user = userRepo.save(user());
        Project firstProject = projectRepo.save(project(user));
        Project secondProject = projectRepo.save(project(user));

        TestSuite firstProjectTestSuite  = testSuiteRepo.save(testSuite(0, firstProject, new ArrayList<>()));
        TestSuite secondProjectTestSuite  = testSuiteRepo.save(testSuite(0, "title2", secondProject, new ArrayList<>()));

        testCaseRepo.save(testCase(0, firstProject, firstProjectTestSuite, new ArrayList<>()));
        testCaseRepo.save(testCase(0, "title2", secondProject, secondProjectTestSuite, new ArrayList<>()));

        List<ProjectWithTestCountDtoResponse> response = projectService.findAllProjectsWithTestCasesAndSuitesCount("email");

        assertAll(
                () -> assertEquals(2, response.size()),
                () -> assertEquals(firstProject.getId(), response.get(1).getId()),
                () -> assertEquals(secondProject.getId(), response.get(0).getId()),
                () -> assertEquals(1, response.get(0).getTestSuiteCount()),
                () -> assertEquals(1, response.get(1).getTestSuiteCount()),
                () -> assertEquals(1, response.get(0).getTestCaseCount()),
                () -> assertEquals(1, response.get(1).getTestCaseCount())
        );
    }

    @Test
    public void testFindAllUserProjectsByName(){
        User user = userRepo.save(user());

        projectRepo.save(project(0, "pr_name", "desc", user));
        projectRepo.save(project(0, "project_name", "desc", user));

        List<ProjectWithTestCountDtoResponse> response = projectService.findUserProjectsByName("email", "pr_n");

        assertAll(
                () -> assertEquals(1, response.size()),
                () -> assertEquals("pr_name", response.get(0).getName())
        );
    }

    @Test
    public void testFindProjectTestSuitesWithTestCases(){
        User user = userRepo.save(user());

        Project project = projectRepo.save(project(0, "pr_name", "desc", user));

        TestSuite firstTestSuite = testSuiteRepo.save(testSuite(0, "title1", project, new ArrayList<>()));
        TestSuite secondTestSuite = testSuiteRepo.save(testSuite(0, "title2", project, new ArrayList<>()));

        TestCase firstTestCase = testCaseRepo.save(testCase(0, "title1", project, firstTestSuite, new ArrayList<>()));
        TestCase secondTestCase = testCaseRepo.save(testCase(0, "title2", project, firstTestSuite, new ArrayList<>()));
        TestCase thirdTestCase = testCaseRepo.save(testCase(0, "title3", project, secondTestSuite, new ArrayList<>()));
        TestCase fourthTestCase = testCaseRepo.save(testCase(0, "title4", project, secondTestSuite, new ArrayList<>()));

        List<ProjectTestSuiteWithCasesDtoResponse> response = projectService.findProjectTestSuiteWithCases("email", project.getId());

        assertAll(
                () -> assertEquals(2, response.size()),
                () -> assertEquals(2, response.get(0).getTestCases().size()),
                () -> assertEquals(2, response.get(1).getTestCases().size())
        );
    }

}
