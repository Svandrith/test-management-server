package dev.integration_tests;

import dev.domain.*;
import dev.domain.enums.Result;
import dev.dto.responses.testresult.TestResultDtoResponse;
import dev.repos.*;
import dev.service.AuthUserService;
import dev.service.CheckService;
import dev.service.TestResultService;
import dev.tools.mapper.TestResultMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop",
        "spring.flyway.enabled=false"
})
public class TestResultServiceTest {
    @Autowired
    private TestResultRepo testResultRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ProjectRepo projectRepo;
    @Autowired
    private TestCaseRepo testCaseRepo;
    @Autowired
    private TestSuiteRepo testSuiteRepo;
    @Autowired
    private PlanCaseResultRepo planCaseResultRepo;
    private TestResultMapper testResultMapper;
    private AuthUserService authUserService;
    private CheckService checkService;
    private TestResultService testResultService;

    @BeforeEach
    void setUp() {
        testResultMapper = new TestResultMapper();
        authUserService = new AuthUserService(userRepo);

        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);
        testResultService = new TestResultService(testResultRepo, testResultMapper, authUserService, checkService);
    }

    @Test
    public void testAddTestResult(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestCase testCase = testCaseRepo.save(testCase(0, project, testSuite, new ArrayList<>()));

        TestResultDtoResponse response = testResultService.addTestResult("email", project.getId(), testCase.getId(), addTestResultDtoRequest());

        assertAll(
                () -> assertEquals(Result.Passed, response.getResult()),
                () -> assertEquals("comment", response.getComment()),
                () -> assertEquals(testCase.getId(), response.getTestCaseId())
        );
    }

    @Test
    public void testDeleteTestResult(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestCase testCase = testCaseRepo.save(testCase(0, project, testSuite, new ArrayList<>()));
        TestResult testResult = testResultRepo.save(testResult(0, testCase));

        testResultService.deleteTestResult("email", project.getId(), testCase.getId(), testResult.getId());

        assertFalse(testResultRepo.existsById(testResult.getId()));
    }

    @Test
    public void testFindTestResultById(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestCase testCase = testCaseRepo.save(testCase(0, project, testSuite, new ArrayList<>()));
        TestResult testResult = testResultRepo.save(testResult(0, testCase));

        TestResultDtoResponse response = testResultService.findTestResultById("email", project.getId(), testCase.getId(), testResult.getId());

        assertAll(
                () -> assertEquals(Result.Passed, response.getResult()),
                () -> assertEquals("comment", response.getComment()),
                () -> assertEquals(testCase.getId(), response.getTestCaseId())
        );
    }

    @Test
    public void testUpdateTestResult(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestCase testCase = testCaseRepo.save(testCase(0, project, testSuite, new ArrayList<>()));
        TestResult testResult = testResultRepo.save(testResult(0, testCase));

        TestResultDtoResponse response = testResultService.updateTestResult("email", project.getId(), testCase.getId(), testResult.getId(), updateTestResultDtoRequest());

        assertAll(
                () -> assertEquals(Result.Failed, response.getResult()),
                () -> assertEquals("newComment", response.getComment()),
                () -> assertEquals(testCase.getId(), response.getTestCaseId())
        );
    }

    @Test
    public void testFindAllTestCaseResults(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestCase testCase = testCaseRepo.save(testCase(0, project, testSuite, new ArrayList<>()));
        testResultRepo.save(testResult(0, testCase));
        testResultRepo.save(testResult(0, testCase));
        testResultRepo.save(testResult(0, testCase));

        List<TestResultDtoResponse> response = testResultService.findAllTestCaseResults("email", project.getId(), testCase.getId());

        assertEquals(3, response.size());
    }
}
