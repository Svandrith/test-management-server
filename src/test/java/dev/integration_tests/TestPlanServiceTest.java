package dev.integration_tests;

import dev.domain.*;
import dev.domain.enums.PlanStatus;
import dev.domain.enums.Result;
import dev.dto.requests.testPlanCaseResult.TestPlanCaseDtoRequest;
import dev.dto.responses.testplan.TestPlanDtoResponse;
import dev.dto.responses.testplan.TestPlanWithShortTestCasesDtoResponse;
import dev.repos.*;
import dev.service.AuthUserService;
import dev.service.CheckService;
import dev.service.TestPlanService;
import dev.service.TestSuiteService;
import dev.tools.mapper.TestCaseMapper;
import dev.tools.mapper.TestPlanMapper;
import dev.tools.mapper.TestSuiteMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop",
        "spring.flyway.enabled=false"
})
public class TestPlanServiceTest {
    @Autowired
    private TestSuiteRepo testSuiteRepo;
    @Autowired
    private TestPlanRepo testPlanRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ProjectRepo projectRepo;
    @Autowired
    private TestCaseRepo testCaseRepo;
    @Autowired
    private PlanCaseResultRepo planCaseResultRepo;
    private TestSuiteMapper testSuiteMapper;
    private TestPlanMapper testPlanMapper;
    private TestCaseMapper testCaseMapper;
    private AuthUserService authUserService;
    private CheckService checkService;
    private TestPlanService testPlanService;

    @BeforeEach
    void setUp() {
        testSuiteMapper = new TestSuiteMapper();
        authUserService = new AuthUserService(userRepo);

        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);
        testPlanMapper = new TestPlanMapper(testSuiteMapper, checkService);
        testCaseMapper = new TestCaseMapper(checkService);
        testPlanService = new TestPlanService(authUserService, testPlanRepo, testSuiteRepo, testCaseRepo, checkService, testPlanMapper, testCaseMapper, planCaseResultRepo);
    }

    @Test
    public void testAddTestPlan(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));

        TestPlanDtoResponse response = testPlanService.addTestPlan("email", project.getId(), addTestPlanDtoRequest(Collections.singletonList(testSuite.getId())));

        assertAll(
                () -> assertEquals("name", response.getName()),
                () -> assertEquals("description", response.getDescription()),
                () -> assertEquals(PlanStatus.In_progress, response.getStatus()),
                () -> assertEquals(1, response.getTestSuites().size())
        );
    }

    @Test
    public void testDeleteTestPlan(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestPlan testPlan = testPlanRepo.save(testPlan(0, project, Collections.singletonList(testSuite)));

        testPlanService.deleteTestPlan("email", project.getId(), testPlan.getId());

        assertFalse(testPlanRepo.existsById(testPlan.getId()));
    }

    @Test
    public void testUpdateTestPlan(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestPlan testPlan = testPlanRepo.save(testPlan(0, project, Collections.singletonList(testSuite)));

        TestSuite firstNewTestSuite = testSuiteRepo.save(testSuite(0, "firstTestCase", project, new ArrayList<>()));
        TestSuite secondNewTestSuite = testSuiteRepo.save(testSuite(0, "secondTestCase", project, new ArrayList<>()));

        TestPlanDtoResponse response = testPlanService.updateTestPlan("email", project.getId(), testPlan.getId(),
                updateTestPlanDtoRequest("new_name", "new_desc", PlanStatus.Done,Arrays.asList(firstNewTestSuite.getId(), secondNewTestSuite.getId())));

        assertAll(
                () -> assertEquals("new_name", response.getName()),
                () -> assertEquals("new_desc", response.getDescription()),
                () -> assertEquals(PlanStatus.Done, response.getStatus()),
                () -> assertEquals(2, response.getTestSuites().size())
        );
    }

    @Test
    public void testFindTestPlanById(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestPlan testPlan = testPlanRepo.save(testPlan(0, project, Collections.singletonList(testSuite)));


        TestPlanWithShortTestCasesDtoResponse response = testPlanService.findTestPlanById("email", project.getId(), testPlan.getId());

        assertAll(
                () -> assertEquals("name", response.getName()),
                () -> assertEquals("description", response.getDescription()),
                () -> assertEquals(PlanStatus.Done, response.getStatus()),
                () -> assertEquals(project.getId(), response.getProjectId()),
                () -> assertEquals(1, response.getTestSuites().size()),
                () -> assertEquals("title", response.getTestSuites().get(0).getTitle())
        );
    }

    @Test
    public void testFindAllProjectTestPlan(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        testPlanRepo.save(testPlan(0, project, Collections.singletonList(testSuite)));
        testPlanRepo.save(testPlan(0, project, Collections.singletonList(testSuite)));

        List<TestPlanDtoResponse> response = testPlanService.findAllProjectTestPlan("email", project.getId());

        assertEquals(2, response.size());
    }

    @Test
    public void testAddTestPlanCaseResult(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestCase testCase = testCaseRepo.save(testCase(0, project, testSuite, new ArrayList<>()));
        TestPlan testPlan = testPlanRepo.save(testPlan(0, project, Collections.singletonList(testSuite)));

        testPlanService.addTestPlanCaseResult("email", project.getId(), testCase.getId(), testPlan.getId(), new TestPlanCaseDtoRequest(Result.Failed));

        PlanCaseResult result = planCaseResultRepo.findById(new PlanCaseId(testCase.getId(), testPlan.getId())).get();
        assertEquals(Result.Failed,result.getResult());
    }

    @Test
    public void testUpdateTestPlanCaseResult(){
        User user = userRepo.save(user());
        Project project = projectRepo.save(project(user));
        TestSuite testSuite = testSuiteRepo.save(testSuite(0, project, new ArrayList<>()));
        TestCase testCase = testCaseRepo.save(testCase(0, project, testSuite, new ArrayList<>()));
        TestPlan testPlan = testPlanRepo.save(testPlan(0, project, Collections.singletonList(testSuite)));
        PlanCaseResult planCaseResult = planCaseResultRepo.save(new PlanCaseResult(new PlanCaseId(testCase.getId(), testPlan.getId()), Result.Passed, testCase, testPlan));

        testPlanService.updateTestPlanCaseResult("email", project.getId(), testCase.getId(), testPlan.getId(), new TestPlanCaseDtoRequest(Result.Failed));

        PlanCaseResult result = planCaseResultRepo.findById(new PlanCaseId(testCase.getId(), testPlan.getId())).get();
        assertEquals(Result.Failed, result.getResult());
    }

}
