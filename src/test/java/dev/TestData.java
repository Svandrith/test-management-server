package dev;

import dev.domain.*;
import dev.domain.enums.*;
import dev.dto.requests.project.CreateProjectDtoRequest;
import dev.dto.requests.project.UpdateProjectDtoRequest;
import dev.dto.requests.testcase.AddTestCaseDtoRequest;
import dev.dto.requests.testcase.StepDtoRequest;
import dev.dto.requests.testcase.UpdateStepDtoRequest;
import dev.dto.requests.testcase.UpdateTestCaseDtoRequest;
import dev.dto.requests.testplan.AddTestPlanDtoRequest;
import dev.dto.requests.testplan.UpdateTestPlanDtoRequest;
import dev.dto.requests.testresult.AddTestResultDtoRequest;
import dev.dto.requests.testresult.UpdateTestResultDtoRequest;
import dev.dto.requests.testsuite.AddTestSuiteDtoRequest;
import dev.dto.requests.testsuite.UpdateTestSuiteDtoRequest;
import dev.dto.responses.project.ProjectDtoResponse;
import dev.dto.responses.testcase.StepDtoResponse;
import dev.dto.responses.testcase.TestCaseDtoResponse;
import dev.dto.responses.testplan.TestPlanDtoResponse;
import dev.dto.responses.testplan.TestPlanWithShortTestCasesDtoResponse;
import dev.dto.responses.testresult.TestResultDtoResponse;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;
import dev.dto.responses.testsuite.TestSuiteWithCasesWithoutStepsDtoResponse;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Data generator for tests
 */
public class TestData {


    // USER
    public static User user(long id, String email, boolean isActivated, String activationToken){
        User user = new User(email, isActivated);
        user.setId(id);
        user.setActivationToken(activationToken);
        return user;
    }

    public static User user(){
        return new User("email", true);
    }



    // PROJECT
    public static Project project(long id, String name, String description, User user){
        return new Project(id, name, description, user);
    }

    public static Project project(){
        return project(1, "project", "desc", user());
    }

    public static Project project(User user){
        return project(0, "name", "description", user);
    }

    //TEST CASE
    public static TestCase testCase(long id, String title, CaseStatus status, String description, String preConditions, String postConditions, CaseSeverity severity, CasePriority priority, CaseType type, CaseBehavior behavior, Project project, TestSuite testSuite, List<Step> steps){
        return new TestCase(id, title, status, description, preConditions, postConditions, severity, priority, type, behavior, project, testSuite, steps);
    }

    public static TestCase testCase(long id, Project project, TestSuite testSuite, List<Step> steps){
        return testCase(id, "title", CaseStatus.Active, "desc", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, project, testSuite, steps);
    }

    public static TestCase testCase(long id, String title, Project project, TestSuite testSuite, List<Step> steps){
        return testCase(id, title, CaseStatus.Active, "desc", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, project, testSuite, steps);
    }
    //STEP
    public static Step step(long id, String description, String expectedResult){
        return new Step(id, description, expectedResult);
    }

    public static Step step(long id){
        return step(id, "desc", "expRes");
    }

    //TEST SUITE
    public static TestSuite testSuite(long id, String title, SuiteStatus status, String description, String preConditions, Project project, List<TestPlan> testPlans){
        return new TestSuite(id, title, status, description, preConditions, project, testPlans);
    }

    public static TestSuite testSuite(long id, Project project, List<TestPlan> testPlans){
        return new TestSuite(id, "title", SuiteStatus.Active, "description", "preConditions", project, testPlans);
    }

    public static TestSuite testSuite(long id, String title, Project project, List<TestPlan> testPlans){
        return new TestSuite(id, title, SuiteStatus.Active, "description", "preConditions", project, testPlans);
    }

    //TEST PLAN
    public static TestPlan testPlan(long id, String name, String description, PlanStatus status, LocalDateTime creationDate, LocalDateTime lastUpdateDate, Project project, List<TestSuite> testSuite){
        return new TestPlan(id, name, description, status, creationDate, lastUpdateDate, project, testSuite);
    }

    public static TestPlan testPlan(long id, LocalDateTime creationDate, LocalDateTime lastUpdateDate, Project project, List<TestSuite> testSuite){
        return new TestPlan(id, "name", "description", PlanStatus.Done, creationDate, lastUpdateDate, project, testSuite);
    }

    public static TestPlan testPlan(long id, Project project, List<TestSuite> testSuite){
        LocalDateTime date = LocalDateTime.now();
        return new TestPlan(id, "name", "description", PlanStatus.Done, date, date, project, testSuite);
    }

    //TEST RESULT
    public static TestResult testResult(long id, LocalDateTime testDate, Result result, String comment, TestCase testCase){
        return new TestResult(id, testDate, result, comment, testCase);
    }

    public static TestResult testResult(long id, TestCase testCase){
        return testResult(id, LocalDateTime.now(), Result.Passed, "comment", testCase);
    }


    // DTO REQUESTS
    public static CreateProjectDtoRequest createProjectDtoRequest(String name, String description){
        return new CreateProjectDtoRequest(name, description);
    }
    public static CreateProjectDtoRequest createProjectDtoRequest(){
        return createProjectDtoRequest("name", "desc");
    }

    public static UpdateProjectDtoRequest updateProjectDtoRequest(String name, String description){
        return new UpdateProjectDtoRequest(name, description);
    }

    public static UpdateProjectDtoRequest updateProjectDtoRequest(){
        return updateProjectDtoRequest("newName", "newDesc");
    }

    public static UpdateProjectDtoRequest updateProjectDtoRequest(String name){
        UpdateProjectDtoRequest request = new UpdateProjectDtoRequest();
        request.setName(name);
        return request;
    }

    public static AddTestCaseDtoRequest addTestCaseDtoRequest(String title, CaseStatus status, String description, String preConditions, String postConditions, CaseSeverity severity, CasePriority priority, CaseType type, CaseBehavior behavior, long testSuiteId, List<StepDtoRequest> steps){
        return new AddTestCaseDtoRequest(title, status, description, preConditions, postConditions, steps, severity, priority, behavior, type, testSuiteId);
    }

    public static AddTestCaseDtoRequest addTestCaseDtoRequest(long testSuiteId){
        StepDtoRequest step1 = new StepDtoRequest("1desc", "1exp");
        StepDtoRequest step2 = new StepDtoRequest("2desc", "2exp");
        return addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, testSuiteId, Arrays.asList(step1, step2));
    }

    public static StepDtoRequest stepDtoRequest(String description, String expectedResult){
        return new StepDtoRequest(description, expectedResult);
    }

    public static StepDtoRequest stepDtoRequest(){
        return stepDtoRequest("desc", "expRes");
    }

    public static UpdateTestCaseDtoRequest updateTestCaseDtoRequest(String title, CaseStatus status, String description, String preConditions, String postConditions, CaseSeverity severity, CasePriority priority, CaseType type, CaseBehavior behavior, long testSuiteId, List<UpdateStepDtoRequest> steps){
        return new UpdateTestCaseDtoRequest(title, status, description, preConditions, postConditions, severity, priority, type, behavior, testSuiteId, steps);
    }

    public static UpdateTestCaseDtoRequest updateTestCaseDtoRequest(){
        UpdateStepDtoRequest step1 = new UpdateStepDtoRequest(1, "desc1", "expRes1");
        UpdateStepDtoRequest step2 = new UpdateStepDtoRequest(2, "desc2", "expRes2");
        return new UpdateTestCaseDtoRequest("new_title", CaseStatus.Active, "new_descriptions", "new_preConditions", "new_postConditions", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1L, Arrays.asList(step1, step2));
    }

    public static AddTestSuiteDtoRequest addTestSuiteDtoRequest(String title, SuiteStatus status, String description, String preConditions){
        return new AddTestSuiteDtoRequest(title, status, description, preConditions);
    }

    public static AddTestSuiteDtoRequest addTestSuiteDtoRequest(){
        return addTestSuiteDtoRequest("title", SuiteStatus.Active, "desc", "preCond");
    }

    public static UpdateStepDtoRequest updateStepDtoRequest(long id, String stepDescription, String expectedResult){
        return new UpdateStepDtoRequest(id, stepDescription, expectedResult);
    }

    public static UpdateTestSuiteDtoRequest updateTestSuiteDtoRequest(String title, SuiteStatus status, String description, String preConditions){
        return new UpdateTestSuiteDtoRequest(title, status, description, preConditions);
    }

    public static UpdateTestSuiteDtoRequest updateTestSuiteDtoRequest(){
        return updateTestSuiteDtoRequest("title", SuiteStatus.Active, "desc", "preCond");
    }

    public static AddTestResultDtoRequest addTestResultDtoRequest(LocalDateTime testDate, Result result, String comment){
        return new AddTestResultDtoRequest(testDate, result, comment);
    }

    public static AddTestResultDtoRequest addTestResultDtoRequest(){
        LocalDateTime date = LocalDateTime.parse("2022-12-12 10:10:05", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return new AddTestResultDtoRequest(date, Result.Passed, "comment");
    }

    public static UpdateTestResultDtoRequest updateTestResultDtoRequest(LocalDateTime testDate, Result result, String comment){
        return new UpdateTestResultDtoRequest(testDate, result, comment);
    }

    public static UpdateTestResultDtoRequest updateTestResultDtoRequest(){
        LocalDateTime date = LocalDateTime.parse("2022-12-12 10:10:05", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return new UpdateTestResultDtoRequest(date, Result.Failed, "newComment");
    }

    public static AddTestPlanDtoRequest addTestPlanDtoRequest(String name, String description, PlanStatus status, List<Long> testSuitesId){
        return new AddTestPlanDtoRequest(name, description, status, testSuitesId);
    }

    public static AddTestPlanDtoRequest addTestPlanDtoRequest(List<Long> testSuitesId){
        return new AddTestPlanDtoRequest("name", "description", PlanStatus.In_progress, testSuitesId);
    }

    public static UpdateTestPlanDtoRequest updateTestPlanDtoRequest(String name, String description, PlanStatus status, List<Long> testSuitesId){
        return new UpdateTestPlanDtoRequest(name, description, status, testSuitesId);
    }

    public static UpdateTestPlanDtoRequest updateTestPlanDtoRequest(List<Long> testSuitesId){
        return updateTestPlanDtoRequest("name", "description", PlanStatus.Done, testSuitesId);
    }

    public static UpdateTestPlanDtoRequest updateTestPlanDtoRequest(){
        return updateTestPlanDtoRequest("name", "description", PlanStatus.In_progress, new ArrayList<>());
    }




    // DTO RESPONSES
    public static ProjectDtoResponse projectDtoResponse(long id, String name, String description){
        return new ProjectDtoResponse(id, name, description);
    }

    public static ProjectDtoResponse projectDtoResponse(){
        return projectDtoResponse(1, "name", "desc");
    }

    public static TestCaseDtoResponse testCaseDtoResponse(long id, String title, CaseStatus status, String description,
                                                          String preConditions, String postConditions,
                                                          List<StepDtoResponse> steps, CaseSeverity severity,
                                                          CasePriority priority, CaseType type, CaseBehavior behavior,
                                                          long projectId, long suiteId){
        return new TestCaseDtoResponse(id, title, status, description, preConditions, postConditions, steps, severity,
                priority, type, behavior, projectId, suiteId);
    }

    public static TestCaseDtoResponse testCaseDtoResponse(long id, long projectId, long suiteId){
        StepDtoResponse step1 = new StepDtoResponse(1,"1desc", "1exp");
        StepDtoResponse step2 = new StepDtoResponse(2,"2desc", "2exp");
        return testCaseDtoResponse(id, "title", CaseStatus.Active, "desc", "preCond", "postCond", Arrays.asList(step1, step2), CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, projectId, suiteId);
    }

    public static TestSuiteDtoResponse testSuiteDtoResponse(long id, String title, SuiteStatus status, String description, String preConditions, long projectId){
        return new TestSuiteDtoResponse(id, title, status, description, preConditions, projectId);
    }

    public static TestSuiteDtoResponse testSuiteDtoResponse(long id, long projectId){
        return new TestSuiteDtoResponse(id, "title", SuiteStatus.Active, "description", "preConditions", projectId);
    }

    public static TestResultDtoResponse testResultDtoResponse(long id, LocalDateTime testDate, Result result, String comment, long testCaseId){
        return new TestResultDtoResponse(id, testDate, result, comment, testCaseId);
    }

    public static TestResultDtoResponse testResultDtoResponse(long id, long testCaseId){
        return testResultDtoResponse(id, LocalDateTime.now(), Result.Passed, "comment", testCaseId);
    }

    public static TestPlanDtoResponse testPlanDtoResponse(long id, String name, String description, PlanStatus status, long projectId, List<TestSuiteDtoResponse> testSuites){
        return new TestPlanDtoResponse(id, name, description, status, projectId, testSuites);
    }

    public static TestPlanDtoResponse testPlanDtoResponse(long id, long projectId, List<TestSuiteDtoResponse> testSuites){
        return testPlanDtoResponse(id, "name", "desc", PlanStatus.In_progress, projectId, testSuites);
    }

    public static TestPlanWithShortTestCasesDtoResponse testPlanWithShortTestCasesDtoResponse(long id, String name, String description, PlanStatus status, long projectId, List<TestSuiteWithCasesWithoutStepsDtoResponse> testSuites){
        return new TestPlanWithShortTestCasesDtoResponse(id, name, description, status, projectId, testSuites);
    }

    public static TestPlanWithShortTestCasesDtoResponse testPlanWithShortTestCasesDtoResponse(long id, long projectId, List<TestSuiteWithCasesWithoutStepsDtoResponse> testSuites){
        return new TestPlanWithShortTestCasesDtoResponse(id, "name", "description", PlanStatus.Done, projectId, testSuites);
    }

}
