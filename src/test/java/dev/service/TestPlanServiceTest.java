package dev.service;

import dev.domain.Project;
import dev.domain.TestPlan;
import dev.domain.User;
import dev.dto.requests.testplan.UpdateTestPlanDtoRequest;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.*;
import dev.tools.mapper.TestCaseMapper;
import dev.tools.mapper.TestPlanMapper;
import dev.tools.mapper.TestSuiteMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPlanServiceTest {
    private UserRepo userRepo;
    private ProjectRepo projectRepo;
    private TestCaseRepo testCaseRepo;
    private AuthUserService authUserService;
    private TestPlanRepo testPlanRepo;
    private TestSuiteRepo testSuiteRepo;
    private PlanCaseResultRepo planCaseResultRepo;
    private CheckService checkService;
    private TestPlanMapper testPlanMapper;
    private TestSuiteMapper testSuiteMapper;
    private TestCaseMapper testCaseMapper;
    private TestPlanService testPlanService;

    @BeforeEach
    public void init(){
        userRepo = mock(UserRepo.class);
        projectRepo = mock(ProjectRepo.class);
        testCaseRepo = mock(TestCaseRepo.class);
        testSuiteRepo = mock(TestSuiteRepo.class);
        testPlanRepo = mock(TestPlanRepo.class);
        planCaseResultRepo = mock(PlanCaseResultRepo.class);
        authUserService = new AuthUserService(userRepo);
        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);

        testSuiteMapper = new TestSuiteMapper();
        testPlanMapper = new TestPlanMapper(testSuiteMapper, checkService);
        testPlanService = new TestPlanService(authUserService, testPlanRepo, testSuiteRepo, testCaseRepo, checkService, testPlanMapper, testCaseMapper, planCaseResultRepo);
    }

    @Test
    public void testAddTestPlan_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.addTestPlan("email", 1, addTestPlanDtoRequest(new ArrayList<>())));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testAddTestPlan_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.addTestPlan("email", 1, addTestPlanDtoRequest(new ArrayList<>())));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestPlan_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.deleteTestPlan("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestPlan_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.deleteTestPlan("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestPlan_testPlanNotFound(){
        User user = user(1, "email", true, null);

        Project project = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testPlanRepo.findByIdAndProject(1, project)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_PLAN_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.deleteTestPlan("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestPlan_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.updateTestPlan("email", 1, 1, updateTestPlanDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestPlan_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.updateTestPlan("email", 1, 1, updateTestPlanDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestPlan_testPlanNotFound(){
        User user = user(1, "email", true, null);

        Project project = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testPlanRepo.findByIdAndProject(1, project)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_PLAN_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.updateTestPlan("email", 1, 1, updateTestPlanDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestPlan_newTestSuiteNotFound(){
        User user = user(1, "email", true, null);

        Project project = project(1, "name", "desc", user);

        TestPlan testPlan = testPlan(1, project, new ArrayList<>());

        UpdateTestPlanDtoRequest request = updateTestPlanDtoRequest(Arrays.asList(1L));

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testPlanRepo.findByIdAndProject(1, project)).thenReturn(Optional.of(testPlan));
        when(testSuiteRepo.findByIdAndProject(1, project)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_SUITE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.updateTestPlan("email", 1, 1, request));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestPlanById_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.findTestPlanById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestPlanById_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.findTestPlanById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestPlan_testPlanNotFound(){
        User user = user(1, "email", true, null);

        Project project = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testPlanRepo.findByIdAndProject(1, project)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_PLAN_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testPlanService.findTestPlanById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }




}
