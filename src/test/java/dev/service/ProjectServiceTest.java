package dev.service;

import dev.domain.Project;
import dev.domain.User;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.*;
import dev.tools.mapper.ProjectMapper;
import dev.tools.mapper.TestCaseMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static dev.TestData.*;
import static org.mockito.Mockito.*;

public class ProjectServiceTest {

    private ProjectService projectService;
    private UserRepo userRepo;
    private ProjectRepo projectRepo;
    private TestCaseRepo testCaseRepo;
    private TestSuiteRepo testSuiteRepo;
    private PlanCaseResultRepo planCaseResultRepo;
    private ProjectMapper projectMapper;
    private TestCaseMapper testCaseMapper;
    private AuthUserService authUserService;
    private CheckService checkService;

    @BeforeEach
    public void init(){
        userRepo = mock(UserRepo.class);
        projectRepo = mock(ProjectRepo.class);
        testCaseRepo = mock(TestCaseRepo.class);
        testSuiteRepo = mock(TestSuiteRepo.class);
        planCaseResultRepo = mock(PlanCaseResultRepo.class);
        authUserService = new AuthUserService(userRepo);
        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);


        projectMapper = new ProjectMapper();
        testCaseMapper = new TestCaseMapper(checkService);
        projectService = new ProjectService(userRepo, projectMapper, projectRepo, authUserService, testCaseRepo, testSuiteRepo, checkService, testCaseMapper);
    }

    @Test
    public void testInsertProject(){
        User user = user(1, "email", true, null);
        Project projectBeforeSave = project(0, "name", "desc", user);
        Project projectAfterSave = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.save(projectBeforeSave)).thenReturn(projectAfterSave);

        assertEquals(projectDtoResponse(1, "name", "desc"), projectService.createProject(createProjectDtoRequest(), "email"));
    }

    @Test
    public void testInsertProject_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.createProject(createProjectDtoRequest(), "email"));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteProject_userNotFound(){
        User user = user(1, "email", true, null);
        Project projectBeforeSave = project(0, "name", "desc", user);
        Project projectAfterSave = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());
        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.deleteProject( "email", 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteProject_projectNotFound(){
        User user = user();
        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.deleteProject("email", 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateProject_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.updateProject(1, "email", updateProjectDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateProject_projectNotFound(){
        User user = user();
        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.updateProject(1, "email", updateProjectDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindProjectById_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.findProjectById("email", 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindProjectById_projectNotFound(){
        User user = user();
        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.findProjectById("email", 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindUserProjectsWithTestCount_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.findAllProjectsWithTestCasesAndSuitesCount("email"));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindProjectTestSuitesAndCases_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.findAllProjectsWithTestCasesAndSuitesCount("email"));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindProjectTestSuitesAndCases_projectNotFound(){
        User user = user();

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(0, user)).thenReturn(Optional.empty());


        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.findProjectTestSuiteWithCases("email", 0));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindUserProjectsByName_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> projectService.findUserProjectsByName("email", "name"));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }
}