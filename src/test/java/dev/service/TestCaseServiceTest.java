package dev.service;

import dev.domain.*;
import dev.dto.requests.testcase.AddTestCaseDtoRequest;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.*;
import dev.tools.mapper.TestCaseMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestCaseServiceTest {
    private TestCaseMapper testCaseMapper;
    private ProjectRepo projectRepo;
    private UserRepo userRepo;
    private StepRepo stepRepo;
    private TestCaseRepo testCaseRepo;
    private TestSuiteRepo testSuiteRepo;
    private PlanCaseResultRepo planCaseResultRepo;
    private AuthUserService authUserService;
    private CheckService checkService;
    private TestCaseService testCaseService;

    @BeforeEach
    public void init(){
        userRepo = mock(UserRepo.class);
        projectRepo = mock(ProjectRepo.class);
        stepRepo = mock(StepRepo.class);
        testCaseRepo = mock(TestCaseRepo.class);
        testSuiteRepo = mock(TestSuiteRepo.class);
        planCaseResultRepo = mock(PlanCaseResultRepo.class);
        authUserService = new AuthUserService(userRepo);
        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);


        testCaseMapper = new TestCaseMapper(checkService);
        testCaseService = new TestCaseService(testCaseMapper, stepRepo, testCaseRepo, testSuiteRepo, authUserService, checkService);
    }

    @Test
    public void testAddTestCase(){
        User user = user(1, "email", true, null);

        Project project = project(1, "name", "desc", user);

        TestSuite testSuite = testSuite(1, project, new ArrayList<>());

        Step step1 = new Step(1L, "1desc", "1exp");
        Step step2 = new Step(2L, "2desc", "2exp");

        AddTestCaseDtoRequest request = addTestCaseDtoRequest(1);
        TestCase testCase = testCase(1, project, testSuite, Arrays.asList(step1, step2));


        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.findById(1L)).thenReturn(Optional.of(testSuite));
        when(testCaseRepo.save(testCaseMapper.toModel(request, project, testSuite))).thenReturn(testCase);

        assertEquals(testCaseDtoResponse(1, 1, 1), testCaseService.addTestCase("email", 1, request));
    }

    @Test
    public void testAddTestCase_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.addTestCase("email", 1, addTestCaseDtoRequest(1)));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testAddTestCase_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.addTestCase("email", 1, addTestCaseDtoRequest(1)));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testAddTestCase_testSuiteNotFound(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_SUITE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.addTestCase("email", 1, addTestCaseDtoRequest(1)));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestCase_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.deleteTestCase("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestCase_notUsersProject(){
        User user = user(1, "email", true, null);
        Project project = project(user);

        TestCase testCase = testCase(1, project, testSuite(1, project, new ArrayList<>()), new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase));

        String expectedServerErrorCode = ServerErrorCode.NOT_USERS_TEST_CASE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.deleteTestCase("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestCase_testCaseNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_CASE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.deleteTestCase("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestCase_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.updateTestCase("email", 1, 1, updateTestCaseDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestCase_testCaseNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_CASE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.updateTestCase("email", 1, 1, updateTestCaseDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestCase_notUsersProject(){
        User user = user(1, "email", true, null);
        Project project = project(user);

        TestCase testCase = testCase(1, project, testSuite(1, project, new ArrayList<>()), new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase));

        String expectedServerErrorCode = ServerErrorCode.NOT_USERS_TEST_CASE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.updateTestCase("email", 1, 1, updateTestCaseDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestCaseById_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.findTestCaseById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestCaseById_testCaseNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_CASE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.findTestCaseById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestCaseById_notUsersProject(){
        User user = user(1, "email", true, null);
        Project project = project(user);

        TestCase testCase = testCase(1, project, testSuite(1, project, new ArrayList<>()), new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase));

        String expectedServerErrorCode = ServerErrorCode.NOT_USERS_TEST_CASE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testCaseService.findTestCaseById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }
}
