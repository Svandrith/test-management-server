package dev.service;

import dev.domain.Project;
import dev.domain.TestCase;
import dev.domain.TestSuite;
import dev.domain.User;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.*;
import dev.tools.mapper.TestResultMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Optional;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestResultServiceTest {
    private UserRepo userRepo;
    private ProjectRepo projectRepo;
    private TestCaseRepo testCaseRepo;
    private TestSuiteRepo testSuiteRepo;
    private TestResultRepo testResultRepo;
    private PlanCaseResultRepo planCaseResultRepo;
    private TestResultMapper testResultMapper;
    private AuthUserService authUserService;
    private CheckService checkService;
    private TestResultService testResultService;

    @BeforeEach
    public void init(){
        userRepo = mock(UserRepo.class);
        projectRepo = mock(ProjectRepo.class);
        testCaseRepo = mock(TestCaseRepo.class);
        testSuiteRepo = mock(TestSuiteRepo.class);
        testResultRepo = mock(TestResultRepo.class);
        planCaseResultRepo = mock(PlanCaseResultRepo.class);
        authUserService = new AuthUserService(userRepo);
        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);

        testResultMapper = new TestResultMapper();
        testResultService = new TestResultService(testResultRepo, testResultMapper, authUserService, checkService);
    }

    @Test
    public void testAddTestResult_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.addTestResult("email", 1, 1, addTestResultDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testAddTestResult_testCaseNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_CASE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.addTestResult("email", 1, 1, addTestResultDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testAddTestResult_notUsersTestCase(){
        User user = user(1, "email", true, null);
        Project project = project(2, "name", "desc", user);
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase(1, project, testSuite, new ArrayList<>())));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project(user(2, "ema", true, ""))));

        String expectedServerErrorCode = ServerErrorCode.NOT_USERS_TEST_CASE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.addTestResult("email", 1, 1, addTestResultDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestResult_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.deleteTestResult("email", 1, 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestResult_testCaseNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_CASE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.deleteTestResult("email", 1, 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestResult_notUsersTestCase(){
        User user = user(1, "email", true, null);
        Project project = project(2, "name", "desc", user);
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase(1, project, testSuite, new ArrayList<>())));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project(user(2, "ema", true, ""))));

        String expectedServerErrorCode = ServerErrorCode.NOT_USERS_TEST_CASE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.deleteTestResult("email", 1, 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestResult_testResultNotFound(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());
        TestCase testCase = testCase(1, project, testSuite, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testResultRepo.findByIdAndTestCase(1, testCase)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_RESULT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.deleteTestResult("email", 1, 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestResultById_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.findTestResultById("email", 1, 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestResultById_testCaseNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_CASE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.findTestResultById("email", 1, 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestResultById_notUsersTestCase(){
        User user = user(1, "email", true, null);
        Project project = project(2, "name", "desc", user);
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase(1, project, testSuite, new ArrayList<>())));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project(user(2, "ema", true, ""))));

        String expectedServerErrorCode = ServerErrorCode.NOT_USERS_TEST_CASE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.findTestResultById("email", 1, 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestResultById_testResultNotFound(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());
        TestCase testCase = testCase(1, project, testSuite, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testResultRepo.findByIdAndTestCase(1, testCase)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_RESULT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.findTestResultById("email", 1, 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestResult_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.updateTestResult("email", 1, 1, 1, updateTestResultDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestResult_testCaseNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_CASE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.updateTestResult("email", 1, 1, 1, updateTestResultDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestResult_notUsersTestCase(){
        User user = user(1, "email", true, null);
        Project project = project(2, "name", "desc", user);
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase(1, project, testSuite, new ArrayList<>())));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project(user(2, "ema", true, ""))));

        String expectedServerErrorCode = ServerErrorCode.NOT_USERS_TEST_CASE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.updateTestResult("email", 1, 1, 1, updateTestResultDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestResult_testResultNotFound(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());
        TestCase testCase = testCase(1, project, testSuite, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testResultRepo.findByIdAndTestCase(1, testCase)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_RESULT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.updateTestResult("email", 1, 1, 1, updateTestResultDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void findAllTestCaseResults_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.findAllTestCaseResults("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void findAllTestCaseResults_testCaseNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_CASE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testResultService.findAllTestCaseResults("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void findAllTestCaseResults_notUsersTestCase(){
        User user = user(1, "email", true, null);
        Project project = project(2, "name", "desc", user);
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(testCaseRepo.findById(1L)).thenReturn(Optional.of(testCase(1, project, testSuite, new ArrayList<>())));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project(user(2, "ema", true, ""))));

        String expectedServerErrorCode = ServerErrorCode.NOT_USERS_TEST_CASE.toString();

            ServerException serverException = assertThrows(ServerException.class, () -> testResultService.findAllTestCaseResults("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

}
