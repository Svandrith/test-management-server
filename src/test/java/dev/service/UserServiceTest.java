package dev.service;

import dev.domain.User;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.UserRepo;
import dev.tools.MyMailSender;
import dev.tools.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.mail.Session;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static dev.TestData.*;
import static org.mockito.Mockito.*;

public class UserServiceTest {
    private UserRepo userRepo;
    private UserMapper userMapper;
    private MyMailSender mailSender;
    private AuthUserService authUserService;
    private PasswordEncoder passwordEncoder;
    private UserService userService;
    private Session session;

    @BeforeEach
    public void setUp() {
        passwordEncoder = new BCryptPasswordEncoder();
        userRepo = mock(UserRepo.class);
        userMapper = new UserMapper(passwordEncoder);
        authUserService = new AuthUserService(userRepo);
        userService = new UserService(userRepo, userMapper, mailSender, authUserService);
    }

    @Test
    public void testDeleteUser_userNotFound() {
        User user = user();
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> userService.deleteUser("email"));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }
}
