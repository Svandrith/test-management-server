package dev.service;

import dev.domain.Project;
import dev.domain.TestSuite;
import dev.domain.User;
import dev.domain.enums.SuiteStatus;
import dev.dto.requests.testsuite.AddTestSuiteDtoRequest;
import dev.dto.requests.testsuite.UpdateTestSuiteDtoRequest;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;
import dev.exception.ServerErrorCode;
import dev.exception.ServerException;
import dev.repos.*;
import dev.tools.mapper.TestSuiteMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestSuiteServiceTest {

    private UserRepo userRepo;
    private ProjectRepo projectRepo;
    private TestCaseRepo testCaseRepo;
    private TestSuiteRepo testSuiteRepo;
    private PlanCaseResultRepo planCaseResultRepo;
    private TestSuiteMapper testSuiteMapper;
    private AuthUserService authUserService;
    private CheckService checkService;
    private TestSuiteService testSuiteService;

    @BeforeEach
    public void init(){
        userRepo = mock(UserRepo.class);
        projectRepo = mock(ProjectRepo.class);
        testCaseRepo = mock(TestCaseRepo.class);
        testSuiteRepo = mock(TestSuiteRepo.class);
        planCaseResultRepo = mock(PlanCaseResultRepo.class);
        authUserService = new AuthUserService(userRepo);
        checkService = new CheckService(projectRepo, testCaseRepo, testSuiteRepo, planCaseResultRepo);

        testSuiteMapper = new TestSuiteMapper();
        testSuiteService = new TestSuiteService(testSuiteRepo, testSuiteMapper, authUserService, checkService);
    }

    @Test
    public void testAddTestSuite(){
        User user = user(1, "email", true, null);

        Project project = project(1, "name", "desc", user);

        AddTestSuiteDtoRequest request = addTestSuiteDtoRequest();
        TestSuite testSuite = testSuite(1, project, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.save(testSuiteMapper.toModel(request, project))).thenReturn(testSuite);

        assertEquals(testSuiteDtoResponse(1, 1), testSuiteService.addTestSuite("email", 1, request));
    }

    @Test
    public void testAddTestSuite_userNotFound(){
        AddTestSuiteDtoRequest request = addTestSuiteDtoRequest();
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.addTestSuite("email", 1, addTestSuiteDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testAddTestSuite_projectNotFound(){
        User user = user(1, "email", true, null);

        AddTestSuiteDtoRequest request = addTestSuiteDtoRequest();

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.addTestSuite("email", 1, addTestSuiteDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindAllTestSuitesByProject(){
        User user = user(1, "email", true, null);

        Project project = project(1, "name", "desc", user);

        AddTestSuiteDtoRequest request = addTestSuiteDtoRequest();
        TestSuite firstTestSuite = testSuite(1, project, new ArrayList<>());
        TestSuite secondTestSuite = testSuite(2, project, new ArrayList<>());
        TestSuite thirdTestSuite = testSuite(3, project, new ArrayList<>());

        List<TestSuite> list = Arrays.asList(firstTestSuite, secondTestSuite, thirdTestSuite);
        List<TestSuiteDtoResponse> expected = Arrays.asList(
                testSuiteDtoResponse(1, 1), testSuiteDtoResponse(2, 1), testSuiteDtoResponse(3, 1)
        );

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.findAllByProject(project)).thenReturn(Optional.of(list));

        assertEquals(expected, testSuiteService.findAllTestSuitesByProject("email", 1));
    }

    @Test
    public void testFindAllTestSuitesByProject_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.findAllTestSuitesByProject("email", 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindAllTestSuitesByProject_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.findAllTestSuitesByProject("email", 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestSuite_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.updateTestSuite("email", 1, 1, updateTestSuiteDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestSuite_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.updateTestSuite("email", 1, 1, updateTestSuiteDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestSuite_testSuiteNotFound(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.findByIdAndProject(1, project)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_SUITE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.updateTestSuite("email", 1, 1, updateTestSuiteDtoRequest()));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testUpdateTestSuite_tryToUpdateDefaultTestSuite(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);
        TestSuite testSuite = testSuite(1, "defaultTestSuite", project, new ArrayList<>());

        UpdateTestSuiteDtoRequest request = updateTestSuiteDtoRequest("defaultTestSuite", SuiteStatus.Active, "desc", "preCond");

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.findByIdAndProject(1, project)).thenReturn(Optional.of(testSuite));

        String expectedServerErrorCode = ServerErrorCode.CANT_UPDATE_DEFAULT_TEST_SUITE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.updateTestSuite("email", 1, 1, request));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestSuite_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.deleteTestSuite("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestSuite_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.deleteTestSuite("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestSuite_testSuiteNotFound(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.findByIdAndProject(1, project)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_SUITE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.deleteTestSuite("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testDeleteTestSuite_tryToDeleteDefaultTestSuite(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);
        TestSuite testSuite = testSuite(1, "defaultTestSuite", project, new ArrayList<>());

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.findByIdAndProject(1, project)).thenReturn(Optional.of(testSuite));

        String expectedServerErrorCode = ServerErrorCode.CANT_DELETE_DEFAULT_TEST_SUITE.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.deleteTestSuite("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestSuiteById_userNotFound(){
        when(userRepo.findByEmail("email")).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.USER_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.findTestSuiteById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestSuiteById_projectNotFound(){
        User user = user(1, "email", true, null);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.PROJECT_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.findTestSuiteById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }

    @Test
    public void testFindTestSuiteById_testSuiteNotFound(){
        User user = user(1, "email", true, null);
        Project project = project(1, "name", "desc", user);

        when(userRepo.findByEmail("email")).thenReturn(Optional.of(user));
        when(projectRepo.findByIdAndUser(1, user)).thenReturn(Optional.of(project));
        when(testSuiteRepo.findByIdAndProject(1, project)).thenReturn(Optional.empty());

        String expectedServerErrorCode = ServerErrorCode.TEST_SUITE_NOT_FOUND.toString();

        ServerException serverException = assertThrows(ServerException.class, () -> testSuiteService.findTestSuiteById("email", 1, 1));
        assertEquals(expectedServerErrorCode, serverException.getErrorCode().toString());
    }
}
