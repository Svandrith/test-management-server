package dev.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.config.ApplicationUserService;
import dev.config.WebSecurityConfiguration;
import dev.domain.enums.PlanStatus;
import dev.dto.requests.testplan.AddTestPlanDtoRequest;
import dev.dto.requests.testplan.UpdateTestPlanDtoRequest;
import dev.dto.responses.testplan.TestPlanDtoResponse;
import dev.dto.responses.testplan.TestPlanWithShortTestCasesDtoResponse;
import dev.handler.GlobalErrorHandler;
import dev.service.TestPlanService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.ArrayList;
import java.util.stream.Stream;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import(WebSecurityConfiguration.class)
@WebMvcTest(TestPlanController.class)
public class TestPlanControllerTest {
    @MockBean
    private ApplicationUserService applicationUserService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private TestPlanService testPlanService;

    private final String baseURL = "/api/v1/projects";

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertTestPlan() throws Exception {
        AddTestPlanDtoRequest request = addTestPlanDtoRequest(new ArrayList<>());
        TestPlanDtoResponse response = testPlanDtoResponse(1, 1, new ArrayList<>());

        when(testPlanService.addTestPlan("email@mail.ru", 1, request)).thenReturn(response);

        MvcResult result = postTestPlan(request, 1, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestPlanDtoResponse.class));
    }

    @Test
    public void testInsertTestPlan_unauthorizedUser() throws Exception {
        postTestPlan(addTestPlanDtoRequest(new ArrayList<>()), 1, status().isUnauthorized());
    }

    @ParameterizedTest
    @MethodSource("makeWrongAddTestPlanDtoRequest")
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertTestResult_wrongData(AddTestPlanDtoRequest request) throws Exception {
        GlobalErrorHandler.ErrorsResponse errors = mapper.readValue(postTestPlan(request, 1, status().isBadRequest()).getResponse().getContentAsString(), GlobalErrorHandler.ErrorsResponse.class);
        errors.getErrorResponses().forEach((err) -> assertTrue(err.getErrorCode().startsWith("WRONG")));
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testUpdateTestPlan() throws Exception {
        UpdateTestPlanDtoRequest request = updateTestPlanDtoRequest();
        TestPlanDtoResponse response = testPlanDtoResponse(1, 1, new ArrayList<>());

        when(testPlanService.updateTestPlan("email@mail.ru", 1, 1, request)).thenReturn(response);

        MvcResult result = putTestPlan(request, 1, 1, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestPlanDtoResponse.class));
    }

    @Test
    public void testUpdateTestPlan_unauthorizedUser() throws Exception {
        postTestPlan(addTestPlanDtoRequest(new ArrayList<>()), 1, status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testDeleteTestPlan() throws Exception {
        doNothing().when(testPlanService).deleteTestPlan("email@mail.ru", 1, 1);

        deleteTestPlan(status().isNoContent(), 1, 1);
    }

    @Test
    public void testDeleteTestPlan_unauthorizedUser() throws Exception {
        deleteTestPlan(status().isUnauthorized(), 1, 1);
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testFindTestPlanById() throws Exception {
        TestPlanWithShortTestCasesDtoResponse response = testPlanWithShortTestCasesDtoResponse(1, 1, new ArrayList<>());

        when(testPlanService.findTestPlanById("email@mail.ru", 1, 1)).thenReturn(response);

        MvcResult result = findTestPlanById(status().isOk(), 1, 1);

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestPlanWithShortTestCasesDtoResponse.class));
    }

    @Test
    public void testFindTestPlanById_unauthorizedUser() throws Exception {
        findTestPlanById(status().isUnauthorized(), 1, 1);
    }

    public MvcResult postTestPlan(AddTestPlanDtoRequest request, long projectId, ResultMatcher status) throws Exception {
        return mvc.perform(post(baseURL + "/" + projectId + "/test-plan")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult putTestPlan(UpdateTestPlanDtoRequest request, long projectId, long testPlanId, ResultMatcher status) throws Exception {
        return mvc.perform(put(baseURL + "/" + projectId + "/test-plan/" + testPlanId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult findTestPlanById(ResultMatcher status, long projectId, long testPlanId) throws Exception {
        return mvc.perform(get(baseURL + "/" + projectId + "/test-plan/" + testPlanId))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult deleteTestPlan(ResultMatcher status, long projectId, long testPlanId) throws Exception {
        return mvc.perform(delete(baseURL + "/" + projectId + "/test-plan/" + testPlanId))
                .andExpect(status)
                .andReturn();
    }

    public static Stream<Arguments> makeWrongAddTestPlanDtoRequest() {
        return Stream.of(
                Arguments.arguments(addTestPlanDtoRequest(null, "description", PlanStatus.In_progress, new ArrayList<>())),
                Arguments.arguments(addTestPlanDtoRequest("name", "description", null, new ArrayList<>())),
                Arguments.arguments(addTestPlanDtoRequest("", "description", PlanStatus.In_progress, new ArrayList<>()))
                );
    }

}
