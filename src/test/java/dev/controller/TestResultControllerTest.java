package dev.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.config.ApplicationUserService;
import dev.config.WebSecurityConfiguration;
import dev.domain.enums.Result;
import dev.dto.requests.testresult.AddTestResultDtoRequest;
import dev.dto.requests.testresult.UpdateTestResultDtoRequest;
import dev.dto.responses.testresult.TestResultDtoResponse;
import dev.handler.GlobalErrorHandler;
import dev.service.TestResultService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import(WebSecurityConfiguration.class)
@WebMvcTest(TestResultController.class)
public class TestResultControllerTest {
    @MockBean
    private ApplicationUserService applicationUserService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private TestResultService testResultService;

    private final String baseURL = "/api/v1/projects";

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertTestResult() throws Exception {
        AddTestResultDtoRequest request = addTestResultDtoRequest();
        TestResultDtoResponse response = testResultDtoResponse(1, 1);

        when(testResultService.addTestResult("email@mail.ru", 1, 1, request)).thenReturn(response);

        MvcResult result = postTestResult(request, 1, 1, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestResultDtoResponse.class));
    }

    @Test
    public void testInsertTestResult_unauthorizedUser() throws Exception {
        postTestResult(addTestResultDtoRequest(), 1, 1, status().isUnauthorized());
    }

    @ParameterizedTest
    @MethodSource("makeWrongAddTestResultDtoRequest")
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertTestResult_wrongData(AddTestResultDtoRequest request) throws Exception {
        GlobalErrorHandler.ErrorsResponse errors = mapper.readValue(postTestResult(request, 1, 1, status().isBadRequest()).getResponse().getContentAsString(), GlobalErrorHandler.ErrorsResponse.class);
        errors.getErrorResponses().forEach((err) -> assertTrue(err.getErrorCode().startsWith("WRONG")));
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testUpdateTestResult() throws Exception {
        UpdateTestResultDtoRequest request = updateTestResultDtoRequest();
        TestResultDtoResponse response = testResultDtoResponse(1, 1);

        when(testResultService.updateTestResult("email@mail.ru", 1, 1, 1, request)).thenReturn(response);

        MvcResult result = putTestResult(request, 1, 1, 1, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestResultDtoResponse.class));
    }

    @Test
    public void testUpdateTestResult_unauthorizedUser() throws Exception {
        putTestResult(updateTestResultDtoRequest(), 1, 1, 1, status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testDeleteTestResult() throws Exception {
        doNothing().when(testResultService).deleteTestResult("email@mail.ru", 1, 1, 1);

        deleteTestResult(status().isNoContent(), 1, 1, 1);
    }

    @Test
    public void testDeleteTestResult_unauthorizedUser() throws Exception {
        deleteTestResult(status().isUnauthorized(), 1, 1, 1);
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testFindTestResultById() throws Exception {
        TestResultDtoResponse response = testResultDtoResponse(1, 1);

        when(testResultService.findTestResultById("email@mail.ru", 1, 1, 1)).thenReturn(response);

        MvcResult result = findTestResultById(status().isOk(), 1, 1, 1);

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestResultDtoResponse.class));
    }

    @Test
    public void testFindTestResultById_unauthorizedUser() throws Exception {
        findTestResultById(status().isUnauthorized(), 1, 1, 1);
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testFindAllTestCaseResults() throws Exception {
        List<TestResultDtoResponse> response = Arrays.asList(testResultDtoResponse(1, 1), testResultDtoResponse(2, 1));

        when(testResultService.findAllTestCaseResults("email@mail.ru", 1, 1)).thenReturn(response);

        MvcResult result = findAllTestCaseResults(status().isOk(), 1, 1);
        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<TestResultDtoResponse>>() {}));
    }

    @Test
    public void testFindAllTestCaseResults_unauthorizedUser() throws Exception {
        findAllTestCaseResults(status().isUnauthorized(), 1, 1);
    }

    public MvcResult postTestResult(AddTestResultDtoRequest request, long projectId, long testCaseId, ResultMatcher status) throws Exception {
        return mvc.perform(post(baseURL + "/" + projectId + "/test-case/" + testCaseId + "/test-result")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult putTestResult(UpdateTestResultDtoRequest request, long projectId, long testCaseId, long testResultId, ResultMatcher status) throws Exception {
        return mvc.perform(put(baseURL + "/" + projectId + "/test-case/" + testCaseId + "/test-result/" + testResultId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult findTestResultById(ResultMatcher status, long projectId, long testCaseId, long testResultId) throws Exception {
        return mvc.perform(get(baseURL + "/" + projectId + "/test-case/" + testCaseId + "/test-result/" + testResultId))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult deleteTestResult(ResultMatcher status, long projectId, long testCaseId, long testResultId) throws Exception {
        return mvc.perform(delete(baseURL + "/" + projectId + "/test-case/" + testCaseId + "/test-result/" + testResultId))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult findAllTestCaseResults(ResultMatcher status, long projectId, long testCaseId) throws Exception {
        return mvc.perform(get(baseURL + "/" + projectId + "/test-case/" + testCaseId + "/test-result"))
                .andExpect(status)
                .andReturn();
    }

    public static Stream<Arguments> makeWrongAddTestResultDtoRequest() {
        LocalDateTime date = LocalDateTime.now();
        return Stream.of(
                Arguments.arguments(addTestResultDtoRequest(null, Result.Passed, "comment")),
                Arguments.arguments(addTestResultDtoRequest(date, null, "desc"))
        );
    }
}
