package dev.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.config.ApplicationUserService;
import dev.config.WebSecurityConfiguration;
import dev.domain.enums.*;
import dev.dto.requests.testcase.AddTestCaseDtoRequest;
import dev.dto.requests.testcase.StepDtoRequest;
import dev.dto.requests.testcase.UpdateTestCaseDtoRequest;
import dev.dto.responses.testcase.TestCaseDtoResponse;
import dev.handler.GlobalErrorHandler;
import dev.service.TestCaseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.Arrays;
import java.util.stream.Stream;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Import(WebSecurityConfiguration.class)
@WebMvcTest(TestCaseController.class)
public class TestCaseControllerTest {
    @MockBean
    private ApplicationUserService applicationUserService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private TestCaseService testCaseService;

    private final String baseURL = "/api/v1/projects";

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertTestCase() throws Exception {
        AddTestCaseDtoRequest request = addTestCaseDtoRequest(1);
        TestCaseDtoResponse response = testCaseDtoResponse(1, 1, 1);

        when(testCaseService.addTestCase("email@mail.ru", 1, request)).thenReturn(response);

        MvcResult result = postTestCase(request, 1, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestCaseDtoResponse.class));
    }

    @Test
    public void testInsertTestCase_unauthorizedUser() throws Exception {
        postTestCase(addTestCaseDtoRequest(1), 1, status().isUnauthorized());
    }

    @ParameterizedTest
    @MethodSource("makeWrongAddTestCaseDtoRequest")
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertTestCase_wrongData(AddTestCaseDtoRequest request) throws Exception {
        GlobalErrorHandler.ErrorsResponse errors = mapper.readValue(postTestCase(request, 1, status().isBadRequest()).getResponse().getContentAsString(), GlobalErrorHandler.ErrorsResponse.class);
        errors.getErrorResponses().forEach((err) -> assertTrue(err.getErrorCode().startsWith("WRONG")));
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testUpdateTestCase() throws Exception {
        UpdateTestCaseDtoRequest request = updateTestCaseDtoRequest();
        TestCaseDtoResponse response = testCaseDtoResponse(1, 1, 1);

        when(testCaseService.updateTestCase("email@mail.ru", 1, 1, request)).thenReturn(response);

        MvcResult result = putTestCase(request, 1, 1, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestCaseDtoResponse.class));
    }

    @Test
    public void testUpdateTestCase_unauthorizedUser() throws Exception {
        putTestCase(updateTestCaseDtoRequest(), 1, 1, status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testDeleteTestCase() throws Exception {
        doNothing().when(testCaseService).deleteTestCase("email@mail.ru", 1, 1);

        deleteTestCase(status().isNoContent(), 1, 1);
    }

    @Test
    public void testDeleteTestCase_unauthorizedUser() throws Exception {
        putTestCase(updateTestCaseDtoRequest(), 1, 1, status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testFindTestCase() throws Exception {

        TestCaseDtoResponse response = testCaseDtoResponse(1, 1, 1);

        when(testCaseService.findTestCaseById("email@mail.ru", 1, 1)).thenReturn(response);

        MvcResult result = findTestCaseById(status().isOk(), 1, 1);

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestCaseDtoResponse.class));
    }

    @Test
    public void testFindTestCase_unauthorizedUser() throws Exception {
        findTestCaseById(status().isUnauthorized(), 1, 1);
    }

    public MvcResult postTestCase(AddTestCaseDtoRequest request, long projectId, ResultMatcher status) throws Exception {
        return mvc.perform(post(baseURL + "/" + projectId + "/test-case")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult putTestCase(UpdateTestCaseDtoRequest request, long projectId, long testCaseId, ResultMatcher status) throws Exception {
        return mvc.perform(put(baseURL + "/" + projectId + "/test-case/" + testCaseId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult findTestCaseById(ResultMatcher status, long projectId, long testCaseId) throws Exception {
        return mvc.perform(get(baseURL + "/" + projectId + "/test-case/" + testCaseId))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult deleteTestCase(ResultMatcher status, long projectId, long testCaseId) throws Exception {
        return mvc.perform(delete(baseURL + "/" + projectId + "/test-case/" + testCaseId))
                .andExpect(status)
                .andReturn();
    }

    public static Stream<Arguments> makeWrongAddTestCaseDtoRequest() {
        StepDtoRequest step1 = new StepDtoRequest("1desc", "1exp");
        StepDtoRequest step2 = new StepDtoRequest("2desc", "2exp");
        return Stream.of(
                Arguments.arguments(addTestCaseDtoRequest(null, CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(step1, step2))),
                Arguments.arguments(addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", null, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(step1, step2))),
                Arguments.arguments(addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, null, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(step1, step2))),
                Arguments.arguments(addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, null, CaseBehavior.Positive, 1, Arrays.asList(step1, step2))),
                Arguments.arguments(addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, null, 1, Arrays.asList(step1, step2))),
                Arguments.arguments(addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(new StepDtoRequest(null, "exp")))),
                Arguments.arguments(addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(new StepDtoRequest("description", null)))),
                Arguments.arguments(addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(new StepDtoRequest("", "exp")))),
                Arguments.arguments(addTestCaseDtoRequest("title", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(new StepDtoRequest("title", "")))),
                Arguments.arguments(addTestCaseDtoRequest("", CaseStatus.Active, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(step1, step2))),
                Arguments.arguments(addTestCaseDtoRequest("title", null, "description", "preCond", "postCond", CaseSeverity.Medium, CasePriority.Low, CaseType.Usability, CaseBehavior.Positive, 1, Arrays.asList(step1, step2)))
        );
    }


}
