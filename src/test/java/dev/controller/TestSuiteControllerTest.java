package dev.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.config.ApplicationUserService;
import dev.config.WebSecurityConfiguration;
import dev.domain.enums.SuiteStatus;
import dev.dto.requests.testsuite.AddTestSuiteDtoRequest;
import dev.dto.requests.testsuite.UpdateTestSuiteDtoRequest;
import dev.dto.responses.testsuite.TestSuiteDtoResponse;
import dev.handler.GlobalErrorHandler;
import dev.service.TestSuiteService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import(WebSecurityConfiguration.class)
@WebMvcTest(TestSuiteController.class)
public class TestSuiteControllerTest {
    @MockBean
    private ApplicationUserService applicationUserService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private TestSuiteService testSuiteService;

    private final String baseURL = "/api/v1/projects";

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertTestSuite() throws Exception {
        AddTestSuiteDtoRequest request = addTestSuiteDtoRequest();
        TestSuiteDtoResponse response = testSuiteDtoResponse(1, 1);

        when(testSuiteService.addTestSuite("email@mail.ru", 1, request)).thenReturn(response);

        MvcResult result = postTestSuite(request, 1, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestSuiteDtoResponse.class));
    }

    @Test
    public void testInsertTestSuite_unauthorizedUser() throws Exception {
        postTestSuite(addTestSuiteDtoRequest(), 1, status().isUnauthorized());
    }

    @ParameterizedTest
    @MethodSource("makeWrongAddTestSuiteDtoRequest")
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertTestSuite_wrongData(AddTestSuiteDtoRequest request) throws Exception {
        GlobalErrorHandler.ErrorsResponse errors = mapper.readValue(postTestSuite(request, 1, status().isBadRequest()).getResponse().getContentAsString(), GlobalErrorHandler.ErrorsResponse.class);
        errors.getErrorResponses().forEach((err) -> assertTrue(err.getErrorCode().startsWith("WRONG")));
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testDeleteTestSuite() throws Exception {
        doNothing().when(testSuiteService).deleteTestSuite("email@mail.ru", 1, 1);

        deleteTestSuite(status().isNoContent(), 1, 1);
    }

    @Test
    public void testDeleteTestSuite_unauthorizedUser() throws Exception {
        deleteTestSuite(status().isUnauthorized(), 1, 1);
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testFindTestSuiteById() throws Exception {
        TestSuiteDtoResponse response = testSuiteDtoResponse(1, 1);

        when(testSuiteService.findTestSuiteById("email@mail.ru", 1, 1)).thenReturn(response);

        MvcResult result = findTestSuiteById(status().isOk(), 1, 1);

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), TestSuiteDtoResponse.class));
    }

    @Test
    public void testFindTestSuiteById_unauthorizedUser() throws Exception {
        deleteTestSuite(status().isUnauthorized(), 1, 1);
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testUpdateTestSuite() throws Exception {
        UpdateTestSuiteDtoRequest request = updateTestSuiteDtoRequest();
        TestSuiteDtoResponse expectedResponse = testSuiteDtoResponse(1, 1);

        when(testSuiteService.updateTestSuite("email@mail.ru", 1, 1, request)).thenReturn(expectedResponse);

        MvcResult result = putTestSuite(request, 1, 1, status().isOk());

        assertEquals(expectedResponse, mapper.readValue(result.getResponse().getContentAsString(), TestSuiteDtoResponse.class));
    }

    @Test
    public void testUpdateTestSuite_unauthorizedUser() throws Exception {
        putTestSuite(updateTestSuiteDtoRequest(), 1, 1, status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testFindAllTestSuitesByProjectId() throws Exception {
        List<TestSuiteDtoResponse> expectedResponse = Arrays.asList(testSuiteDtoResponse(1, 1), testSuiteDtoResponse(2, 1));

        when(testSuiteService.findAllTestSuitesByProject("email@mail.ru", 1)).thenReturn(expectedResponse);

        MvcResult result = findAllTestSuitesByProjectId(status().isOk(), 1);

        assertEquals(expectedResponse, mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<TestSuiteDtoResponse>>() {}));
    }

    @Test
    public void testFindAllTestSuitesByProjectId_unauthorizedUser() throws Exception {
        findAllTestSuitesByProjectId(status().isUnauthorized(), 1);
    }


    public MvcResult postTestSuite(AddTestSuiteDtoRequest request, long projectId, ResultMatcher status) throws Exception {
        return mvc.perform(post(baseURL + "/" + projectId + "/test-suite")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult putTestSuite(UpdateTestSuiteDtoRequest request, long projectId, long testSuiteId, ResultMatcher status) throws Exception {
        return mvc.perform(put(baseURL + "/" + projectId + "/test-suite/" + testSuiteId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult findTestSuiteById(ResultMatcher status, long projectId, long testSuiteId) throws Exception {
        return mvc.perform(get(baseURL + "/" + projectId + "/test-suite/" + testSuiteId))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult deleteTestSuite(ResultMatcher status, long projectId, long testSuiteId) throws Exception {
        return mvc.perform(delete(baseURL + "/" + projectId + "/test-suite/" + testSuiteId))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult findAllTestSuitesByProjectId(ResultMatcher status, long projectId) throws Exception {
        return mvc.perform(get(baseURL + "/" + projectId + "/test-suite"))
                .andExpect(status)
                .andReturn();
    }

    public static Stream<Arguments> makeWrongAddTestSuiteDtoRequest() {
        return Stream.of(
                Arguments.arguments(addTestSuiteDtoRequest(null, SuiteStatus.Outdated, "desc", "precond")),
                Arguments.arguments(addTestSuiteDtoRequest("title", null, "desc", "precond")),
                Arguments.arguments(addTestSuiteDtoRequest("", SuiteStatus.Outdated, "desc", "precond"))
        );
    }
}
