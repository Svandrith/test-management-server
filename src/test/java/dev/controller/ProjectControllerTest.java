package dev.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.config.ApplicationUserService;
import dev.config.WebSecurityConfiguration;
import dev.dto.requests.project.CreateProjectDtoRequest;
import dev.dto.requests.project.UpdateProjectDtoRequest;
import dev.dto.responses.project.ProjectDtoResponse;
import dev.dto.responses.testsuite.ProjectTestSuiteWithCasesDtoResponse;
import dev.dto.responses.project.ProjectWithTestCountDtoResponse;
import dev.handler.GlobalErrorHandler;
import dev.service.ProjectService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static dev.TestData.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import(WebSecurityConfiguration.class)
@WebMvcTest(ProjectController.class)
public class ProjectControllerTest {
    @MockBean
    private ApplicationUserService applicationUserService;

    @MockBean
    private ProjectService projectService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    private final String baseURL = "/api/v1/projects";

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertProject() throws Exception {
        CreateProjectDtoRequest request = createProjectDtoRequest();
        ProjectDtoResponse response = projectDtoResponse();

        when(projectService.createProject(request, "email@mail.ru")).thenReturn(response);

        MvcResult result = postProject(request, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), ProjectDtoResponse.class));
    }

    @Test
    public void testInsertProject_unauthorizedUser() throws Exception {
        postProject(createProjectDtoRequest(), status().isUnauthorized());
    }

    @ParameterizedTest
    @MethodSource("makeWrongInsertProjectDtoRequest")
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testInsertProject_wrongData(CreateProjectDtoRequest request) throws Exception {
        GlobalErrorHandler.ErrorsResponse errors = mapper.readValue(postProject(request, status().isBadRequest()).getResponse().getContentAsString(), GlobalErrorHandler.ErrorsResponse.class);
        errors.getErrorResponses().forEach((err) -> assertTrue(err.getErrorCode().startsWith("WRONG")));
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testDeleteProject() throws Exception {
        doNothing().when(projectService).deleteProject("email@mail.ru", 1);

        deleteProject(1, status().isNoContent());
    }

    @Test
    public void testDeleteProject_unauthorizedUser() throws Exception {
        deleteProject(1, status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testUpdateProject() throws Exception {
        UpdateProjectDtoRequest request = updateProjectDtoRequest();
        ProjectDtoResponse response = projectDtoResponse();

        when(projectService.updateProject(1, "email@mail.ru", request)).thenReturn(response);

        MvcResult result = updateProject(1, request, status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), ProjectDtoResponse.class));
    }

    @Test
    public void testUpdateProject_unauthorizedUser() throws Exception {
        updateProject(1, updateProjectDtoRequest(), status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testFindProjectById() throws Exception {
        ProjectDtoResponse response = projectDtoResponse();

        when(projectService.findProjectById("email@mail.ru", 1)).thenReturn(response);

        MvcResult result = getUserProjectById(status().isOk(), 1);

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), ProjectDtoResponse.class));
    }

    @Test
    public void testFindProjectById_unauthorizedUser() throws Exception {
        getUserProjectById(status().isUnauthorized(), 1);
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testGetUserProjectsWithTestCount() throws Exception {
        List<ProjectWithTestCountDtoResponse> response = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            response.add(new ProjectWithTestCountDtoResponse((long) i, "name" + i, "desc" + i, i, i));
        }

        when(projectService.findAllProjectsWithTestCasesAndSuitesCount("email@mail.ru")).thenReturn(response);

        MvcResult result = getUserProjectsWithTestCount(status().isOk());

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<ProjectWithTestCountDtoResponse>>() {
        }));
    }

    @Test
    public void testGetUserProjectsWithTestCount_unauthorizedUser() throws Exception {
        getUserProjectsWithTestCount(status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testGetAllUserProjectsByName() throws Exception {
        List<ProjectWithTestCountDtoResponse> response = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            response.add(new ProjectWithTestCountDtoResponse());
        }

        when(projectService.findUserProjectsByName("email@mail.ru", "name")).thenReturn(response);

        MvcResult result = getAllUserProjectsByName(status().isOk(), "name");

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<ProjectWithTestCountDtoResponse>>() {
        }));
    }

    @Test
    public void testGetAllUserProjectsByName_unauthorizedUser() throws Exception {
        getAllUserProjectsByName(status().isUnauthorized(), "name");
    }

    @Test
    @WithMockUser(value = "email@mail.ru", roles = {})
    public void testGetProjectWithTestCasesAndSuites() throws Exception {
        List<ProjectTestSuiteWithCasesDtoResponse> response = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            response.add(new ProjectTestSuiteWithCasesDtoResponse());
        }

        when(projectService.findProjectTestSuiteWithCases("email@mail.ru", 1)).thenReturn(response);

        MvcResult result = getProjectTestCasesAndSuites(status().isOk(), 1);

        assertEquals(response, mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<ProjectTestSuiteWithCasesDtoResponse>>() {
        }));
    }

    @Test
    public void testProjectTestSuiteWithTestCases_unauthorizedUser() throws Exception {
        getProjectTestCasesAndSuites(status().isUnauthorized(), 1);
    }

    public MvcResult postProject(CreateProjectDtoRequest request, ResultMatcher status) throws Exception {
        return mvc.perform(post(baseURL)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult deleteProject(long projectId, ResultMatcher status) throws Exception {
        return mvc.perform(delete(baseURL + "/" + projectId))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult getUserProjectsWithTestCount(ResultMatcher status) throws Exception {
        return mvc.perform(get(baseURL + "/all"))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult getAllUserProjectsByName(ResultMatcher status, String name) throws Exception {
        return mvc.perform(get(baseURL)
                        .queryParam("projectName", name))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult getProjectTestCasesAndSuites(ResultMatcher status, long projectId) throws Exception {
        return mvc.perform(get(baseURL + "/" + projectId + "/test-doc"))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult getUserProjectById(ResultMatcher status, long projectId) throws Exception {
        return mvc.perform(get(baseURL + "/" + projectId))
                .andExpect(status)
                .andReturn();
    }

    public MvcResult updateProject(long projectId, UpdateProjectDtoRequest request, ResultMatcher status) throws Exception {
        return mvc.perform(put(baseURL + "/" + projectId)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status)
                .andReturn();
    }

    public static Stream<Arguments> makeWrongInsertProjectDtoRequest() {
        return Stream.of(
                Arguments.arguments(createProjectDtoRequest(null, "desc")),
                Arguments.arguments(createProjectDtoRequest("", "desc"))
        );
    }
}