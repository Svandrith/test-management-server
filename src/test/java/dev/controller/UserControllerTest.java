package dev.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.config.ApplicationUserService;
import dev.config.WebSecurityConfiguration;
import dev.service.ProjectService;
import dev.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Import(WebSecurityConfiguration.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
    @MockBean
    private ApplicationUserService applicationUserService;

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    private final String baseURL = "/api/v1/users";

    @Test
    @WithMockUser(username = "email@mail.com", roles = {})
    public void testDeleteUser() throws Exception {
        doNothing().when(userService).deleteUser("email@mail.com");
        deleteProject(status().isNoContent());
    }

    @Test
    public void testDeleteUser_notAuthenticated() throws Exception {
        deleteProject(status().isUnauthorized());
    }

    public MvcResult deleteProject(ResultMatcher status) throws Exception{
        return mvc.perform(delete(baseURL))
                .andExpect(status)
                .andReturn();
    }

}
