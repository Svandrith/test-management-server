Чтобы локально запустить сервер нужно установить postgres на комп и создать сервер. А также в идеи нужно задать свою конфигурацию.

Создаем сервер в postgres у которого будет, например, следующие характеристики:
1) Port : 6432
2) Username: postgres
3) Host name : localhost
4) Password : 123456

Далее задаем свою конфигурацию.
В IntelliJ IDEA это делается так:
1) Сверху справа рядом с кнопкой Run нажать на список конфигураций и выбрать раздел "Edit configurations".

2) В раздел Working directory указываем путь от корня до папки test-management-server

3) В раздел Main class вводим: dev.TestManagementApplication

4) В разделе Environment variables нужно будет указать 4 переменных(значения для переменных буду брать те, которые указывал при создании сервера):
    1. PG_HOST=localhost
    2. PG_PASSWORD=123456
    3. PG_USER=postgres
    4. PG_PORT=6432
    
5) Также перед запуском надо не забыть создать базу данных под названием: "test-management-db".


// Порт для API: 8081 

// Версия PostgreSQL: 15


